//
//  ExtensionUtils.swift
//  homecredit
//
//  Created by Oscar Perdanakusuma - Private on 9/6/17.
//  Copyright © 2017 Home Credit Indonesia. All rights reserved.
//
import UIKit
import Foundation

class PaddingLabel: UILabel {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let padding = UIEdgeInsets(top: 8, left: 12, bottom: 8, right: 12)
    
    override func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: padding))
    }
    
    override public var intrinsicContentSize: CGSize {
        let superContentSize = super.intrinsicContentSize
        let width = superContentSize.width + padding.left + padding.right
        let heigth = superContentSize.height + padding.top + padding.bottom
        return CGSize(width: width, height: heigth)
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        let superSizeThatFits = super.sizeThatFits(size)
        let width = superSizeThatFits.width + padding.left + padding.right
        let heigth = superSizeThatFits.height + padding.top + padding.bottom
        return CGSize(width: width, height: heigth)
    }
    
    // Override -intrinsicContentSize: for Auto layout code
    //    override func intrinsicContentSize() -> CGSize {
    //        let superContentSize = super.intrinsicContentSize()
    //        let width = superContentSize.width + padding.left + padding.right
    //        let heigth = superContentSize.height + padding.top + padding.bottom
    //        return CGSize(width: width, height: heigth)
    //    }
    
    // Override -sizeThatFits: for Springs & Struts code
    //    override func sizeThatFits(size: CGSize) -> CGSize {
    //        let superSizeThatFits = super.sizeThatFits(size)
    //        let width = superSizeThatFits.width + padding.left + padding.right
    //        let heigth = superSizeThatFits.height + padding.top + padding.bottom
    //        return CGSize(width: width, height: heigth)
    //    }
}

enum ScrollDirection {
    case Top
    case Right
    case Bottom
    case Left
    
    func contentOffsetWith(scrollView: UIScrollView) -> CGPoint {
        var contentOffset = CGPoint.zero
        switch self {
        case .Top:
            contentOffset = CGPoint(x: 0, y: -scrollView.contentInset.top)
        case .Right:
            contentOffset = CGPoint(x: scrollView.contentSize.width - scrollView.bounds.size.width, y: 0)
        case .Bottom:
            contentOffset = CGPoint(x: 0, y: scrollView.contentSize.height - scrollView.bounds.size.height)
        case .Left:
            contentOffset = CGPoint(x: -scrollView.contentInset.left, y: 0)
        }
        return contentOffset
    }
}

extension UIScrollView {
    
    func scrollTo(direction: ScrollDirection, animated: Bool = true) {
        self.setContentOffset(direction.contentOffsetWith(scrollView: self), animated: animated)
    }
    
}

extension UIView{
    func setCircleStrokeForView(view: UIView, strokeWidth: CGFloat, strokeColor: UIColor){
        view.layer.borderWidth = strokeWidth
        view.layer.borderColor = strokeColor.cgColor
        view.layer.cornerRadius = (view.frame.height) / 2
        view.clipsToBounds = true
    }
    
    func setCircleBackgroundForView(view: UIView, bgColor: UIColor){
        view.layer.cornerRadius = (view.frame.width) / 2
        view.layer.masksToBounds = true
        view.clipsToBounds = true
        view.backgroundColor = bgColor
    }
    
    func addTapEventForView(view: UIView, action: Selector){
        let tap = UITapGestureRecognizer(target: self, action: action)
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(tap)
    }
    
    func fadeIn(duration: TimeInterval) {
        // Move our fade out code from earlier
        UIView.animate(withDuration: duration, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
            self.isHidden = false
        }, completion: nil)
    }
    
    func fadeOut(duration: TimeInterval) {
        UIView.animate(withDuration: duration, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.alpha = 0.0
            self.isHidden = true
        }, completion: nil)
    }
    
    
    func fadeIn() {
        // Move our fade out code from earlier
        UIView.animate(withDuration: 0.25, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
            self.isHidden = false
        }, completion: nil)
    }
    
    func fadeOut() {
        UIView.animate(withDuration: 0.25, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.alpha = 0.0
            self.isHidden = true
        }, completion: nil)
    }
    
    func addTopBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: width)
        self.layer.addSublayer(border)
    }
    
    func addRightBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: self.frame.size.width - width, y: 0, width: width, height: self.frame.size.height)
        self.layer.addSublayer(border)
    }
    
    func addBottomBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
        self.layer.addSublayer(border)
    }
    
    func addLeftBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: 0, width: width, height: self.frame.size.height)
        self.layer.addSublayer(border)
    }
}

extension UIViewController{
    
    func showMainLayout(view: UIView){
        view.fadeIn()
    }
    
    func hideMainLayout(view: UIView){
        view.alpha = 0
        view.isHidden = true
    }
    
    func setButtonText(button: UIButton, text: String){
        button.setTitle(text, for: .normal)
        button.setTitle(text, for: .selected)
        button.setTitle(text, for: .highlighted)
    }
    
    func removeHTMLTag(text: String) -> String{
        return text.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    }
    
    func showToast(message : String) {
        let toastLabel = PaddingLabel(frame: CGRect.zero)
        toastLabel.text = message
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.75)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center
        toastLabel.numberOfLines = 0
        toastLabel.font = UIFont.systemFont(ofSize: Constant.sharedIns.NORMAL_FONT_SIZE)
        toastLabel.alpha = 1.0
        toastLabel.sizeToFit()
        
        
        let labelSize = toastLabel.sizeThatFits(CGSize(width: self.view.frame.width / 2, height: CGFloat.greatestFiniteMagnitude))
        
        toastLabel.frame.size.width = labelSize.width
        toastLabel.frame.size.height = labelSize.height
        toastLabel.frame.origin.x =  (UIScreen.main.bounds.width / 2) - (labelSize.width / 2)
        toastLabel.frame.origin.y = 125
        
        toastLabel.layer.cornerRadius = 12;
        toastLabel.clipsToBounds  =  true
        
        self.view.addSubview(toastLabel)
        
        let delay = DispatchTime.now() + 2.5
        DispatchQueue.main.asyncAfter(deadline: delay) {
            UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveEaseOut, animations: {
                toastLabel.alpha = 0.0
            }, completion: {(isCompleted) in
                toastLabel.removeFromSuperview()
            })
        }
    }
    
    func showToast(view : UIView, message : String) {
        let toastLabel = PaddingLabel(frame: CGRect.zero)
        toastLabel.text = message
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.85)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center
        toastLabel.numberOfLines = 0
        toastLabel.font = UIFont.systemFont(ofSize: Constant.sharedIns.NORMAL_FONT_SIZE)
        toastLabel.alpha = 1.0
        toastLabel.sizeToFit()
        
        
        let labelSize = toastLabel.sizeThatFits(CGSize(width: self.view.frame.width / 2, height: CGFloat.greatestFiniteMagnitude))
        
        toastLabel.frame.size.width = labelSize.width
        toastLabel.frame.size.height = labelSize.height
        toastLabel.frame.origin.x =  (UIScreen.main.bounds.width / 2) - (labelSize.width / 2)
        toastLabel.frame.origin.y = (view.frame.height / 2) - (toastLabel.frame.height / 2)
        
        toastLabel.layer.cornerRadius = 12;
        toastLabel.clipsToBounds  =  true
        
        view.addSubview(toastLabel)
        
        let delay = DispatchTime.now() + 2.5
        DispatchQueue.main.asyncAfter(deadline: delay) {
            UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveEaseOut, animations: {
                toastLabel.alpha = 0.0
            }, completion: {(isCompleted) in
                toastLabel.removeFromSuperview()
            })
        }
    }
    
    func showToast(yPos: CGFloat, message : String, duration: Double) {
        let toastLabel = PaddingLabel(frame: CGRect.zero)
        toastLabel.text = message
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.85)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center
        toastLabel.numberOfLines = 0
        toastLabel.font = UIFont.systemFont(ofSize: Constant.sharedIns.NORMAL_FONT_SIZE)
        toastLabel.alpha = 1.0
        toastLabel.sizeToFit()
        
        
        let labelSize = toastLabel.sizeThatFits(CGSize(width: self.view.frame.width / 2, height: CGFloat.greatestFiniteMagnitude))
        
        toastLabel.frame.size.width = labelSize.width
        toastLabel.frame.size.height = labelSize.height
        toastLabel.frame.origin.x =  (UIScreen.main.bounds.width / 2) - (labelSize.width / 2)
        toastLabel.frame.origin.y = yPos
        
        toastLabel.layer.cornerRadius = 12;
        toastLabel.clipsToBounds  =  true
        
        self.view.addSubview(toastLabel)
        
        let delay = DispatchTime.now() + duration
        DispatchQueue.main.asyncAfter(deadline: delay) {
            UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveEaseOut, animations: {
                toastLabel.alpha = 0.0
            }, completion: {(isCompleted) in
                toastLabel.removeFromSuperview()
            })
        }
    }
    
    func dateInterval(from: String) -> Int {
        
        //let currentCalendar = Calendar.current
        let serverDate = CustomUserDefaults.shared.getServerDate()
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        //dateFormatter.locale = NSLocale(localeIdentifier: "id_ID") as Locale
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        let currentDate = dateFormatter.date(from: serverDate)
        let fromDate = dateFormatter.date(from: from)
        
        let calendar = Calendar(identifier: .gregorian)
        
        guard let start = calendar.ordinality(of: .day, in: .era, for: fromDate!) else { return 0 }
        guard let end = calendar.ordinality(of: .day, in: .era, for: currentDate!) else { return 0 }
        
        return Int(start - end)
    }
    
    func getNextDate(fromDate: String, forDays: Int) -> String{
        var nextDateString = ""
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        let date = dateFormatter.date(from: fromDate)
        
        let calendar = Calendar(identifier: .gregorian)
        
        let nextDate = calendar.date(byAdding: .day, value: forDays, to: date!)
        
        nextDateString = Constant.convertDateStringFormat(date: nextDate! as Date, format: "dd/MM/yyyy")
        
        
        return nextDateString
    }
    
    func popViewController()  {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func setTextFieldColor(tField : UITextField, color: String){
        tField.textColor = UIColor(string: color)
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func setCenteredScrollView(scrollView: UIScrollView, contentView: UIView){
        //let scrollViewHeight = scrollView.bounds.height
        let contentHeight = contentView.bounds.height + 85 + UIApplication.shared.statusBarFrame.height
        
        let deviceHeight = (UIApplication.shared.keyWindow?.frame.height)!
        
        
        print("HEIGHT_VIEW: \(deviceHeight) \(contentHeight)")
        
        if(contentHeight < deviceHeight){
            //            let scrollViewBounds = scrollView.bounds
            //
            //            var scrollViewInsets = UIEdgeInsets.zero
            //            scrollViewInsets.top = scrollViewBounds.size.height / 2.0
            //            scrollViewInsets.top -= contentView.bounds.size.height / 2.0
            //
            //            scrollViewInsets.bottom = scrollViewBounds.size.height / 2.0
            //            scrollViewInsets.bottom -= contentView.bounds.size.height / 2.0
            //
            //            scrollView.contentInset = scrollViewInsets
            
            
            contentView.center = self.view.center
            //scrollView.contentMode = .scaleAspectFit
            
            
        }
    }
    
    func addTapEventForView(view: UIView, action: Selector){
        let tap = UITapGestureRecognizer(target: self, action: action)
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(tap)
    }
    
    func addTouchDownEvent(view: UIView, highlightColor: String){
        let longPress = UILongPressGestureRecognizer(target: self, action: nil)
        longPress.minimumPressDuration = 0.1
        
        switch(longPress.state){
        case UIGestureRecognizerState.began:
            view.backgroundColor = UIColor(string: highlightColor)
            break
        case UIGestureRecognizerState.cancelled:
            view.backgroundColor = UIColor.clear
            break
        case UIGestureRecognizerState.ended:
            view.backgroundColor = UIColor.clear
            break
        default:
            view.backgroundColor = UIColor.clear
            break
        }
        
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(longPress)
    }
    
    func getBottomLineTextField(textField: UITextField)->CALayer{
        textField.borderStyle = UITextField.BorderStyle.none
        
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0.0, y: textField.frame.height - 1, width: textField.frame.width, height: 0.5)
        bottomLine.backgroundColor = UIColor.gray.cgColor
        return bottomLine
    }
    
    func setBottomLineView(view: UIView){
        
        //textField.borderStyle = UITextBorderStyle.none
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: view.frame.height-1, width: UIScreen.main.bounds.width - 32, height: 0.65)
        bottomLine.backgroundColor = UIColor.lightGray.cgColor
        view.layer.addSublayer(bottomLine)
    }
    
    func setBottomLineViewForPhone(view: UIView){
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: view.frame.height-1, width: view.bounds.width, height: 0.65)
        bottomLine.backgroundColor = UIColor.lightGray.cgColor
        view.layer.addSublayer(bottomLine)
    }
    
    func setBottomLineView(view: UIView, lineColor: String){
        
        //textField.borderStyle = UITextBorderStyle.none
        
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: view.bounds.height-1, width: view.bounds.width, height: 0.85)
        bottomLine.backgroundColor = UIColor(string: lineColor).cgColor
        view.layer.addSublayer(bottomLine)
    }
    
    func setPaddingTextField(textField: UITextField){
        let paddingView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 12.0, height: textField.frame.height))
        textField.leftView = paddingView
        textField.leftViewMode = UITextField.ViewMode.always
    }
    
    func setPaddingTextView(txtView: UITextView){
        txtView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
    func setCustomRoundedTextField(tField: UITextField){
        setPaddingTextField(textField: tField)
        tField.borderStyle = UITextField.BorderStyle.none
        
        tField.layer.cornerRadius = 8
        tField.backgroundColor = UIColor(string: Constant.sharedIns.color_grey_200)
        tField.textColor = UIColor(string: Constant.sharedIns.color_grey_800)
        tField.clipsToBounds = true
    }
    
    func setCustomRoundedTextView(txtView: UITextView){
        //setPaddingTextView(txtView: txtView)
        
        txtView.layer.cornerRadius = 8
        txtView.backgroundColor = UIColor(string: Constant.sharedIns.color_grey_200)
        txtView.textColor = UIColor(string: Constant.sharedIns.color_grey_800)
        txtView.textContainerInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        //setPaddingTextView(txtView: txtView)
        txtView.clipsToBounds = true
    }
    
    func setRoundedView(view: UIView, bgColor: String)->Void{
        view.layer.cornerRadius = 12
        view.clipsToBounds = true
        view.backgroundColor = UIColor(string: bgColor)
    }
    
    func setRoundedButton(button: UIButton, textColor: String, bgColor: String, isBold: Bool)->Void{
        button.layer.cornerRadius = 8
        button.backgroundColor = UIColor(string: bgColor)
        button.setTitleColor(UIColor(string: textColor), for: .normal)
        button.clipsToBounds = true
        
        button.contentEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        
        if(isBold){
            //button.titleLabel?.font = UIFont.boldSystemFont(ofSize: UIFont.systemFontSize)
        }
    }
    
    func setRoundedButton(button: UIButton,
                          textColor: String,
                          bgColor: String,
                          insetRight: CGFloat,
                          insetLeft: CGFloat,
                          insetTop: CGFloat,
                          insetBottom: CGFloat)->Void{
        button.backgroundColor = UIColor(string: bgColor)
        
        button.layer.cornerRadius = 8
        button.setTitleColor(UIColor(string: textColor), for: .normal)
        button.clipsToBounds = true
        
        button.contentEdgeInsets = UIEdgeInsets(top: insetTop, left: insetLeft, bottom: insetBottom, right: insetRight)
    }
    
    func setRoundedView(view: UIView)->Void{
        view.layer.cornerRadius = 12
        view.clipsToBounds = true
    }
    
    func setRoundedView(view: UIView, corner: CGFloat)->Void{
        view.layer.cornerRadius = corner
        view.clipsToBounds = true
    }
    
    func setRoundedButtonByThemeColor(button: UIButton)->Void{
        let themeColor = CustomUserDefaults.shared.getThemeColor()
        
        if(themeColor == CustomUserDefaults.shared.RED_THEME){
            button.backgroundColor = UIColor(string: Constant.sharedIns.color_red_main)
        } else if(themeColor == CustomUserDefaults.shared.ORANGE_THEME){
            button.backgroundColor = UIColor(string: Constant.sharedIns.color_orange_main)
        } else{
            button.backgroundColor = UIColor(string: Constant.sharedIns.color_green_main)
        }
        
        
        button.layer.cornerRadius = 8
        button.setTitleColor(UIColor.white, for: .normal)
        button.clipsToBounds = true
        
        button.contentEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
    func setRoundedButtonByThemeColor(button: UIButton,
                                      insetRight: CGFloat,
                                      insetLeft: CGFloat,
                                      insetTop: CGFloat,
                                      insetBottom: CGFloat)->Void{
        let themeColor = CustomUserDefaults.shared.getThemeColor()
        
        if(themeColor == CustomUserDefaults.shared.RED_THEME){
            button.backgroundColor = UIColor(string: Constant.sharedIns.color_red_main)
        } else if(themeColor == CustomUserDefaults.shared.ORANGE_THEME){
            button.backgroundColor = UIColor(string: Constant.sharedIns.color_orange_main)
        } else{
            button.backgroundColor = UIColor(string: Constant.sharedIns.color_green_main)
        }
        
        button.layer.cornerRadius = 8
        button.setTitleColor(UIColor.white, for: .normal)
        button.clipsToBounds = true
        
        button.contentEdgeInsets = UIEdgeInsets(top: insetTop, left: insetLeft, bottom: insetBottom, right: insetRight)
    }
    
    func setRoundedViewByThemeColor(view: UIView)->Void{
        let themeColor = CustomUserDefaults.shared.getThemeColor()
        
        if(themeColor == CustomUserDefaults.shared.RED_THEME){
            view.backgroundColor = UIColor(string: Constant.sharedIns.color_red_main)
        } else if(themeColor == CustomUserDefaults.shared.ORANGE_THEME){
            view.backgroundColor = UIColor(string: Constant.sharedIns.color_orange_main)
        } else{
            view.backgroundColor = UIColor(string: Constant.sharedIns.color_green_main)
        }
        
        
        view.layer.cornerRadius = 12
        view.clipsToBounds = true
    }
    
    func setBackgroundViewByThemeColor(view: UIView){
        let themeColor = CustomUserDefaults.shared.getThemeColor()
        
        if(themeColor == CustomUserDefaults.shared.RED_THEME){
            view.backgroundColor = UIColor(string: Constant.sharedIns.color_red_main)
        } else if(themeColor == CustomUserDefaults.shared.ORANGE_THEME){
            view.backgroundColor = UIColor(string: Constant.sharedIns.color_orange_main)
        } else{
            view.backgroundColor = UIColor(string: Constant.sharedIns.color_white)
        }
    }
    
    func setRoundedViewWithBorder(view: UIView, borderColor: String, bgColor: String)->Void{
        view.layer.borderWidth = 0.8
        view.layer.borderColor = UIColor(string: borderColor).cgColor
        view.layer.cornerRadius = 8
        view.backgroundColor = UIColor(string: bgColor)
        view.clipsToBounds = true
        
        //view.contentEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
    }
    
    
    func setRoundedButtonWithBorder(button: UIButton, borderColor: String, textColor: String, bgColor: String, isBold: Bool)->Void{
        button.layer.borderWidth = 0.8
        button.layer.borderColor = UIColor(string: borderColor).cgColor
        button.layer.cornerRadius = 8
        button.backgroundColor = UIColor(string: bgColor)
        button.setTitleColor(UIColor(string: textColor), for: .normal)
        button.clipsToBounds = true
        
        button.contentEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        //
        //        if(isBold){
        //            button.titleLabel?.font = UIFont.boldSystemFont(ofSize: UIFont.systemFontSize)
        //        }
    }
    
    func isConnectingInternet()->Bool{
        if Reachability.isInternetAvailable() {
            return true
        }else{
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_no_internet_connection, actionButton1: "OK", actionButton2: "")
            
            return false
        }
    }
    
    func showHideStatusBar(isShow: Bool){
        ///Show Status Bar when start close drawer
        if(isShow){
            if let window = UIApplication.shared.keyWindow {
                window.windowLevel = UIWindow.Level.normal
            }
        }else{
            if let window = UIApplication.shared.keyWindow {
                window.windowLevel = UIWindow.Level.statusBar
            }
        }
    }
    
    func openWebViewPage(url: String){
//        let storyboard = UIStoryboard(name: Constant.sharedIns.OTHERS_STORYBOARD, bundle: nil)
        
        ///First way
        //let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.WEBVIEW_ID) as! UINavigationController
        //let vc = navCon.viewControllers.first as! privasi
        //present(vc, animated: true, completion: nil)
        
        ///Second way
//        let vc = storyboard.instantiateViewController(withIdentifier: "CommonWebView") as! WebViewController
//        vc.url = url
//        let navCon = UINavigationController(rootViewController: vc)
//        
//        present(navCon, animated: true, completion: nil)
    }
    
    func getCompleteDateString(dateString : String?) -> String{
        let dateFormatter = DateFormatter()
        
        if(dateString != nil){
            dateFormatter.dateFormat = "dd/MM/yyyy"
            let date = dateFormatter.date(from: dateString!)
            
            dateFormatter.locale = NSLocale(localeIdentifier: "id_ID") as Locale
            dateFormatter.dateStyle = .full
            dateFormatter.timeStyle = .none
            return dateFormatter.string(from: date!)
        }else{
            return ""
        }
    }
    
    func getMediumDateString(dateString : String?) -> String{
        let dateFormatter = DateFormatter()
        
        if(dateString != nil){
            dateFormatter.dateFormat = "dd/MM/yyyy"
            let date = dateFormatter.date(from: dateString!)
            
            dateFormatter.locale = NSLocale(localeIdentifier: "id_ID") as Locale
            dateFormatter.dateStyle = .medium
            dateFormatter.timeStyle = .none
            return dateFormatter.string(from: date!)
        }else{
            return ""
        }
    }
    
    func setLabelOtherResult(label: UILabel, text : String, parentView : UIView){
        label.frame = CGRect(origin: .zero, size: CGSize(width: parentView.frame.width - 32, height: parentView.frame.height / 2))
        label.textColor = UIColor(string: Constant.sharedIns.color_grey_700)
        label.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.semibold)
        label.numberOfLines = 2
        label.lineBreakMode = .byWordWrapping
        label.text = text
        
        let labelSize = label.sizeThatFits(CGSize(width: label.frame.width, height: CGFloat.greatestFiniteMagnitude))
        
        label.textAlignment = .center
        
        label.frame.size.width = labelSize.width
        label.frame.size.height = labelSize.height
        label.frame.origin.x = (parentView.frame.width / 2) - (labelSize.width / 2)
        label.frame.origin.y = (parentView.frame.height / 2) - (labelSize.height / 2)
        
        label.removeFromSuperview()
        parentView.addSubview(label)
    }
}

extension UIImage {
    func getImageByThemeColor() -> UIImage{
        let themeColor = CustomUserDefaults.shared.getThemeColor()
        
        if(themeColor == CustomUserDefaults.shared.RED_THEME){
            return self.tintImage(color: UIColor(string: Constant.sharedIns.color_red_main))!
        }else if(themeColor == CustomUserDefaults.shared.ORANGE_THEME){
            return self.tintImage(color: UIColor(string: Constant.sharedIns.color_orange_main))!
        }else{
            return self.tintImage(color: UIColor(string: Constant.sharedIns.color_green_main))!
        }
    }
    
    func resizeUIImage(targetSize: CGSize) -> UIImage {
        var finalWidth = targetSize.width
        var finalHeight = targetSize.height
        
        if (finalWidth > 0 && finalHeight > 0) {
            print("IMAGE_SIZE: \(size.width) - \(size.height)")
            
            let width = size.width
            let height = size.height
            
            var ratioImg : CGFloat!
            
            if width > height{
                ratioImg = height / width
                finalHeight = finalHeight * ratioImg
                
            }else{
                ratioImg = width / height
                if width > targetSize.width{
                    finalWidth = finalWidth * ratioImg
                }
            }
            
            print("IMAGE_SIZE_2: \(finalWidth) - \(finalHeight)")
            
            let newSize = CGSize(width: finalWidth, height: finalHeight)
            UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
            draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: newSize))
            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return newImage!
            
            //            let horizontalRatio = finalWidth / self.size.width
            //            let verticalRatio = finalHeight / self.size.height
            //
            //            let ratio = max(horizontalRatio, verticalRatio)
            //
            //            let newSize = CGSize(width: self.size.width * ratio, height: self.size.height * ratio)
            //            UIGraphicsBeginImageContextWithOptions(newSize, true, 0)
            //            draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: newSize))
            //            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            //            UIGraphicsEndImageContext()
            //
            //            return newImage!
        } else {
            return self
        }
    }
    
    //    func updateImageOrientionUpSide(image: UIImage) -> UIImage? {
    //        if image.imageOrientation == .up {
    //            return image
    //        }
    //
    //        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
    //        image.draw(in: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height))
    //        if let normalizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext() {
    //            UIGraphicsEndImageContext()
    //            return normalizedImage
    //        }
    //        UIGraphicsEndImageContext()
    //        return nil
    //    }
    
    func fixedOrientation() -> UIImage {
        
        if (self.imageOrientation == .up) {
            return self
        }
        
        var transform: CGAffineTransform = CGAffineTransform.identity
        
        
        if ( self.imageOrientation == .left || self.imageOrientation == .leftMirrored ) {
            transform = transform.translatedBy(x: self.size.width, y: 0)
            transform = transform.rotated(by: CGFloat(Double.pi / 2.0))
        } else if ( self.imageOrientation == .right || self.imageOrientation == .rightMirrored ) {
            transform = transform.translatedBy(x: 0, y: self.size.height);
            transform = transform.rotated(by: CGFloat(-Double.pi / 2.0));
        } else if ( self.imageOrientation == .down || self.imageOrientation == .downMirrored ) {
            transform = transform.translatedBy(x: self.size.width, y: self.size.height)
            transform = transform.rotated(by: CGFloat(Double.pi))
        } else if ( self.imageOrientation == .upMirrored || self.imageOrientation == .downMirrored ) {
            transform = transform.translatedBy(x: self.size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        } else if ( self.imageOrientation == .leftMirrored || self.imageOrientation == .rightMirrored ) {
            transform = transform.translatedBy(x: self.size.height, y: 0);
            transform = transform.scaledBy(x: -1, y: 1);
        }
        
        
        if let context: CGContext = CGContext(data: nil, width: Int(self.size.width), height: Int(self.size.height),
                                              bitsPerComponent: self.cgImage!.bitsPerComponent, bytesPerRow: 0,
                                              space: self.cgImage!.colorSpace!,
                                              bitmapInfo: self.cgImage!.bitmapInfo.rawValue) {
            
            context.concatenate(transform)
            
            if ( self.imageOrientation == UIImage.Orientation.left ||
                self.imageOrientation == UIImage.Orientation.leftMirrored ||
                self.imageOrientation == UIImage.Orientation.right ||
                self.imageOrientation == UIImage.Orientation.rightMirrored ) {
                context.draw(self.cgImage!, in: CGRect(x: 0,y: 0,width: self.size.height,height: self.size.width))
            } else {
                context.draw(self.cgImage!, in: CGRect(x: 0,y: 0,width: self.size.width,height: self.size.height))
            }
            
            if let contextImage = context.makeImage() {
                return UIImage(cgImage: contextImage)
            }
            
        }
        
        return self
    }
    
    
    func tintImage(color: UIColor) -> UIImage? {
        
        //BETTER RESULT
        UIGraphicsBeginImageContextWithOptions(self.size, false, UIScreen.main.scale)
        let context = UIGraphicsGetCurrentContext()
        
        color.setFill()
        
        context!.translateBy(x: 0, y: self.size.height)
        context!.scaleBy(x: 1.0, y: -1.0)
        
        context!.setBlendMode(CGBlendMode.colorBurn)
        let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
        context!.draw(self.cgImage!, in: rect)
        
        context!.setBlendMode(CGBlendMode.sourceIn)
        context!.addRect(rect)
        context!.drawPath(using: CGPathDrawingMode.fill)
        
        let coloredImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return coloredImage
    }
    
}

extension UIImageView {
    func tintImageViewColor(color : UIColor) {
        self.image = self.image!.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        self.tintColor = color
    }
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
        }.resume()
    }
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}


struct AppFontName {
    static let regular = "HelveticaNeue-Light"
    static let bold = "HelveticaNeue-Medium"
    static let italic = "HelveticaNeue-ThinItalic"
}

extension UIFont {
    
    @objc class func setFontSizeRegular(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: AppFontName.regular, size: size)!
    }
    
    @objc class func setFontSizeBold(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: AppFontName.bold, size: size)!
    }
    
    @objc class func setFontSizeItalic(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: AppFontName.italic, size: size)!
    }
}

public extension UIDevice {
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
        case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
        case "iPhone10,3", "iPhone10,6":                return "iPhone X"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad6,11", "iPad6,12":                    return "iPad 5"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4":                      return "iPad Pro 9.7 Inch"
        case "iPad6,7", "iPad6,8":                      return "iPad Pro 12.9 Inch"
        case "iPad7,1", "iPad7,2":                      return "iPad Pro 12.9 Inch 2. Generation"
        case "iPad7,3", "iPad7,4":                      return "iPad Pro 10.5 Inch"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
}


public extension UIButton {
    func setImageOnRightButtonSide(imageColor: String){
        let icNext = UIImage(named: "ic_navigate_next")?.withRenderingMode(.alwaysTemplate)
        
        self.setImage(icNext, for: .normal)
        self.tintColor = UIColor(string: imageColor)
        self.titleEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        
        
        //        self.imageView?.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -8.0).isActive = true
        //        self.imageView?.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0.0).isActive = true
        //        self.translatesAutoresizingMaskIntoConstraints = false
        //        self.imageView?.translatesAutoresizingMaskIntoConstraints = false
        
        
        self.titleLabel?.sizeToFit()
        let widthTitleLabel : CGFloat = (self.titleLabel?.frame.width)!
        let widthButton : CGFloat = self.frame.width
        
        var imageLeftInset : CGFloat = 0
        
        if(widthTitleLabel > widthButton){
            imageLeftInset = (widthTitleLabel - widthButton) + 8
        }else{
            imageLeftInset = (widthButton - widthTitleLabel) + 8
        }
        
        print("WIDTH_LABEL: \(widthTitleLabel)  \(widthButton)")
        
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: imageLeftInset, bottom: 0, right: 0)
        
    }
}

public extension UILabel {
    public func setLabelColor(){
        let themeColor = CustomUserDefaults.shared.getThemeColor()
        
        if(themeColor == CustomUserDefaults.shared.RED_THEME){
            self.textColor = UIColor(string: Constant.sharedIns.color_red_main)
        } else if(themeColor == CustomUserDefaults.shared.ORANGE_THEME){
            self.textColor = UIColor(string: Constant.sharedIns.color_orange_main)
        } else{
            self.textColor = UIColor(string: Constant.sharedIns.color_green_main)
        }
        
    }
    
}

public extension UITextView {
}

public extension UIView {
   func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}

//shared helpers
private func colorToHexString(color: UIColor) -> String {
    var r:CGFloat = 0
    var g:CGFloat = 0
    var b:CGFloat = 0
    var a:CGFloat = 0
    
    color.getRed(&r, green: &g, blue: &b, alpha: &a)
    
    let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
    
    return String(format:"#%06x", rgb)
}


extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x,
                                                     y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
}

extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}

extension String {
    var expression: NSExpression {
        return NSExpression(format: self)
    }
    
    var isReallyEmpty: Bool {
        return self.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
    }
    
//    var isNumeric: Bool {
//        guard self.count > 0 else { return false }
//        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
//        return Set(self.characters).isSubset(of: nums)
//    }
    
    var stripped: String {
        return self.components(separatedBy: CharacterSet.symbols).joined(separator: "")
    }
}

extension NSMutableAttributedString {
    var fontSize:CGFloat { return 15 }
    var boldFont:UIFont { return UIFont(name: "AvenirNext-Bold", size: fontSize) ?? UIFont.boldSystemFont(ofSize: fontSize) }
    var normalFont:UIFont { return UIFont(name: "AvenirNext-Regular", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)}
    
    func bold(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font : boldFont
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func normal(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font : normalFont,
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    /* Other styling methods */
    func orangeHighlight(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .foregroundColor : UIColor.white,
            .backgroundColor : UIColor.orange
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func blackHighlight(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .foregroundColor : UIColor.white,
            .backgroundColor : UIColor.black
            
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func underlined(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .underlineStyle : NSUnderlineStyle.single.rawValue
            
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
}

extension Notification.Name{
    static let NOTIF_OPEN_FLEXIFAST = Notification.Name("openFlexiFast")
    static let NOTIF_OPEN_MY_DREAM = Notification.Name("openMyDream")
    static let NOTIF_OPEN_CONTRACT = Notification.Name("openContract")
    static let NOTIF_OPEN_INST_SCHEDULE = Notification.Name("openInstallmentSchedule")
    static let NOTIF_OPEN_HOW_TO_PAY = Notification.Name("openHowToPay")
    static let NOTIF_OPEN_PAYMENT_LOC = Notification.Name("openPaymentLoc")
    static let NOTIF_SELECT_DRAWER_POS = Notification.Name("selectDrawerPos")
    
    static let NOTIF_OPEN_GENERAL_SIMULATION = Notification.Name("openGeneralSimulation")
    static let NOTIF_OPEN_POS_LOCATOR = Notification.Name("openPOSLocator")
    static let NOTIF_OPEN_E_SHOPS = Notification.Name("openEShops")
    static let NOTIF_OPEN_OUR_GUARANTEE = Notification.Name("openOurGuarantee")
    static let NOTIF_SET_CURR_PAGE_CONTRACT = Notification.Name("setCurrentPage")
    static let NOTIF_SET_ACTIVE_CONTRACT = Notification.Name("setActiveContract")
    static let NOTIF_SET_FINISHED_CONTRACT = Notification.Name("setFinishedContract")
    static let NOTIF_SET_SUBMISSION_CONTRACT = Notification.Name("setSubmissionContract")
    
    static let NOTIF_RELOAD_PROFILE_PIC = Notification.Name("reloadProfilePic")
}

extension NSLayoutConstraint{
    func constraintWithMultiplier(_ multiplier: CGFloat) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: self.firstItem, attribute: self.firstAttribute, relatedBy: self.relation, toItem: self.secondItem, attribute: self.secondAttribute, multiplier: multiplier, constant: self.constant)
    }
}

extension Bool {
    mutating func negate() {
        self = !self
    }
}

extension Date {
    var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    
    var sixthDay: Date {
        return Calendar.current.date(byAdding: .day, value: 6, to: noon)!
    }
    
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return tomorrow.month != month
    }
    static func getLastDates(forLastNDays nDays: Int) -> [String] {
        let cal = NSCalendar.current
        // start with today
        var date = cal.startOfDay(for: Date())
        
        var arrDates = [String]()
        var reversedDays = [String]()
        
        for _ in 1 ... nDays {
            // move back in time by one day:
            date = cal.date(byAdding: Calendar.Component.day, value: -1, to: date)!
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyy"
            let dateString = dateFormatter.string(from: date)
            arrDates.append(dateString)
        }
        for arrayIndex in stride(from: arrDates.count - 1, through: 0, by: -1) {
            reversedDays.append(arrDates[arrayIndex])
        }
        return reversedDays
    }
    static func getFutureDates(forFutureNDays nDays: Int) -> [String] {
        let cal = NSCalendar.current
        // start with today
        var date = cal.startOfDay(for: Date())
        
        var arrDates = [String]()
        
        for _ in 1 ... nDays {
            // move back in time by one day:
            date = cal.date(byAdding: Calendar.Component.day, value: 1, to: date)!
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyy"
            let dateString = dateFormatter.string(from: date)
            arrDates.append(dateString)
        }
        return arrDates
    }
    static func getTodayDates() -> [String] {
        let cal = NSCalendar.current
        // start with today
        var date = Date()
        var arrDates = [String]()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyy"
        let dateString = dateFormatter.string(from: date)
        arrDates.append(dateString)
        return arrDates
    }
    
    func interval(ofComponent comp: Calendar.Component, fromDate date: Date) -> Int {
        
        let currentCalendar = Calendar.current
        
        guard let start = currentCalendar.ordinality(of: comp, in: .era, for: date) else { return 0 }
        guard let end = currentCalendar.ordinality(of: comp, in: .era, for: self) else { return 0 }
        
        return end - start
    }
    
    var startOfWeek: Date? {
        var gregorian = Calendar(identifier: .gregorian)
        gregorian.firstWeekday = 1
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: 1, to: sunday)
    }
    
    var endOfWeek: Date? {
        var gregorian = Calendar(identifier: .gregorian)
        gregorian.firstWeekday = 1
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: 7, to: sunday)
    }
    
    var ticks: UInt64 {
        return UInt64((self.timeIntervalSince1970 + 62_135_596_800) * 10_000_000)
    }
    
    init(reverseTicks: UInt64) {
        self.init(timeIntervalSince1970: Double(reverseTicks)/10_000_000 - 62_135_596_800)
    }
}

extension Collection where Indices.Iterator.Element == Index {
    subscript (safe index: Index) -> Iterator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

extension Array where Element: Equatable {
    func indexes(of element: Element) -> [Int] {
        return self.enumerated().filter({ element == $0.element }).map({ $0.offset })
    }
}

extension Array where Element: Hashable {
    func difference(from other: [Element]) -> [Element] {
        let thisSet = Set(self)
        let otherSet = Set(other)
        return Array(thisSet.symmetricDifference(otherSet))
    }
}
