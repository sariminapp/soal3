//
//  Constant.swift
//  Arah
//
//  Created by mac on 4/5/16.
//  Copyright © 2016 Primatek Infocom. All rights reserved.
//

import UIKit
import MessageUI
import Foundation

#if DEVELOPMENT
    let uriBase_api: String = "http://kemalaorganik.com/KemalaOrganics/api/"
    let imageBase_api: String = "https://pokeapi.co/api/"
#elseif PRODUCTION
    let uriBase_api: String = "http://kemalaorganik.com/KemalaOrganics/api/"
    let imageBase_api: String = "https://pokeapi.co/api/"
#else
    let uriBase_api: String = "http://kemalaorganik.com/KemalaOrganics/api/"
    let imageBase_api: String = "https://pokeapi.co/api/"
#endif

let loginAPI: String = "api.php?action=loginClient"
let versionAPI: String = "v2/"
let pokemonAPI: String = "pokemon/"
let pokemonLimitAPI: String = "?limit="
let limit: Int = 5

class Constant {
    
    static let sharedIns = Constant()
    
    //FONT SIZE
    let XX_SMALL_FONT_SIZE : CGFloat = 8
    let X_SMALL_FONT_SIZE : CGFloat = 10
    let SMALL_FONT_SIZE : CGFloat = 12
    let NORMAL_FONT_SIZE : CGFloat = 14
    let MEDIUM_NORMAL_FONT_SIZE : CGFloat = 16
    let MEDIUM_FONT_SIZE : CGFloat = 18
    let MEDIUM_LARGE_FONT_SIZE : CGFloat = 20
    let LARGE_FONT_SIZE : CGFloat = 22
    let X_LARGE_FONT_SIZE : CGFloat = 24
    let XX_LARGE_FONT_SIZE : CGFloat = 26
    let XXX_LARGE_FONT_SIZE : CGFloat = 28
    let SUPER_LARGE_FONT_SIZE : CGFloat = 34
    let HYPER_LARGE_FONT_SIZE : CGFloat = 40
    
    
    //COLORS CODE
    //RED
    let color_red_500 : String = "#F44336"
    //let color_red_main: String = "#e80c00"
    let color_red_main : String = "#E11931"
    let color_red_maroon : String = "#800000"
    
    
    //ORANGE
    let color_orange_200: String = "#ffcc80"
    let color_orange_300: String = "#ffb74d"
    let color_orange_400: String = "#ffa726"
    let color_orange_500: String = "#ff9800"
    let color_orange_600: String = "#FB8C00"
    let color_orange_700: String = "#f57c00"
    let color_orange_800: String = "#ef6c00"
    let color_orange_900: String = "#e65100"
    let color_orange_main : String = "FF9900"
    
    //GREEN
    let color_green_500: String = "#34B44A"
    let color_green_700: String = "#388e3c"
    let color_green_800: String = "#2e7d32"
    let color_green_common: String = "#33B04A"
    let color_green_main : String = "#33b44a"
    
    //GRAY
    
    let color_grey_100: String = "#F5F5F5"
    let color_grey_200: String = "#EEEEEE"
    let color_grey_300: String = "#E0E0E0"
    let color_grey_400: String = "#CBCBCB"
    let color_grey_500: String = "#9E9E9E"
    let color_grey_600: String = "#757575"
    let color_grey_700: String = "#616161"
    let color_grey_800: String = "#424242"
    let color_grey_300_alpha: String = "#1A9E9E9E"
    let color_grey_400_alpha: String = "#80E0E0E0"
    let color_grey_500_alpha: String = "#809E9E9E"
    let color_grey_800_alpha: String = "#80424242"
    let color_grey_gainsboro: String = "#DCDCDC"
    
    
    //BLACK
    let color_black : String = "000000"
    
    //WHITE
    let color_white: String = "#FFFFFF"
    let color_white_alpha_70 : String = "#B3FFFFFF"
    let color_white_alpha_60 : String = "#99FFFFFF"
    let color_white_alpha_50 : String = "#80FFFFFF"
    let color_white_alpha_40 : String = "#66FFFFFF"
    let color_white_alpha_30 : String = "#4DFFFFFF"
    
    //BLUE
    let color_blue : String = "0097a8"
    let color_blue_main : String = "#007AFF"
    
    //FLEXIFAST
    let color_flexi_fast: String = "#81b7dc"
    
    //PUSH MESSAGE
    let color_promotion = "#5ca9df"
    let color_payment = "#FB8C00"
    let color_survey = "#1aa38c"
    let color_quiz = "#b969c7"
    
    
    let INITIAL_STORYBOARD = "Initial"
    let MAIN_STORYBOARD = "Main"
    let HOMEPAGE_STORYBOARD = "Homepage"
    let NOTIFICATION_STORYBOARD = "Notification"
    let MAINMENU_NAV = "MainMenuNav"
    let LOGIN_NAV = "LoginNav"
    let CODEVER_NAV = "CodeVerificationNav"
    let PROFILE_NAV = "ProfileNav"
    let HOMEPAGE_NAV = "HomepageNav"
    let NOTIFICATION_NAV = "NotificationNav"
    let CHAT_NAV = "ChatNav"
    let ADD_CONSULTATION_NAV = "AddConsultationNav"
    
    class func getAPILogin() -> String {
        let uri = uriBase_api + loginAPI
        return uri
    }
    
    class func getAPIPokemonLimit() -> String {
        let uri = imageBase_api + versionAPI + pokemonAPI + pokemonLimitAPI + "\(limit)"
        return uri
    }
    
    class func convertMilisecondToDays(milliseconds: String) -> String {
        var res = ""
        
        if milliseconds == "2592000000" {
            res = "30"
        }
        else if milliseconds == "1209600000" {
            res = "14"
        }
        else if milliseconds == "604800000" {
            res = "7"
        }
        else if milliseconds == "86400000" {
            res = "1"
        }
        
        
        return "\(res)"
    }
    
    class func convertDateStringFormat(date: Date, format: String) -> String {
        
        let dateConvert = date
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat =  format  //"MMMM yyyy"    //"EEEE, h a"
        
        let dateStr = dayTimePeriodFormatter.string(from: dateConvert as Date)
        
        return dateStr
    }
    
    class func getDateNow(format: String) -> String {
        
        let dateConvert = Date()
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat =  format  //"MMMM yyyy"    //"EEEE, h a"
        
        let dateStr = dayTimePeriodFormatter.string(from: dateConvert as Date)
        
        return dateStr
    }
    
    class func convertStringToDate(dateString: String!) -> Date {
        let dateFormatter = DateFormatter()
        //dateFormatter.timeZone = NSTimeZone(abbreviation: "WIB")! as TimeZone
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "d MMMM yyyy"
        dateFormatter.locale = Locale(identifier: "id_ID")
        print("dateString \(dateString)")
        
        let date: Date = dateFormatter.date(from: dateString)! as Date
        return date
    }
    
    class func convertStringToDatewithFormat(dateString: String!, format: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale(identifier: "en_GB")
        print("dateString \(dateString!)")
        let date: Date = dateFormatter.date(from: dateString!)! as Date
        print("convertStringToDatewithFormat:\(date)")
        return date
    }
    
    func setStringToServerDate(dateString: String) -> Date{
        //Server date as GMT
        let serverDateFormat = DateFormatter()
        serverDateFormat.dateFormat = "EEE, dd MMM yyyy HH:mm:ss Z"
        serverDateFormat.timeZone = TimeZone(abbreviation: "GMT")
        
        //Convert to local timezone
        guard let date = serverDateFormat.date(from: dateString) else{
            return Date.init()
        }
        
        let localDateFormat = DateFormatter()
        localDateFormat.dateFormat = "dd/MM/yyyy"
        localDateFormat.timeZone = TimeZone.current
        
        let localDateString = localDateFormat.string(from: date as Date)
        
        guard let localDate = localDateFormat.date(from: localDateString) else {
            return Date.init()
        }
        
        print("DATE_TEMP: \(localDateString)")
        
        return localDate as Date
    }
    
    class func changeToLocalDate(dateString: String!) -> String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.date(from: dateString!)
        
        let outputDatedateFormatter = DateFormatter()
        outputDatedateFormatter.dateFormat = "dd MMMM yyyy"
        //leave the time zone at the default (user's time zone)
        let displayString = outputDatedateFormatter.string(from: date!)

        return displayString
    }
    
    class func convertStringToMonthYear(dateString: String) -> String {
        
        let dateConvert = convertStringToDate(dateString: dateString)
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMMM yyyy"    //"EEEE, h a"
        
        let dateStr = dayTimePeriodFormatter.string(from: dateConvert as Date)
        
        return dateStr
    }
    
    class func convertStringToMonthOnly(dateString: String) -> String {
        
        let dateConvert = convertStringToDate(dateString: dateString)
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMMM"    //"EEEE, h a"
        
        let dateStr = dayTimePeriodFormatter.string(from: dateConvert as Date)
        
        return dateStr
    }
    
    class func convertDateStringtoAnotherFormat(dateString: String, fromFormat: String, toFormat: String) -> String {
        let inputDateFormatter = DateFormatter()
        inputDateFormatter.dateFormat = fromFormat
//        inputDateFormatter.locale = Locale(identifier: "id_ID")
        let date = inputDateFormatter.date(from: dateString)
        
        let outputDateFormatter = DateFormatter()
        outputDateFormatter.dateFormat = toFormat
        let result = outputDateFormatter.string(from: date!)
        return result
    }
    
    class func countAge(birthday: String!) -> Int {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "dd-MM-yyyy"
        print("birthday: \(birthday)")
        let birthdate: Date = dateFormatter.date(from: birthday)! as Date
        let now = Date()
        let birthdays: Date = birthdate
        let calendar = Calendar.current
        let ageComponents = calendar.dateComponents([.year], from: birthdays, to: now)
        let age = ageComponents.year!
        
        return age
    }
    
    class func currentTimeMillis() -> Int64{
        let nowDouble = NSDate().timeIntervalSince1970
        return Int64(nowDouble*1000) + Int64(nowDouble/1000)
    }
    
    class func checkDeviceIsIpad() -> Bool {
        var result = false
        if UIDevice.current.userInterfaceIdiom == .pad {
            result = true
        }
        /*
        UIDevice.currentDevice().userInterfaceIdiom == .Phone
        UIDevice.currentDevice().userInterfaceIdiom == .Unspecified
        */
        return result
    }
    
    /*class func getDeviceImei() -> String {
        return UIDevice.currentDevice.identifierForVendor!.UUIDString
    }*/
    class func configuredMailComposeViewController(delegate: MFMailComposeViewControllerDelegate) -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = delegate //self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["someone@somewhere.com"])
        mailComposerVC.setSubject("Sending you an in-app e-mail...")
        mailComposerVC.setMessageBody("Sending e-mail in-app is not so bad!", isHTML: false)
        
        return mailComposerVC
    }
    
    class func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email",
            message: "Your device could not send e-mail.  Please check e-mail configuration and try again.",
            delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    // ---------------------------------------------------------
    class func getMainStoryboard() -> UIStoryboard {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil);
        
        return storyboard
    }
    class func uicolorFromHex(rgbValue:UInt32)->UIColor{
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:1.0)
    }
    
    class func sizeToFillWidthOfSize(size:CGSize) -> CGSize {
        
        let imageSize = CGSize(width: 110, height: 110)
        var returnSize = size
        
        let aspectRatio = imageSize.width / imageSize.height
        
        returnSize.height = returnSize.width / aspectRatio
        
        if returnSize.height > size.height {
            returnSize.height = size.height
            returnSize.width = size.height * aspectRatio
        }
        
        return returnSize
    }
    class func isValidEmail(testStr:String) -> Bool {
        print("validate emilId: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    class func isValidPhoneNumber(value: String) -> Bool {
        //let PHONE_REGEX = "^\\d{4}-\\d{8}$"
        let PHONE_REGEX = "^((\\+)|(62))[0-9]{11,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    class func currencyNominal(value: Int) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        numberFormatter.locale = Locale(identifier: "id_ID")
        let formattedNumber = numberFormatter.string(from: NSNumber(value:value))
        return formattedNumber!
    }
    class func verifyUrl (urlString: String?) -> Bool {
        if let urlString = urlString {
            if let url  = NSURL(string: urlString) {
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    class func getDayOfWeek()->Int? {
        let todayDate = NSDate()
        let myCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier(rawValue: NSGregorianCalendar))
        let myComponents = myCalendar?.components(.weekday, from: todayDate as Date)
        let weekDay = myComponents?.weekday
        return weekDay
    }
    class func GUIDString() -> NSString {
        let newUniqueID = CFUUIDCreate(kCFAllocatorDefault)
        let newUniqueIDString = CFUUIDCreateString(kCFAllocatorDefault, newUniqueID);
        let guid = newUniqueIDString as! NSString
        
        return guid.lowercased as NSString
    }
    class func getDeviceImei() -> String {
        return (UIDevice.current.identifierForVendor?.uuidString)!
    }
    class func colorWithHexString (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substring(from: 1)
        }
        
        if (cString.count != 6) {
            return UIColor.gray
        }
        
        var rString = (cString as NSString).substring(to: 2)
        var gString = ((cString as NSString).substring(from: 2) as NSString).substring(to: 2)
        var bString = ((cString as NSString).substring(from: 4) as NSString).substring(to: 2)
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string: gString).scanHexInt32(&g)
        Scanner(string: bString).scanHexInt32(&b)
        
        
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
    }
    class func deviceRemainingFreeSpaceInBytes() -> Int64? {
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last!
        guard
            let systemAttributes = try? FileManager.default.attributesOfFileSystem(forPath: documentDirectory),
            let freeSize = systemAttributes[.systemFreeSize] as? NSNumber
            else {
                // something failed
                return nil
        }
        return freeSize.int64Value
    }
    class func getTotalSize() -> Int64?{
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        if let dictionary = try? FileManager.default.attributesOfFileSystem(forPath: paths.last!) {
            if let freeSize = dictionary[FileAttributeKey.systemSize] as? NSNumber {
                return freeSize.int64Value
            }
        }else{
            print("Error Obtaining System Memory Info:")
        }
        return nil
    }
    
    enum BackgroundLayout: String {
        case HeaderLogo = "logo" //"HeaderLogo850Small"
        //case BackgroundLogin = "LoginBackgroundSmall"
        case HeaderLogoLandscape = "logoLandscape"
        case BackgroundLoginSignUp = "bgSignup"
        case BackgroundOrange = "headerOrange"
        case BackgroundOrangeLandscape = "orangeLandscape"
        case BackgroundLandscapeSignUp = "bgSignupLandscape"
        case BackgroundLandscapeSplash = "bgSplashLandscape"
        case HeaderResult = "headerResult"
        case HeaderResultLandscape = "headerResultL"
        
    }
}

