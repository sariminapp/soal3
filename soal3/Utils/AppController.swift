//
//  AppController.swift
//  homecredit
//
//  Created by Oscar Perdanakusuma - Private on 8/15/17.
//  Copyright © 2017 Home Credit Indonesia. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications

class AppController{
    
    static let sharedInstance = AppController()
    
    let API_KEY_GMS_SERVICES = "AIzaSyAMtVm_-F5aRddkaOtqZ9FY5GPoas3FWjQ"
    let API_KEY_GMS_WEB_PLACES = "AIzaSyArFKtV5-nyVgiwnKplfkPrUOrNQFV1u6I"
    let API_KEY_GMS_PLACES = "AIzaSyAMtVm_-F5aRddkaOtqZ9FY5GPoas3FWjQ"
    
    func isLoggedIn() -> Bool{
        if(CustomUserDefaults.shared.getRefreshToken().isEmpty
        && CustomUserDefaults.shared.getPhoneNumber().isEmpty
            && CustomUserDefaults.shared.getAccessToken().isEmpty){
            return false
        }else{
            return true
        }
    }
    
    func setBadgeIconNumber(){
        let badgeCount = CustomUserDefaults.shared.getUnreadCount()
        CustomUserDefaults.shared.setUnreadCount(badgeCount + 1)
        UIApplication.shared.applicationIconBadgeNumber = badgeCount + 1
    }
    
    func setBadgeIconNumber(count: Int){
        //let badgeCount = CustomUserDefaults.shared.getUnreadCount()
        CustomUserDefaults.shared.setUnreadCount(count)
        UIApplication.shared.applicationIconBadgeNumber = count
    }
    
    func logout() -> Void{
        setClearGlobalObject()
        CustomUserDefaults.shared.deleteUserData()
        let storyboard = UIStoryboard(name: Constant.sharedIns.MAIN_STORYBOARD, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.LOGIN_NAV)
        vc.modalPresentationStyle = .fullScreen
        UIApplication.topViewController()?.present(vc, animated: true, completion: nil)
        
        /// Remove all notifications
        UIApplication.shared.unregisterForRemoteNotifications()
        UIApplication.shared.applicationIconBadgeNumber = 0
        UIApplication.shared.cancelAllLocalNotifications()
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().removeAllDeliveredNotifications()
            UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        } else {
            // Fallback on earlier versions
        }
    }
    
    func setClearGlobalObject(){
        
    }
}

class CustomUIButton: UIButton{
    var customInputView: UIView?
    var customInputAccessoryView: UIView?
    
    override var inputView: UIView? {
        get {
            return self.customInputView
        }

        set {
            self.customInputView = newValue
        }
    }

    override var inputAccessoryView: UIView? {
        get {
            return self.customInputAccessoryView
        }

        set {
            self.customInputAccessoryView = newValue
        }
    }
    
    override var canBecomeFirstResponder: Bool{
        return true
    }
}
