//
//  CategoryCellView.swift
//  kemala
//
//  Created by Oscar Perdanakusuma Adipati on 04/04/20.
//  Copyright © 2020 Sarimin App. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class ConsultationCellView: UITableViewCell {
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var cekObatView: UIView!
    @IBOutlet weak var lblCekObat: UILabel!
    @IBOutlet weak var imgCekObat: UIImageView!
    @IBOutlet weak var lblKlinik: UILabel!
    @IBOutlet weak var lblDoctorName: UILabel!
    @IBOutlet weak var lblDiagnosaPrimer: UILabel!
    @IBOutlet weak var lblDiagnosaSekunder: UILabel!
    @IBOutlet weak var lblTindakan: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        imgCekObat.image = imgCekObat.image!.withRenderingMode(.alwaysTemplate)
        imgCekObat.tintColor = .lightGray
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
