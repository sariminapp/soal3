//
//  HomepageViewController.swift
//  soal3
//
//  Created by Oscar Perdanakusuma Adipati on 28/12/20.
//

import UIKit
import CoreData

class HomepageViewController: BaseParentController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var addConsultView: UIView!
    @IBOutlet weak var imgPlus: UIImageView!
    var consultDateArr = [String]()
    var klinikArr = [String]()
    var doctorArr = [String]()
    var primerArr = [String]()
    var sekunderArr = [String]()
    var tindakanArr = [String]()
    var obatArr = [String]()
    var idsaArr = [String]()
    var filteredData = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        if self.navigationController == nil {
            return
        }
        setEnableTapGestureOnMainView(isEnable: false)
        let myimage = UIImage(named: "ic_profile")?.withRenderingMode(.alwaysOriginal)
         navigationItem.rightBarButtonItem = UIBarButtonItem(image: myimage, style: .plain, target: self, action: #selector(ButtonTapped))
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        imgPlus.image = imgPlus.image!.withRenderingMode(.alwaysTemplate)
        imgPlus.tintColor = .white
        addTapEventForView(view: addConsultView, action: #selector(addClicked))
        let yourViewBorder = CAShapeLayer()
        yourViewBorder.strokeColor = UIColor.white.cgColor
        yourViewBorder.lineDashPattern = [2, 2]
        yourViewBorder.frame = self.addConsultView.bounds
        yourViewBorder.fillColor = nil
        yourViewBorder.path = UIBezierPath(rect: self.addConsultView.bounds).cgPath
        self.addConsultView.layer.cornerRadius = 12
        self.addConsultView.layer.addSublayer(yourViewBorder)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.tabBarController?.tabBar.isHidden = false
            //if want like image it should be create programatically UIView, inside that UIView you must add subview of UIImageView with back image white tint and subview of UILabel, I do not have back image. Last subview that UIview inside left navigation item.
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
            self.navigationItem.title = "Hasil Konsultasi";
            UINavigationBar.appearance().isTranslucent = false
            self.callAPIConsultation(name: "All")
        }
    }

    @objc func ButtonTapped() {
        print("Button Tapped")
    }
    
    @objc func addClicked () {
        let storyboard = UIStoryboard(name: Constant.sharedIns.MAIN_STORYBOARD, bundle: nil)
        let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.ADD_CONSULTATION_NAV) as! UINavigationController
        let vc = navCon.viewControllers.first as! AddConsultationViewController
        vc.tabBarController?.tabBar.isHidden = true
        vc.purpose = "add"
        vc.idsa = ""
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func callAPIConsultation(name: String) {
        removeAll()
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequestAgent = NSFetchRequest<NSManagedObject>(entityName: "Consultation")
        if (name != "All") {
            let agentCodeKeyPredicate = NSPredicate(format: "doctorName == %@", name)
            fetchRequestAgent.predicate = agentCodeKeyPredicate
            do {
                let agents = try managedContext.fetch(fetchRequestAgent)
                if (agents.count == 0) {
                    self.firstView.isHidden = false
                    self.tableView.reloadData()
                    AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Tidak ada data", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
                } else {
                    //CoreData
                    for agent in agents {
                        idsaArr.append((agent.value(forKey: "id") as? String)!)
                        consultDateArr.append(Constant.convertDateStringFormat(date: (agent.value(forKey: "date") as? Date)!, format: "d MMM"))
                        klinikArr.append((agent.value(forKey: "hospital") as? String)!)
                        doctorArr.append((agent.value(forKey: "doctorName") as? String)!)
                        primerArr.append((agent.value(forKey: "primer") as? String)!)
                        sekunderArr.append((agent.value(forKey: "sekunder") as? String)!)
                        tindakanArr.append((agent.value(forKey: "tindakan") as? String)!)
                        obatArr.append((agent.value(forKey: "obat") as? String)!)
                    }
                    DispatchQueue.main.async {
                        self.firstView.isHidden = false
                        self.filteredData = self.doctorArr
                        self.tableView.reloadData()
                    }
                }
            } catch let error as NSError {
                print("Could not fetch. \(error), \(error.userInfo)")
            }
        } else {
            do {
                let agents = try managedContext.fetch(fetchRequestAgent)
                if (agents.count == 0) {
                    self.firstView.isHidden = true
                    AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Tidak ada data", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
                } else {
                    //CoreData
                    for agent in agents {
                        idsaArr.append((agent.value(forKey: "id") as? String)!)
                        consultDateArr.append(Constant.convertDateStringFormat(date: (agent.value(forKey: "date") as? Date)!, format: "d MMM"))
                        klinikArr.append((agent.value(forKey: "hospital") as? String)!)
                        doctorArr.append((agent.value(forKey: "doctorName") as? String)!)
                        primerArr.append((agent.value(forKey: "primer") as? String)!)
                        sekunderArr.append((agent.value(forKey: "sekunder") as? String)!)
                        tindakanArr.append((agent.value(forKey: "tindakan") as? String)!)
                        obatArr.append((agent.value(forKey: "obat") as? String)!)
                    }
                    DispatchQueue.main.async {
                        self.firstView.isHidden = false
                        self.filteredData = self.doctorArr
                        self.tableView.reloadData()
                    }
                }
            } catch let error as NSError {
                print("Could not fetch. \(error), \(error.userInfo)")
            }
        }
    }
    ///search by date interval
    func callAPIConsultationByDateInterval(startDate: String, endDate: String) {
        removeAll()
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "d MMMM yyyy"
        dateFormatter.locale = Locale(identifier: "id_ID")
        
        let startdate: Date = dateFormatter.date(from: startDate)! as Date
        let enddate: Date = dateFormatter.date(from: endDate)! as Date
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequestAgent = NSFetchRequest<NSManagedObject>(entityName: "Consultation")
        fetchRequestAgent.predicate = NSPredicate(format: "(date >= %@) AND (date <= %@)", startdate as CVarArg, enddate as CVarArg)
        let sortDescriptor = NSSortDescriptor(key: "date", ascending: false)
        fetchRequestAgent.sortDescriptors = [sortDescriptor]
        do {
            let agents = try managedContext.fetch(fetchRequestAgent)
            if (agents.count == 0) {
                self.firstView.isHidden = false
                self.tableView.reloadData()
                AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Tidak ada data", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
            } else {
                //CoreData
                for agent in agents {
                    idsaArr.append((agent.value(forKey: "id") as? String)!)
                    consultDateArr.append(Constant.convertDateStringFormat(date: (agent.value(forKey: "date") as? Date)!, format: "d MMM"))
                    klinikArr.append((agent.value(forKey: "hospital") as? String)!)
                    doctorArr.append((agent.value(forKey: "doctorName") as? String)!)
                    primerArr.append((agent.value(forKey: "primer") as? String)!)
                    sekunderArr.append((agent.value(forKey: "sekunder") as? String)!)
                    tindakanArr.append((agent.value(forKey: "tindakan") as? String)!)
                    obatArr.append((agent.value(forKey: "obat") as? String)!)
                }
                DispatchQueue.main.async {
                    self.firstView.isHidden = false
                    self.filteredData = self.doctorArr
                    self.tableView.reloadData()
                }
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func removeAll() {
        idsaArr.removeAll()
        consultDateArr.removeAll()
        klinikArr.removeAll()
        doctorArr.removeAll()
        primerArr.removeAll()
        sekunderArr.removeAll()
        tindakanArr.removeAll()
        obatArr.removeAll()
        filteredData.removeAll()
    }

}

extension HomepageViewController: UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredData.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150.0
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "consultationCell", for: indexPath) as! ConsultationCellView
        let row = indexPath.row
        if (filteredData.count > 0) {
            cell.lblDate.text = consultDateArr[row]
            cell.lblDate.textColor = .black
            cell.lblKlinik.text = klinikArr[row]
            cell.lblDoctorName.text = filteredData[row]
            cell.lblDiagnosaPrimer.text = primerArr[row]
            cell.lblDiagnosaSekunder.text = sekunderArr[row]
            cell.lblTindakan.text = tindakanArr[row]
            if (obatArr[row].isReallyEmpty == false) {
                cell.lblCekObat.textColor = .green
                cell.imgCekObat.image = cell.imgCekObat.image!.withRenderingMode(.alwaysTemplate)
                cell.imgCekObat.tintColor = .green
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let index = indexPath.row
        let storyboard = UIStoryboard(name: Constant.sharedIns.MAIN_STORYBOARD, bundle: nil)
        let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.ADD_CONSULTATION_NAV) as! UINavigationController
        let vc = navCon.viewControllers.first as! AddConsultationViewController
        vc.tabBarController?.tabBar.isHidden = true
        vc.purpose = "cek"
        vc.idsa = idsaArr[index]
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if(searchBar.text?.isReallyEmpty == false) {
            callAPIConsultation(name: searchBar.text!)
        }
    }
}

