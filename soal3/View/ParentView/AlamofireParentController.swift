//
//  AlamofireBaseParent.swift
//  homecredit
//
//  Created by Oscar Perdanakusuma on 8/17/17.
//  Copyright © 2017 Home Credit Indonesia. All rights reserved.
//

import Alamofire
import Security
import ContactsUI
import Contacts
import CoreData

class AlamofireBaseParentController: UIViewController, UITextFieldDelegate, UIGestureRecognizerDelegate, CNContactViewControllerDelegate, CNContactPickerDelegate {
    
    var activitiyViewController : ProgressDialogUtil!
    var activityIndicator = UIActivityIndicatorView()
    var errorLayout = UIStackView()
    var isFirstInit : Bool = true
    var alertView : AlertViewUtil!
    var afManager : SessionManager!
    
    var textFields = [UITextField]()
    var textViews = [UITextView]()
    var progressDialog : ProgressDialogUtil_v2!
    var feedbackDialog : FeedbackDialog!
    var activeField : UITextField?
    
    var scrollView : UIScrollView?
    
    var isViewDisappear = false
    
    let datePicker = UIDatePicker()
    
    let dateFormatter = DateFormatter()
    
    var tapGestureForMainView : UITapGestureRecognizer!
    
    var isKeyboardShown = false
    
    var totalProspect : Int!
    var prospectList: [NSManagedObject] = []
    var prospectPhoneList: [NSManagedObject] = []
    var prospectEmailList: [NSManagedObject] = []
    var prospectSpouseList: [NSManagedObject] = []
    var prospectFatherList: [NSManagedObject] = []
    var prospectMotherList: [NSManagedObject] = []
    var prospectChildrenList: [NSManagedObject] = []
    var prospectBrotherList: [NSManagedObject] = []
    var prospectSisterList: [NSManagedObject] = []
    var prospectBroList: [NSManagedObject] = []
    var prospectSisList: [NSManagedObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //print("SET DID LOAD PARENT")
        
        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.black]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [NSAttributedString.Key: Any]
        
        navigationController?.navigationBar.isTranslucent = false
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.done, target: nil, action: nil)
        
        setFlatNavBar()
        setNavBarThemeColor()
        CommonUtils.shared.checkAppVersion()
//        configureAlamoFireSSLPinning()
        self.hideKeyboardWhenTappedOutside()
    }
    
    func hideKeyboardWhenTappedOutside() {
        tapGestureForMainView = UITapGestureRecognizer(target: self, action: #selector(self.closeKeyboard))
        view.addGestureRecognizer(tapGestureForMainView)
    }
    
    func setEnableTapGestureOnMainView(isEnable: Bool){
        tapGestureForMainView.cancelsTouchesInView = isEnable
    }
    
    @objc func closeKeyboard() {
        view.endEditing(true)
    }
    
    func setNavBarThemeColor(){
        print("SET THEME COLOR")
//        navigationController?.navigationBar.barTintColor = UIColor(string: Constant.sharedIns.color_blue)
    }
    
    func setFlatNavBar(){
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    func clearNavigationBarTint(){
        navigationController?.navigationBar.isTranslucent = true
        setFlatNavBar()
        navigationController?.navigationBar.barTintColor = UIColor.clear
    }
    
    func setBackgroundThemeColor(themeColor: Int){
        print("SET BACKGROUND THEME COLOR")
        if(themeColor == CustomUserDefaults.shared.RED_THEME){
            self.view.backgroundColor = UIColor(string: Constant.sharedIns.color_red_main)
        }else if(themeColor == CustomUserDefaults.shared.ORANGE_THEME){
            self.view.backgroundColor = UIColor(string: Constant.sharedIns.color_orange_main)
        }else{
            self.view.backgroundColor = UIColor(string: Constant.sharedIns.color_green_main)
        }
    }
    
    func getCurrentThemeColor()-> UIColor{
        let themeColor = CustomUserDefaults.shared.getThemeColor()
        if(themeColor == CustomUserDefaults.shared.RED_THEME){
            return UIColor(string: Constant.sharedIns.color_red_main)
        }else if(themeColor == CustomUserDefaults.shared.ORANGE_THEME){
            return UIColor(string: Constant.sharedIns.color_orange_main)
        }else{
            return UIColor(string: Constant.sharedIns.color_green_main)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isViewDisappear = false
        
        registerForKeyboardNotifications()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        isViewDisappear = true
        deregisterFromKeyboardNotifications()
        
        print("DID_DISAPPEAR")
    }
    
    func cancelAPISession(){
        afManager.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
            //            sessionDataTask.forEach { $0.cancel() }
            sessionDataTask.forEach({task in
                if task.currentRequest?.url?.absoluteString != Constant.getAPILogin(){
                    task.cancel()
                }
            })
            
            uploadData.forEach { $0.cancel() }
            downloadData.forEach { $0.cancel() }
        }
    }
    
    func clickRegister(_ sender: AnyObject){
//        let storyboard = UIStoryboard(name: Constant.sharedIns.INITIAL_STORYBOARD, bundle: nil)
//        let nav = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.REGISTER_NAV) as! UINavigationController
//
//        let vc = nav.viewControllers.first as! RegisterController
//        navigationController?.pushViewController(vc, animated: true)
    }
    
    func clickForgotPIN(_ sender: AnyObject){
//        let storyboard = UIStoryboard(name: Constant.sharedIns.INITIAL_STORYBOARD, bundle: nil)
//        let nav = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.FORGOT_PIN_NAV) as! UINavigationController
//        
//        let vc = nav.viewControllers.first as! ForgotPINController
//        navigationController?.pushViewController(vc, animated: true)
    }
    
    func enablePopGesture(isEnabled: Bool)->Void{
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        navigationController?.interactivePopGestureRecognizer?.isEnabled = isEnabled
    }
    
    func showErrorRetry(errorMsg: String, colorCode: String)->Void{
        errorLayout = UIStackView()
        errorLayout.axis  = NSLayoutConstraint.Axis.vertical
        errorLayout.distribution  = UIStackView.Distribution.fill
        errorLayout.alignment = UIStackView.Alignment.fill
        
        let msgLabel = UILabel()
        msgLabel.text  = errorMsg
        msgLabel.numberOfLines = 0
        msgLabel.textAlignment = .center
        msgLabel.textColor = UIColor(string: colorCode)
        msgLabel.font = msgLabel.font.withSize(Constant.sharedIns.NORMAL_FONT_SIZE)
        
        var icRetry = UIImage(named: "ic_refresh")
        icRetry = icRetry?.tintImage(color: UIColor(string: colorCode))
        let btnRetry = UIImageView(image: icRetry)
        btnRetry.contentMode = .scaleAspectFit
        btnRetry.frame = CGRect(x: 0, y: 0, width: 36.0, height: 36.0)
        
        addTapEventForView(view: btnRetry, action: #selector(self.retryCallAPI(sender:)))
        
        addCustomConstraint(customView: btnRetry)
        
        errorLayout.addArrangedSubview(msgLabel)
        errorLayout.addArrangedSubview(btnRetry)
        
        errorLayout.layoutMargins = UIEdgeInsets(top:0, left: 24.0, bottom: 0, right: 24.0)
        errorLayout.isLayoutMarginsRelativeArrangement = true
        
        errorLayout.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: msgLabel.frame.height + btnRetry.frame.height + 60)
        
        //        if(isViewDisappear == false){
        //            if(navigationController?.navigationBar.isTranslucent == true){
        //                errorLayout.center = self.view.center
        //            }else{
        //                errorLayout.frame.origin.x = (UIScreen.main.bounds.size.width / 2) - (errorLayout.frame.width / 2)
        //                errorLayout.frame.origin.y = UIScreen.main.bounds.size.height / 2 - ((navigationController?.navigationBar.frame.height)! * 2)
        //            }
        //
        //        }else{
        //            errorLayout.center = self.view.center
        //        }
        
        errorLayout.frame.origin.x = (self.view.frame.width / 2) - (errorLayout.frame.width / 2)
        errorLayout.frame.origin.y = (self.view.frame.height / 2) - (errorLayout.frame.height / 2)
        
        self.view.addSubview(errorLayout)
        self.view.bringSubviewToFront(errorLayout)
    }
    
    /** OVERLOADING function error retry view with custom parentView**/
    func showErrorRetry(parentView: UIView, errorMsg: String, colorCode: String)->Void{
        errorLayout = UIStackView()
        errorLayout.axis  = NSLayoutConstraint.Axis.vertical
        errorLayout.distribution  = UIStackView.Distribution.fill
        errorLayout.alignment = UIStackView.Alignment.fill
        
        let msgLabel = UILabel()
        msgLabel.text  = errorMsg
        msgLabel.numberOfLines = 0
        msgLabel.textAlignment = .center
        msgLabel.textColor = UIColor(string: colorCode)
        msgLabel.font = msgLabel.font.withSize(Constant.sharedIns.NORMAL_FONT_SIZE)
        
        var icRetry = UIImage(named: "ic_refresh")
        icRetry = icRetry?.tintImage(color: UIColor(string: colorCode))
        let btnRetry = UIImageView(image: icRetry)
        btnRetry.contentMode = .scaleAspectFit
        btnRetry.frame = CGRect(x: 0, y: 0, width: 36.0, height: 36.0)
        
        addTapEventForView(view: btnRetry, action: #selector(self.retryCallAPI(sender:)))
        
        addCustomConstraint(customView: btnRetry)
        
        errorLayout.addArrangedSubview(msgLabel)
        errorLayout.addArrangedSubview(btnRetry)
        
        errorLayout.layoutMargins = UIEdgeInsets(top:0, left: 24.0, bottom: 0, right: 24.0)
        errorLayout.isLayoutMarginsRelativeArrangement = true
        
        errorLayout.frame = CGRect(x: 0, y: 0, width: parentView.frame.width, height: msgLabel.frame.height + btnRetry.frame.height + 60)
        
        errorLayout.frame.origin.x = (parentView.frame.width / 2) - (errorLayout.frame.width / 2)
        errorLayout.frame.origin.y = (parentView.frame.height / 2) - (errorLayout.frame.height / 2)
        
        //        if(isViewDisappear == false){
        //            if(navigationController?.navigationBar.isTranslucent == true){
        //                errorLayout.center = parentView.center
        //            }else{
        //                errorLayout.frame.origin.x = (UIScreen.main.bounds.size.width / 2) - (errorLayout.frame.width / 2)
        //                errorLayout.frame.origin.y = UIScreen.main.bounds.size.height / 2 - ((navigationController?.navigationBar.frame.height)! * 2)
        //            }
        //
        //        }else{
        //            errorLayout.center = self.view.center
        //        }
        
        parentView.addSubview(errorLayout)
        parentView.bringSubviewToFront(errorLayout)
    }
    
    func addCustomConstraint(customView: UIView)->Void{
        let widthContraints = NSLayoutConstraint(item: customView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.width, multiplier: 1.0,
                                                 constant: customView.frame.width + 15)
        
        let heightContraints = NSLayoutConstraint(item: customView, attribute:
            .height, relatedBy: .equal, toItem: nil,
                     attribute: NSLayoutConstraint.Attribute.height, multiplier: 1.0,
                     constant: customView.frame.height)
        
        
        customView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([widthContraints,heightContraints])
    }
    
    @objc func retryCallAPI(sender: AnyObject){
        removeErrorRetry()
    }
    
    func removeErrorRetry()->Void{
        errorLayout.removeFromSuperview()
    }
    
    
    func showActivityIndicator(colorCode: String)->Void{
        isFirstInit = false
        
        self.activityIndicator = UIActivityIndicatorView()
        
        DispatchQueue.main.async {
            self.activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
            
            if !colorCode.isEmpty{
                //activityIndicator.color = UIColor(string: colorCode)
                self.activityIndicator.color = UIColor(string: Constant.sharedIns.color_grey_500)
            }
            
            var heightNavBar : CGFloat =  0
            if (self.navigationController?.navigationBar.frame.height) != nil{
                heightNavBar = (self.navigationController?.navigationBar.frame.height)!
            }
            //        if(navigationController?.navigationBar.isTranslucent == true){
            //            activityIndicator.center = (UIApplication.shared.keyWindow?.center)!
            //        }else{
            self.activityIndicator.frame.origin.x = (UIScreen.main.bounds.size.width / 2) - (self.activityIndicator.frame.width / 2)
            self.activityIndicator.frame.origin.y = UIScreen.main.bounds.size.height / 2 - heightNavBar
            //}
            
            self.view.addSubview(self.activityIndicator)
            self.view.bringSubviewToFront(self.activityIndicator)
            self.activityIndicator.startAnimating()
        }
    }
    
    /** OVERLOADING function activity indicator view with custom parentView**/
    func showActivityIndicator(parentView: UIView, colorCode: String)->Void{
        isFirstInit = false
        
        self.activityIndicator = UIActivityIndicatorView()
        
        DispatchQueue.main.async {
            self.activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
            if !colorCode.isEmpty{
                //activityIndicator.color = UIColor(string: colorCode)
                self.activityIndicator.color = UIColor(string: Constant.sharedIns.color_grey_500)
            }
            
            
            self.activityIndicator.frame.origin.x = (parentView.bounds.width / 2) - (self.activityIndicator.frame.width / 2)
            self.activityIndicator.frame.origin.y = (parentView.bounds.height / 2) - (self.activityIndicator.frame.height / 2)
            
            parentView.addSubview(self.activityIndicator)
            parentView.bringSubviewToFront(self.activityIndicator)
            
            self.activityIndicator.startAnimating()
        }
    }
    
    func removeActivityIndicator()->Void{
        self.activityIndicator.stopAnimating()
        self.activityIndicator.removeFromSuperview()
    }
    
    func showFeedbackDialog(message: String, type: FeedbackDialog.type, completion: @escaping (()->()))->Void{
        
        setEnableDrawerViewGesture(isEnable: false)
        
        ///Give a delay to set interaction enabled false after dismiss progress dialog
        let delay = DispatchTime.now() + 0.15
        DispatchQueue.main.asyncAfter(deadline: delay) {
            self.view.isUserInteractionEnabled = false
            self.navigationController?.navigationBar.isUserInteractionEnabled = false
        }
        
        DispatchQueue.main.async {
            self.feedbackDialog = FeedbackDialog(message: message, type: type)
            
            var heightNavBar : CGFloat =  0
            if (self.navigationController?.navigationBar.frame.height) != nil{
                heightNavBar = (self.navigationController?.navigationBar.frame.height)!
            }
            
            if(self.navigationController?.navigationBar.isTranslucent == true){
                self.feedbackDialog.center = self.view.center
            }else{
                self.feedbackDialog.frame.origin.x = UIScreen.main.bounds.size.width / 2
                self.feedbackDialog.frame.origin.y = UIScreen.main.bounds.size.height / 2 - heightNavBar
            }
            
            self.feedbackDialog.transform = CGAffineTransform.init(scaleX: 1.2, y: 1.2)
            self.feedbackDialog.alpha = 0
            
            UIView.animate(withDuration: 0.2) {
                self.view.addSubview(self.feedbackDialog)
                self.feedbackDialog.alpha = 1
                self.feedbackDialog.transform = CGAffineTransform.identity
            }
        }
        
        let delay2 = DispatchTime.now() + 3.2
        DispatchQueue.main.asyncAfter(deadline: delay2) {
            self.view.isUserInteractionEnabled = true
            self.navigationController?.navigationBar.isUserInteractionEnabled = true
            self.setEnableDrawerViewGesture(isEnable: true)
            self.dismissFeedbackDialog()
            
            let delay2 = DispatchTime.now() + 0.25
            DispatchQueue.main.asyncAfter(deadline: delay2) {
                completion()
            }
        }
    }
    
    private func dismissFeedbackDialog(){
        UIView.animate(withDuration: 0.2, animations: {
            self.feedbackDialog.transform = CGAffineTransform.init(scaleX: 1.05, y: 1.05)
            self.feedbackDialog.alpha = 0
            
        }) { (success:Bool) in
            self.feedbackDialog.removeFromSuperview()
            self.feedbackDialog = nil
        }
    }
    
    func showProgressDialog(message: String) -> Void{
        self.view.isUserInteractionEnabled = false
        navigationController?.navigationBar.isUserInteractionEnabled = true
        
        DispatchQueue.main.async {
            self.progressDialog = ProgressDialogUtil_v2(message: message)
            
            if(self.navigationController?.navigationBar.isTranslucent == true){
                self.progressDialog.center = (UIApplication.shared.keyWindow?.center)!
            }else{
                self.progressDialog.frame.origin.x = UIScreen.main.bounds.size.width / 2
                self.progressDialog.frame.origin.y = UIScreen.main.bounds.size.height / 2 - (self.navigationController?.navigationBar.frame.height)!
            }
            
            self.progressDialog.transform = CGAffineTransform.init(scaleX: 1.1, y: 1.1)
            self.progressDialog.alpha = 0
            
            UIView.animate(withDuration: 0.2) {
                self.view.addSubview(self.progressDialog)
                self.progressDialog.alpha = 1
                self.progressDialog.transform = CGAffineTransform.identity
            }
        }
    }
    
    func dismissProgressDialog() -> Void{
        if(progressDialog != nil){
            UIView.animate(withDuration: 0.075, animations: {
                self.progressDialog.transform = CGAffineTransform.init(scaleX: 1.1, y: 1.1)
                self.progressDialog.alpha = 0
                
            }) { (success:Bool) in
                self.view.isUserInteractionEnabled = true
                self.navigationController?.navigationBar.isUserInteractionEnabled = true
                if self.progressDialog != nil{
                    self.progressDialog.removeFromSuperview()
                    self.progressDialog = nil
                }
            }
        }
    }
    
    func initAlertView() -> Void{
        if(alertView == nil){
            alertView = AlertViewUtil()
        }
    }
    
    /**
     ** SSL Pinning
     **/
    func configureAlamoFireSSLPinning()->Void{
        // Your hostname and endpoint
        let hostname = uriBase_api
        //let endpoint = "YOUR_ENDPOINT"
        let cert = "*.homecredit.co.id" // e.g. for cert.der, this should just be "cert"
        
        // Set up certificates
        let pathToCert = Bundle.main.path(forResource: cert, ofType: "cer")
        
        //print("pathtocert:\(pathToCert!)")
        
        let localCertificate = NSData(contentsOfFile: pathToCert!)
        let certificates = [SecCertificateCreateWithData(nil, localCertificate!)!]
        
        // Configure the trust policy manager
        let serverTrustPolicy = ServerTrustPolicy.pinCertificates(
            certificates: certificates,
            validateCertificateChain: true,
            validateHost: true
        )
        
        let serverTrustPolicies = [hostname: serverTrustPolicy]
        let serverTrustPolicyManager = ServerTrustPolicyManager(policies: serverTrustPolicies)
        
        // Configure session manager with trust policy
        afManager = SessionManager(
            configuration: URLSessionConfiguration.default,
            serverTrustPolicyManager: serverTrustPolicyManager
        )
    }
    
    func getReformatPhoneNumber(phoneNumber: String)->String{
        return phoneNumber.replacingOccurrences(of: "-", with: "")
    }
    
    func isCorrectPhoneNumber(tfPhone: UITextField)->Bool{
        let phoneNumber = getReformatPhoneNumber(phoneNumber: tfPhone.text! as String)
        var errorMessage = ""
        
        if(phoneNumber.isEmpty){
            errorMessage = Wordings.msg_error_fill_phone_number
        }else if(phoneNumber.hasPrefix("8") == false) {
            errorMessage = Wordings.msg_error_phone_number_starts_with
        }else if (phoneNumber.count < 9) {
            errorMessage = Wordings.msg_error_phone_number_length
        }
        
        if(errorMessage.isEmpty == false){
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: errorMessage, actionButton1: Wordings.BTN_OK, actionButton2: "")
            return false
        }
        
        return true
    }
    
    
    /** isCorrectPIN overloading without message **/
    func isCorrectPINLength(tfPIN: UITextField)->Bool{
        if(tfPIN.text!.count < 4){
            
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.mesg_error_pin_length, actionButton1: Wordings.BTN_OK, actionButton2: "")
            
            return false
        }
        
        return true
    }
    
    func isPIN_InputValidation(tfPIN: UITextField, tfConfirmPIN: UITextField)->Bool{
        if(tfPIN.text!.isEmpty){
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_input_pin_is_empty, actionButton1: Wordings.BTN_OK, actionButton2: "")
            return false
        }
        
        if(tfPIN.text!.count < 4){
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_input_pin_correctly, actionButton1: Wordings.BTN_OK, actionButton2: "")
            return false
        }
        
        if(tfConfirmPIN.text!.isEmpty){
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_input_confirm_pin_is_empty, actionButton1: Wordings.BTN_OK, actionButton2: "")
            return false
        }
        
        if(tfConfirmPIN.text!.count < 4){
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_input_confirm_pin_correctly, actionButton1: Wordings.BTN_OK, actionButton2: "")
            
            return false
        }
        
        if(tfPIN.text! != tfConfirmPIN.text!){
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_pin_not_same, actionButton1: Wordings.BTN_OK, actionButton2: "")
            return false
        }
        
        
        return true
    }
    
    
    func isCorrectInputOTP(tfOTP: UITextField)->Bool{
        if(tfOTP.text!.isEmpty){
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_input_otp_isempty, actionButton1: Wordings.BTN_OK, actionButton2: "")
            return false
        }
        
        if(tfOTP.text!.count < 4){
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_input_otp_length, actionButton1: Wordings.BTN_OK, actionButton2: "")
            return false
        }
        return true
    }
    
    func editingPINField(textField: UITextField){
        if(textField.text!.isEmpty){
            setTextFieldFontStyle(textField, isBold: true)
        }else{
            setTextFieldFontStyle(textField, isBold: false)
        }
    }
    
    
    func setTextFieldFontStyle(_ textField: UITextField,isBold: Bool)->Void{
        if(isBold){
            textField.font = UIFont.boldSystemFont(ofSize: UIFont.labelFontSize)
            return
        }
        
        if(textField.text!.count < 0 || textField.text!.isEmpty){
            textField.font = UIFont.boldSystemFont(ofSize: UIFont.labelFontSize)
        }else{
            textField.font = UIFont.systemFont(ofSize: UIFont.labelFontSize)
        }
    }
    
    func isDOBFilled(tfDOB: UITextField)->Bool{
        if(tfDOB.text!.isEmpty){
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_input_dob, actionButton1: Wordings.BTN_OK, actionButton2: "")
            return false
        }
        
        return true
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        //Always set cursor to last position
        let newPosition = textField.endOfDocument
        textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
        
        let length = textField.text?.count
        let str = textField.text!
        
        print("PHONE: \(str)")
        
        if(string.isEmpty){
            print("BACKSPACE_PRESSED_EMPTY")
            
            print("DELETE")
            if(length == 5){
                let index = str.index(str.startIndex, offsetBy: 4)
                
                textField.text = str.substring(to: index)
            }else if(length == 10){
                let index = str.index(str.startIndex, offsetBy: 9)
                textField.text = str.substring(to: index)
            }else if(length == 15){
                let index = str.index(str.startIndex, offsetBy: 14)
                textField.text = str.substring(to: index)
            }
            
            return true
        }
        
        print("ADD TEXT")
//        if(length==3){
//            textField.text="\(str)-"
//        }else if(length==8){
//            textField.text="\(str)-"
//        }else if(length==13){
//            textField.text="\(str)-"
//        }
        textField.text="\(str)"
        
        return true
    }
    
    
    
    func moveTextField(moveDistance: Int, isUp: Bool){
        
        print("MOVE_KEYBOARD: \(moveDistance)")
        var moveDuration = 0.4
        let movement = CGFloat(isUp ? moveDistance : -moveDistance)
        
        UIView.beginAnimations("animateTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        
        if(isUp == false){
            moveDuration = 0.2
        }
        
        UIView.setAnimationDuration(moveDuration)
        let rect = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        self.view.frame = rect.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
        moveTextField(moveDistance: 0, isUp: false)
    }
    
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        print("KEYBOARD_SHOWN")
        isKeyboardShown = true
        //self.scrollView.isScrollEnabled = true
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        
        if(self.scrollView != nil){
            let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize!.height / 2, right: 0.0)
            
            self.scrollView?.contentInset = contentInsets
            self.scrollView?.scrollIndicatorInsets = contentInsets
            
            var aRect : CGRect = self.view.frame
            aRect.size.height -= keyboardSize!.height
            if let activeField = self.activeField {
                if (!aRect.contains(activeField.frame.origin)){
                    self.scrollView?.scrollRectToVisible(activeField.frame, animated: true)
                }
            }
        }else{
            
            if self.activeField != nil {
                moveTextField(moveDistance: Int(-keyboardSize!.height * 0.4), isUp: true)
            }
        }
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        //Once keyboard disappears, restore original positions
        print("KEYBOARD_HIDDEN")
        isKeyboardShown = false
        if(self.scrollView != nil){
            self.scrollView?.isScrollEnabled = true
            
            //var info = notification.userInfo!
            //let keyboardSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
            //let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
            var contentInset = self.scrollView?.contentInset
            contentInset?.bottom = 0
            self.scrollView?.contentInset = contentInset!
            self.scrollView?.scrollIndicatorInsets = contentInset!
            //self.scrollView?.contentInset = UIEdgeInsets.zero
            //self.scrollView?.scrollIndicatorInsets = UIEdgeInsets.zero
            
        }else{
            if self.activeField != nil {
                moveTextField(moveDistance: 0, isUp: false)
            }
        }
    }
    
    func initDatePicker(view: UITextField){
        DispatchQueue.main.async {
            let currDate = Date.init()
            let dateFormatter = DateFormatter()
            
            ///Set as a year
            dateFormatter.dateFormat = "yyyy"
            let currYear = dateFormatter.string(from: currDate)
            let yearsOldEligibility = 19 ///at least 19 years old if married
            let defaultYear = Int(currYear)! - yearsOldEligibility
            
            ///Set as a date and month
            dateFormatter.dateFormat = "dd/MM"
            let partialDate = dateFormatter.string(from: currDate)
            let completeDate = "\(partialDate)/\(String(defaultYear))"
            
            print("completeDate: \(completeDate)")
            
            ///Set as a complete date
            dateFormatter.dateFormat = "dd/MM/yyyy"

            if let date = dateFormatter.date(from: completeDate){
                let todaysDate = Date()
//                let loc = Locale(identifier: "id")
//                self.datePicker.locale = loc
                self.datePicker.date = date
                self.datePicker.locale = NSLocale(localeIdentifier: "en_GB") as Locale
                self.datePicker.minimumDate = todaysDate
            }
//            let loc = Locale(identifier: "id")
//            self.datePicker.locale = loc
            self.datePicker.locale = NSLocale(localeIdentifier: "en_GB") as Locale
            self.datePicker.datePickerMode = .date
//            self.datePicker.locale = NSLocale(localeIdentifier: "id_ID") as Locale
            
            let toolbar  = UIToolbar()
            toolbar.sizeToFit()
            
            let chooseButton = UIBarButtonItem(title: Wordings.BTN_CHOOSE, style: UIBarButtonItem.Style.done, target: nil, action: #selector(self.datePickerDonePressed))
            
            let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            
            chooseButton.tintColor = UIColor(string: Constant.sharedIns.color_green_main)
            
            let cancelButton = UIBarButtonItem(title: Wordings.BTN_CANCEL, style: UIBarButtonItem.Style.done, target: nil, action: #selector(self.dismissKeyboard))
            
            cancelButton.tintColor = UIColor(string: Constant.sharedIns.color_red_main)
            
            toolbar.setItems([cancelButton, flexSpace, chooseButton], animated: false)
            view.inputAccessoryView = toolbar
            view.inputView = self.datePicker
        }
    }
    
    @objc func datePickerDonePressed(){
        dismissKeyboard()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_GB") as Locale
//        dateFormatter.locale = NSLocale(localeIdentifier: "id_ID") as Locale
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .none
    }
    
    
    func setEnableDrawerViewGesture(isEnable : Bool){
//        if let drawerController = parent as? NavDrawerController{
//            print("isENABLED_DRAWER: \(isEnable)")
//            if(isEnable){
//                drawerController.addGesturesRecognizerDrawer()
//            }else{
//                drawerController.removeGesturesRecognizerDrawer()
//            }
//            return
//        }
        
        
//        if let drawerController = navigationController?.parent as? NavDrawerController{
//            print("isENABLED_DRAWER: \(isEnable)")
//            if(isEnable){
//                drawerController.addGesturesRecognizerDrawer()
//            }else{
//                drawerController.removeGesturesRecognizerDrawer()
//            }
//        }
    }
    
    func openContact() {
        let contactPicker = CNContactPickerViewController()
        contactPicker.delegate = self
        contactPicker.displayedPropertyKeys =
            [CNContactGivenNameKey
                , CNContactPhoneNumbersKey]
        self.present(contactPicker, animated: true, completion: nil)
    }
    
    func createContactNative() {
        let controller = CNContactViewController(forNewContact: nil)
        controller.delegate = self
        let navigationController = UINavigationController(rootViewController: controller)
        self.present(navigationController, animated: true)
    }
    
    func contactViewController(_ viewController: CNContactViewController, didCompleteWith contact: CNContact?) {
        viewController.navigationController?.dismiss(animated: true)
    }
    
    func contactPicker(_ picker: CNContactPickerViewController,
                       didSelect contactProperty: CNContactProperty) {
        
    }
    
    @available(iOS 9.0, *)
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contacts: [CNContact]) {
        var spouseName: String!
        var sonName: String!
        var fatherName: String!
        var motherName: String!
        var brotherName: String!
        var sisterName: String!
        var birthday: String!
        var occupation: String!
        var primaryPhoneNumberStr: String!
        var primaryPhoneNumberStr3: String!
        var emailAddress: String!
        var brotherCount: Int!
        var sisterCount: Int!
        var brotherSisterCount: Int!
        var sonCount: Int!
        
        //core data
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        // You can fetch selected name and number in the following way
        for contact in contacts {
            let timeinserted = Date().ticks
            //identifier
            let id: String = contact.identifier
            // user name
            let givenName:String = contact.givenName ?? ""
            let familyName:String = contact.familyName ?? ""
            let name = "\(givenName) \(familyName)"
            // user phone number
            let userPhoneNumbers:[CNLabeledValue<CNPhoneNumber>] = contact.phoneNumbers
            if (userPhoneNumbers.count > 0) {
                let firstPhoneNumber:CNPhoneNumber = userPhoneNumbers[0].value
                // user phone number string
                let primaryPhoneNumberStr1 = firstPhoneNumber.stringValue ?? ""
                let primaryPhoneNumberStr2 = primaryPhoneNumberStr1.components(separatedBy: .whitespaces)
                print("primaryPhoneNumberStr2 count:\(primaryPhoneNumberStr2.count)")
                if (primaryPhoneNumberStr2.count > 1) {
                    if primaryPhoneNumberStr2[0].range(of:"+") != nil {
                        primaryPhoneNumberStr3 = primaryPhoneNumberStr2.dropFirst().joined()
                    } else {
                        primaryPhoneNumberStr3 = primaryPhoneNumberStr2.joined()
                    }
                    let primaryPhoneNumberStr4 = primaryPhoneNumberStr3.replacingOccurrences(of: "-", with: "", options: .literal, range: nil)
                    let primaryPhoneNumberStr5 = primaryPhoneNumberStr4.replacingOccurrences(of: "(", with: "", options: .literal, range: nil)
                    let primaryPhoneNumberStr6 = primaryPhoneNumberStr5.replacingOccurrences(of: ")", with: "", options: .literal, range: nil)
                    if primaryPhoneNumberStr6.prefix(1) == "0" {
                        primaryPhoneNumberStr = primaryPhoneNumberStr6.replacingCharacters(in: ...primaryPhoneNumberStr6.startIndex, with: "+62")
                    } else if primaryPhoneNumberStr6.range(of:"+62") == nil {
                        primaryPhoneNumberStr = "+62\(primaryPhoneNumberStr6)"
                    } else {
                        primaryPhoneNumberStr = primaryPhoneNumberStr6
                    }
                } else {
                    let primaryPhoneNumberStr4 = primaryPhoneNumberStr2[0].replacingOccurrences(of: "-", with: "", options: .literal, range: nil)
                    let primaryPhoneNumberStr5 = primaryPhoneNumberStr4.replacingOccurrences(of: "(", with: "", options: .literal, range: nil)
                    let primaryPhoneNumberStr6 = primaryPhoneNumberStr5.replacingOccurrences(of: ")", with: "", options: .literal, range: nil)
                    if primaryPhoneNumberStr6.prefix(1) == "0" {
                        primaryPhoneNumberStr = primaryPhoneNumberStr6.replacingCharacters(in: ...primaryPhoneNumberStr6.startIndex, with: "+62")
                    } else if primaryPhoneNumberStr6.range(of:"+62") == nil {
                        primaryPhoneNumberStr = "+62\(primaryPhoneNumberStr6)"
                    } else {
                        primaryPhoneNumberStr = primaryPhoneNumberStr6
                    }
                }
                print("primaryPhoneNumberStr1:\(primaryPhoneNumberStr1)")
                print("primaryPhoneNumberStr:\(primaryPhoneNumberStr!)")
            } else {
                primaryPhoneNumberStr = ""
            }
            // user email address
            if (contact.emailAddresses.count > 0) {
                emailAddress = contact.emailAddresses.first?.value as! String ?? ""
            } else {
                emailAddress = ""
            }
            // user birthday
            let day = contact.birthday?.day ?? 00
            let month = contact.birthday?.month ?? 00
            let year = contact.birthday?.year ?? 0000
            let temporaryBirthday = "\(day)-\(month)-\(year)"
//            if (temporaryBirthday == "0-0-0") {
//                birthday = Constant.convertDateStringtoAnotherFormat(dateString: "01-01-1900", fromFormat: "dd-MM-yyyy", toFormat: "dd MMMM yyyy")
//            } else {
//                birthday = Constant.convertDateStringtoAnotherFormat(dateString: "\(day)-\(month)-\(year)", fromFormat: "dd-MM-yyyy", toFormat: "dd MMMM yyyy")
//            }
            birthday = "01 January 1900"
            // user occupation
//            let temporaryOccupation = contact.jobTitle.description
//            if (temporaryOccupation == "") {
//                occupation = "Please Choose"
//            } else {
//                occupation = contact.jobTitle.description
//            }
            occupation = "Please Choose"
            
            print("id:\(id), username:\(name), primaryPhoneNumber:\(primaryPhoneNumberStr!), email:\(emailAddress!), birthday:\(birthday!), occupation:\(occupation!)")
            
            let fetchRequestContact = NSFetchRequest<NSManagedObject>(entityName: "Contact")
            let namePredicate = NSPredicate(format: "name == %@", name)
            fetchRequestContact.predicate = namePredicate
            do {
                totalProspect = try managedContext.count(for: fetchRequestContact)
                print("totalContact:\(totalProspect!)")
                if (totalProspect! == 0) {
                    ///to CoreData
                    let insertContact = NSEntityDescription.insertNewObject(forEntityName: "Contact", into: managedContext)
                    let insertSpouse = NSEntityDescription.insertNewObject(forEntityName: "Contact", into: managedContext)
                    let insertFather = NSEntityDescription.insertNewObject(forEntityName: "Contact", into: managedContext)
                    let insertMother = NSEntityDescription.insertNewObject(forEntityName: "Contact", into: managedContext)
                    let insertBro = NSEntityDescription.insertNewObject(forEntityName: "Contact", into: managedContext)
                    let insertSis = NSEntityDescription.insertNewObject(forEntityName: "Contact", into: managedContext)
                    let convertedBirthday = Constant.convertDateStringtoAnotherFormat(dateString: birthday!, fromFormat: "dd MMMM yyyy", toFormat: "dd-MM-yyyy")
                    let age = Constant.countAge(birthday: convertedBirthday)
                    if (age < 26) {
                        insertContact.setValue("≤ 25 years", forKey: "ageBand")
                    } else if (25 < age && age < 36) {
                        insertContact.setValue("26 - 35 years", forKey: "ageBand")
                    } else if (35 < age && age < 46) {
                        insertContact.setValue("36 - 45 years", forKey: "ageBand")
                    } else if (45 < age && age < 55) {
                        insertContact.setValue("46 - 55 years", forKey: "ageBand")
                    } else if (age > 55) {
                        insertContact.setValue("≥ 56 years", forKey: "ageBand")
                    }
                    insertContact.setValue("Contact:\(id)", forKey: "id")
                    insertContact.setValue(CustomUserDefaults.shared.getUsername(), forKey: "agentCode")
                    insertContact.setValue(name, forKey: "name")
                    insertContact.setValue("Please Choose", forKey: "gender")
                    insertContact.setValue(birthday!, forKey: "dateOfBirth")
                    insertContact.setValue("Please Choose", forKey: "sourceOfProspect")
                    insertContact.setValue("Please Choose", forKey: "maritalStatus")
                    insertContact.setValue("0", forKey: "totalChildren")
                    insertContact.setValue(occupation!, forKey: "occupation")
                    insertContact.setValue("Please Choose", forKey: "exactSourceOfProspect")
                    insertContact.setValue("Please Choose", forKey: "exactOccupation")
                    insertContact.setValue("Please Choose", forKey: "income")
                    insertContact.setValue("1", forKey: "exactIncome")
                    // user address
//                    let addresses = contact.postalAddresses
//                    if (addresses.count > 0) {
//                        var subLocality: String!
//                        var subAdministrativeArea: String!
//                        for address in addresses {
//                            let rawAddressLabel = address.label?.description
//                            let rawAddressLabel1 = rawAddressLabel!.replacingOccurrences(of: "_$!<", with: "", options: .literal, range: nil)
//                            let addressLabel = rawAddressLabel1.replacingOccurrences(of: ">!$_", with: "", options: .literal, range: nil)
//                            let street = address.value.street
//                            if #available(iOS 10.3, *) {
//                                subLocality = address.value.subLocality
//                            } else {
//                                subLocality = ""
//                            }
//                            let city = address.value.city
//                            if #available(iOS 10.3, *) {
//                                subAdministrativeArea = address.value.subAdministrativeArea
//                            } else {
//                                subAdministrativeArea = ""
//                            }
//                            let state = address.value.state
//                            let postalCode = address.value.postalCode
//                            let country = address.value.country
//                            if (addressLabel == "Home") {
//                                print("inserted Home Address")
//                                insertContact.setValue("\(street), \(city), \(state), \(postalCode), \(country)", forKey: "homeAddress")
//                            } else if (addressLabel == "Work") {
//                                print("inserted Work Address")
//                                insertContact.setValue("\(street), \(city), \(state), \(postalCode), \(country)", forKey: "workAddress")
//                            }
//                        }
//                    } else {
//                        insertContact.setValue("", forKey: "homeAddress")
//                        insertContact.setValue("", forKey: "workAddress")
//                    }
                    insertContact.setValue("", forKey: "homeAddress")
                    insertContact.setValue("", forKey: "workAddress")
                    insertContact.setValue("Please Choose", forKey: "locationAccessibility")
                    insertContact.setValue("Please Choose", forKey: "knowThisPerson")
                    insertContact.setValue("Please Choose", forKey: "howCloseAreYou")
                    insertContact.setValue("Please Choose", forKey: "howInfluenceThisPerson")
                    insertContact.setValue("Please Choose", forKey: "prospectingStatus")
                    //relation
                    let contactRelation = contact.contactRelations
                    if contactRelation.count > 0 {
                        for contact in contactRelation {
                            let rawContactLabel = contact.label?.description
                            let rawContactLabel1 = rawContactLabel!.replacingOccurrences(of: "_$!<", with: "", options: .literal, range: nil)
                            let contactLabel = rawContactLabel1.replacingOccurrences(of: ">!$_", with: "", options: .literal, range: nil)
                            let contactName = contact.value.name
                            print("contact label:\(contactLabel) with name:\(contactName)")
                            if (contactLabel == "Son") {
                                sonName = contactName ?? ""
                                sonCount = sonName.count
                                if (sonName.isEmpty == false) {
                                    let insertChildren = NSEntityDescription.insertNewObject(forEntityName: "Children", into: managedContext)
                                    insertChildren.setValue("Contact:\(id)", forKey: "userId")
                                    insertChildren.setValue("Children:\(sonName)\(id)", forKey: "childrenId")
                                    insertChildren.setValue(CustomUserDefaults.shared.getUsername(), forKey: "agentCode")
                                    insertChildren.setValue(sonName, forKey: "childrenName")
                                    insertChildren.setValue("Please Choose", forKey: "childrenStatus")
                                    do {
                                        try managedContext.save()
                                        prospectChildrenList.append(insertChildren)
                                    } catch let error as NSError {
                                        print("Could not save. \(error), \(error.userInfo)")
                                    }
                                }
                            } else if (contactLabel == "Brother") {
                                brotherName = contactName ?? ""
                                brotherCount = brotherName.count
                                print("brotherCount:\(brotherCount!)")
                                //brother
                                if (brotherName.isEmpty == false) {
                                    let insertBrother = NSEntityDescription.insertNewObject(forEntityName: "Brother", into: managedContext)
                                    insertBrother.setValue("Contact:\(id)", forKey: "userId")
                                    insertBrother.setValue("Brother:\(brotherName)\(id)", forKey: "brotherId")
                                    insertBrother.setValue(CustomUserDefaults.shared.getUsername(), forKey: "agentCode")
                                    insertBrother.setValue(brotherName, forKey: "brotherName")
                                    insertBrother.setValue("Please Choose", forKey: "brotherAgeBand")
                                    //brother in contact
                                    insertBro.setValue("Please Choose", forKey: "ageBand")
                                    insertBro.setValue("Brothers:\(timeinserted)", forKey: "id")
                                    insertBro.setValue(CustomUserDefaults.shared.getUsername(), forKey: "agentCode")
                                    insertBro.setValue(brotherName, forKey: "name")
                                    insertBro.setValue("Male", forKey: "gender")
                                    insertBro.setValue(Constant.convertDateStringtoAnotherFormat(dateString: "01-01-1900", fromFormat: "dd-MM-yyyy", toFormat: "dd MMMM yyyy"), forKey: "dateOfBirth")
                                    insertBro.setValue("Family & Relatives", forKey: "sourceOfProspect")
                                    insertBro.setValue("Please Choose", forKey: "maritalStatus")
                                    insertBro.setValue("0", forKey: "totalChildren")
                                    insertBro.setValue("Please Choose", forKey: "occupation")
                                    insertBro.setValue("Please Choose", forKey: "exactSourceOfProspect")
                                    insertBro.setValue("Please Choose", forKey: "exactOccupation")
                                    insertBro.setValue("Please Choose", forKey: "income")
                                    insertBro.setValue("1", forKey: "exactIncome")
                                    insertBro.setValue("", forKey: "homeAddress")
                                    insertBro.setValue("", forKey: "workAddress")
                                    insertBro.setValue("Please Choose", forKey: "locationAccessibility")
                                    insertBro.setValue("Please Choose", forKey: "knowThisPerson")
                                    insertBro.setValue("Please Choose", forKey: "howCloseAreYou")
                                    insertBro.setValue("Please Choose", forKey: "howInfluenceThisPerson")
                                    insertBro.setValue("Please Choose", forKey: "prospectingStatus")
                                    insertBro.setValue("", forKey: "spouseName")
                                    insertBro.setValue("", forKey: "fatherName")
                                    insertBro.setValue("", forKey: "motherName")
                                    insertBro.setValue("", forKey: "spouseId")
                                    insertBro.setValue("Father:\(timeinserted)", forKey: "fatherId")
                                    insertBro.setValue("Mother:\(timeinserted)", forKey: "motherId")
                                    //deleted CoreData
                                    insertBro.setValue("-", forKey: "spouseRemarks")
                                    insertBro.setValue("-", forKey: "motherRemarks")
                                    insertBro.setValue("-", forKey: "childrenAgeBand")
                                    insertBro.setValue("-", forKey: "childrenRemarks")
                                    insertBro.setValue("-", forKey: "fatherAgeBand")
                                    insertBro.setValue("-", forKey: "fatherRemarks")
                                    insertBro.setValue("-", forKey: "motherAgeBand")
                                    insertBro.setValue("-", forKey: "spouseAgeBand")
                                    insertBro.setValue("-", forKey: "spouseIncome")
                                    insertBro.setValue("-", forKey: "spouseOccupation")
                                    do {
                                        try managedContext.save()
                                        prospectBrotherList.append(insertBrother)
                                        prospectBroList.append(insertBro)
                                    } catch let error as NSError {
                                        print("Could not save. \(error), \(error.userInfo)")
                                    }
                                }
                            } else if (contactLabel == "Sister") {
                                sisterName = contactName ?? ""
                                sisterCount = sisterName.count
                                //sister
                                if (sisterName.isEmpty == false) {
                                    let insertSister = NSEntityDescription.insertNewObject(forEntityName: "Sister", into: managedContext)
                                    insertSister.setValue("Contact:\(id)", forKey: "userId")
                                    insertSister.setValue("Sister:\(sisterName)\(id)", forKey: "sisterId")
                                    insertSister.setValue(CustomUserDefaults.shared.getUsername(), forKey: "agentCode")
                                    insertSister.setValue(sisterName, forKey: "sisterName")
                                    insertSister.setValue("Please Choose", forKey: "sisterAgeBand")
                                    //sister in contact
                                    insertSis.setValue("Please Choose", forKey: "ageBand")
                                    insertSis.setValue("Sister:\(timeinserted)", forKey: "id")
                                    insertSis.setValue(CustomUserDefaults.shared.getUsername(), forKey: "agentCode")
                                    insertSis.setValue(sisterName, forKey: "name")
                                    insertSis.setValue("Female", forKey: "gender")
                                    insertSis.setValue(Constant.convertDateStringtoAnotherFormat(dateString: "01-01-1900", fromFormat: "dd-MM-yyyy", toFormat: "dd MMMM yyyy"), forKey: "dateOfBirth")
                                    insertSis.setValue("Family & Relatives", forKey: "sourceOfProspect")
                                    insertSis.setValue("Please Choose", forKey: "maritalStatus")
                                    insertSis.setValue("0", forKey: "totalChildren")
                                    insertSis.setValue("Please Choose", forKey: "occupation")
                                    insertSis.setValue("Please Choose", forKey: "exactSourceOfProspect")
                                    insertSis.setValue("Please Choose", forKey: "exactOccupation")
                                    insertSis.setValue("Please Choose", forKey: "income")
                                    insertSis.setValue("1", forKey: "exactIncome")
                                    insertSis.setValue("", forKey: "homeAddress")
                                    insertSis.setValue("", forKey: "workAddress")
                                    insertSis.setValue("Please Choose", forKey: "locationAccessibility")
                                    insertSis.setValue("Please Choose", forKey: "knowThisPerson")
                                    insertSis.setValue("Please Choose", forKey: "howCloseAreYou")
                                    insertSis.setValue("Please Choose", forKey: "howInfluenceThisPerson")
                                    insertSis.setValue("Please Choose", forKey: "prospectingStatus")
                                    insertSis.setValue("", forKey: "spouseName")
                                    insertSis.setValue("", forKey: "fatherName")
                                    insertSis.setValue("", forKey: "motherName")
                                    insertSis.setValue("", forKey: "spouseId")
                                    insertSis.setValue("Father:\(timeinserted)", forKey: "fatherId")
                                    insertSis.setValue("Mother:\(timeinserted)", forKey: "motherId")
                                    //deleted CoreData
                                    insertSis.setValue("-", forKey: "spouseRemarks")
                                    insertSis.setValue("-", forKey: "motherRemarks")
                                    insertSis.setValue("-", forKey: "childrenAgeBand")
                                    insertSis.setValue("-", forKey: "childrenRemarks")
                                    insertSis.setValue("-", forKey: "fatherAgeBand")
                                    insertSis.setValue("-", forKey: "fatherRemarks")
                                    insertSis.setValue("-", forKey: "motherAgeBand")
                                    insertSis.setValue("-", forKey: "spouseAgeBand")
                                    insertSis.setValue("-", forKey: "spouseIncome")
                                    insertSis.setValue("-", forKey: "spouseOccupation")
                                    do {
                                        try managedContext.save()
                                        prospectSisterList.append(insertSister)
                                        prospectSisList.append(insertSis)
                                    } catch let error as NSError {
                                        print("Could not save. \(error), \(error.userInfo)")
                                    }
                                }
                            } else if (contactLabel == "Spouse") {
                                spouseName = contactName ?? ""
                                if (spouseName.isEmpty == false) {
                                    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                                        return
                                    }
                                    let managedContext = appDelegate.persistentContainer.viewContext
                                    let fetchRequestAnotherContact = NSFetchRequest<NSManagedObject>(entityName: "Contact")
                                    let namePredicate = NSPredicate(format: "name == %@", spouseName!)
                                    let agentCodePredicate = NSPredicate(format: "agentCode == %@", CustomUserDefaults.shared.getUsername())
                                    let anotherContactPredicateCompound = NSCompoundPredicate.init(type: .and, subpredicates: [agentCodePredicate,namePredicate])
                                    fetchRequestAnotherContact.predicate = anotherContactPredicateCompound
                                    do {
                                        let contacts = try managedContext.fetch(fetchRequestAnotherContact)
                                        if (contacts.count == 0) {
                                            insertSpouse.setValue("Please Choose", forKey: "ageBand")
                                            insertSpouse.setValue("Spouse:\(timeinserted)", forKey: "id")
                                            insertSpouse.setValue(CustomUserDefaults.shared.getUsername(), forKey: "agentCode")
                                            insertSpouse.setValue(spouseName, forKey: "name")
                                            insertSpouse.setValue("Female", forKey: "gender")
                                            insertSpouse.setValue(Constant.convertDateStringtoAnotherFormat(dateString: "01-01-1900", fromFormat: "dd-MM-yyyy", toFormat: "dd MMMM yyyy"), forKey: "dateOfBirth")
                                            insertSpouse.setValue("Family & Relatives", forKey: "sourceOfProspect")
                                            insertSpouse.setValue("Married", forKey: "maritalStatus")
                                            insertSpouse.setValue("0", forKey: "totalChildren")
                                            insertSpouse.setValue("Please Choose", forKey: "occupation")
                                            insertSpouse.setValue("Please Choose", forKey: "exactSourceOfProspect")
                                            insertSpouse.setValue("Please Choose", forKey: "exactOccupation")
                                            insertSpouse.setValue("Please Choose", forKey: "income")
                                            insertSpouse.setValue("1", forKey: "exactIncome")
                                            insertSpouse.setValue("", forKey: "homeAddress")
                                            insertSpouse.setValue("", forKey: "workAddress")
                                            insertSpouse.setValue("Please Choose", forKey: "locationAccessibility")
                                            insertSpouse.setValue("Please Choose", forKey: "knowThisPerson")
                                            insertSpouse.setValue("Please Choose", forKey: "howCloseAreYou")
                                            insertSpouse.setValue("Please Choose", forKey: "howInfluenceThisPerson")
                                            insertSpouse.setValue("Please Choose", forKey: "prospectingStatus")
                                            insertSpouse.setValue(name, forKey: "spouseName")
                                            insertSpouse.setValue("", forKey: "fatherName")
                                            insertSpouse.setValue("", forKey: "motherName")
                                            insertSpouse.setValue("Contact:\(id)", forKey: "spouseId")
                                            insertSpouse.setValue("", forKey: "fatherId")
                                            insertSpouse.setValue("", forKey: "motherId")
                                            //deleted CoreData
                                            insertSpouse.setValue("-", forKey: "spouseRemarks")
                                            insertSpouse.setValue("-", forKey: "motherRemarks")
                                            insertSpouse.setValue("-", forKey: "childrenAgeBand")
                                            insertSpouse.setValue("-", forKey: "childrenRemarks")
                                            insertSpouse.setValue("-", forKey: "fatherAgeBand")
                                            insertSpouse.setValue("-", forKey: "fatherRemarks")
                                            insertSpouse.setValue("-", forKey: "motherAgeBand")
                                            insertSpouse.setValue("-", forKey: "spouseAgeBand")
                                            insertSpouse.setValue("-", forKey: "spouseIncome")
                                            insertSpouse.setValue("-", forKey: "spouseOccupation")
                                            do {
                                                try managedContext.save()
                                                prospectSpouseList.append(insertSpouse)
                                            } catch let error as NSError {
                                                print("Could not save. \(error), \(error.userInfo)")
                                            }
                                        }
                                    } catch let error as NSError {
                                        print("Could not fetch. \(error), \(error.userInfo)")
                                    }
                                }
                            } else if (contactLabel == "Father") {
                                fatherName = contactName ?? ""
                                if (contactLabel == "Mother") {
                                    motherName = contactName ?? ""
                                }
                                if (fatherName.isEmpty == false) {
                                    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                                        return
                                    }
                                    let managedContext = appDelegate.persistentContainer.viewContext
                                    let fetchRequestAnotherContact = NSFetchRequest<NSManagedObject>(entityName: "Contact")
                                    let namePredicate = NSPredicate(format: "name == %@", fatherName!)
                                    let agentCodePredicate = NSPredicate(format: "agentCode == %@", CustomUserDefaults.shared.getUsername())
                                    let anotherContactPredicateCompound = NSCompoundPredicate.init(type: .and, subpredicates: [agentCodePredicate,namePredicate])
                                    fetchRequestAnotherContact.predicate = anotherContactPredicateCompound
                                    do {
                                        let contacts = try managedContext.fetch(fetchRequestAnotherContact)
                                        if (contacts.count == 0) {
                                            insertFather.setValue("Please Choose", forKey: "ageBand")
                                            insertFather.setValue("Father:\(timeinserted)", forKey: "id")
                                            insertFather.setValue(CustomUserDefaults.shared.getUsername(), forKey: "agentCode")
                                            insertFather.setValue(fatherName, forKey: "name")
                                            insertFather.setValue("Male", forKey: "gender")
                                            insertFather.setValue(Constant.convertDateStringtoAnotherFormat(dateString: "01-01-1900", fromFormat: "dd-MM-yyyy", toFormat: "dd MMMM yyyy"), forKey: "dateOfBirth")
                                            insertFather.setValue("Family & Relatives", forKey: "sourceOfProspect")
                                            insertFather.setValue("Married", forKey: "maritalStatus")
                                            insertFather.setValue("0", forKey: "totalChildren")
                                            insertFather.setValue("Please Choose", forKey: "occupation")
                                            insertFather.setValue("Please Choose", forKey: "exactSourceOfProspect")
                                            insertFather.setValue("Please Choose", forKey: "exactOccupation")
                                            insertFather.setValue("Please Choose", forKey: "income")
                                            insertFather.setValue("1", forKey: "exactIncome")
                                            insertFather.setValue("", forKey: "homeAddress")
                                            insertFather.setValue("", forKey: "workAddress")
                                            insertFather.setValue("Please Choose", forKey: "locationAccessibility")
                                            insertFather.setValue("Please Choose", forKey: "knowThisPerson")
                                            insertFather.setValue("Please Choose", forKey: "howCloseAreYou")
                                            insertFather.setValue("Please Choose", forKey: "howInfluenceThisPerson")
                                            insertFather.setValue("Please Choose", forKey: "prospectingStatus")
                                            insertFather.setValue(motherName, forKey: "spouseName")
                                            insertFather.setValue("", forKey: "fatherName")
                                            insertFather.setValue("", forKey: "motherName")
                                            insertFather.setValue("Mother:\(timeinserted)", forKey: "spouseId")
                                            insertFather.setValue("", forKey: "fatherId")
                                            insertFather.setValue("", forKey: "motherId")
                                            //deleted CoreData
                                            insertFather.setValue("-", forKey: "spouseRemarks")
                                            insertFather.setValue("-", forKey: "motherRemarks")
                                            insertFather.setValue("-", forKey: "childrenAgeBand")
                                            insertFather.setValue("-", forKey: "childrenRemarks")
                                            insertFather.setValue("-", forKey: "fatherAgeBand")
                                            insertFather.setValue("-", forKey: "fatherRemarks")
                                            insertFather.setValue("-", forKey: "motherAgeBand")
                                            insertFather.setValue("-", forKey: "spouseAgeBand")
                                            insertFather.setValue("-", forKey: "spouseIncome")
                                            insertFather.setValue("-", forKey: "spouseOccupation")
                                            do {
                                                try managedContext.save()
                                                prospectFatherList.append(insertFather)
                                            } catch let error as NSError {
                                                print("Could not save. \(error), \(error.userInfo)")
                                            }
                                            //insert children on father coreData
                                            prospectChildrenList.removeAll()
                                            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                                                return
                                            }
                                            let managedContext = appDelegate.persistentContainer.viewContext
                                            let insertChildren = NSEntityDescription.insertNewObject(forEntityName: "Children", into: managedContext)
                                            insertChildren.setValue("Father:\(timeinserted)", forKey: "userId")
                                            insertChildren.setValue("Children:\(name)\(timeinserted)", forKey: "childrenId")
                                            insertChildren.setValue(CustomUserDefaults.shared.getUsername(), forKey: "agentCode")
                                            insertChildren.setValue(name, forKey: "childrenName")
                                            insertChildren.setValue("Please Choose", forKey: "childrenStatus")
                                            print("Children:\(name)")
                                            do {
                                                try managedContext.save()
                                                prospectChildrenList.append(insertChildren)
                                            } catch let error as NSError {
                                                print("Could not save. \(error), \(error.userInfo)")
                                            }
                                        }
                                    } catch let error as NSError {
                                        print("Could not fetch. \(error), \(error.userInfo)")
                                    }
                                }
                            } else if (contactLabel == "Mother") {
                                motherName = contactName ?? ""
                                if (motherName.isEmpty == false) {
                                    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                                        return
                                    }
                                    let managedContext = appDelegate.persistentContainer.viewContext
                                    let fetchRequestAnotherContact = NSFetchRequest<NSManagedObject>(entityName: "Contact")
                                    let namePredicate = NSPredicate(format: "name == %@", motherName!)
                                    let agentCodePredicate = NSPredicate(format: "agentCode == %@", CustomUserDefaults.shared.getUsername())
                                    let anotherContactPredicateCompound = NSCompoundPredicate.init(type: .and, subpredicates: [agentCodePredicate,namePredicate])
                                    fetchRequestAnotherContact.predicate = anotherContactPredicateCompound
                                    do {
                                        let contacts = try managedContext.fetch(fetchRequestAnotherContact)
                                        if (contacts.count == 0) {
                                            insertMother.setValue("Please Choose", forKey: "ageBand")
                                            insertMother.setValue("Mother:\(timeinserted)", forKey: "id")
                                            insertMother.setValue(CustomUserDefaults.shared.getUsername(), forKey: "agentCode")
                                            insertMother.setValue(motherName, forKey: "name")
                                            insertMother.setValue("Female", forKey: "gender")
                                            insertMother.setValue(Constant.convertDateStringtoAnotherFormat(dateString: "01-01-1900", fromFormat: "dd-MM-yyyy", toFormat: "dd MMMM yyyy"), forKey: "dateOfBirth")
                                            insertMother.setValue("Family & Relatives", forKey: "sourceOfProspect")
                                            insertMother.setValue("Married", forKey: "maritalStatus")
                                            insertMother.setValue("0", forKey: "totalChildren")
                                            insertMother.setValue("Please Choose", forKey: "occupation")
                                            insertMother.setValue("Please Choose", forKey: "exactSourceOfProspect")
                                            insertMother.setValue("Please Choose", forKey: "exactOccupation")
                                            insertMother.setValue("Please Choose", forKey: "income")
                                            insertMother.setValue("1", forKey: "exactIncome")
                                            insertMother.setValue("", forKey: "homeAddress")
                                            insertMother.setValue("", forKey: "workAddress")
                                            insertMother.setValue("Please Choose", forKey: "locationAccessibility")
                                            insertMother.setValue("Please Choose", forKey: "knowThisPerson")
                                            insertMother.setValue("Please Choose", forKey: "howCloseAreYou")
                                            insertMother.setValue("Please Choose", forKey: "howInfluenceThisPerson")
                                            insertMother.setValue("Please Choose", forKey: "prospectingStatus")
                                            insertMother.setValue(fatherName, forKey: "spouseName")
                                            insertMother.setValue("", forKey: "fatherName")
                                            insertMother.setValue("", forKey: "motherName")
                                            insertMother.setValue("Father:\(timeinserted)", forKey: "spouseId")
                                            insertMother.setValue("", forKey: "fatherId")
                                            insertMother.setValue("", forKey: "motherId")
                                            //deleted CoreData
                                            insertMother.setValue("-", forKey: "spouseRemarks")
                                            insertMother.setValue("-", forKey: "motherRemarks")
                                            insertMother.setValue("-", forKey: "childrenAgeBand")
                                            insertMother.setValue("-", forKey: "childrenRemarks")
                                            insertMother.setValue("-", forKey: "fatherAgeBand")
                                            insertMother.setValue("-", forKey: "fatherRemarks")
                                            insertMother.setValue("-", forKey: "motherAgeBand")
                                            insertMother.setValue("-", forKey: "spouseAgeBand")
                                            insertMother.setValue("-", forKey: "spouseIncome")
                                            insertMother.setValue("-", forKey: "spouseOccupation")
                                            do {
                                                try managedContext.save()
                                                prospectMotherList.append(insertMother)
                                            } catch let error as NSError {
                                                print("Could not save. \(error), \(error.userInfo)")
                                            }
                                            //insert children on mother coreData
                                            prospectChildrenList.removeAll()
                                            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                                                return
                                            }
                                            let managedContext = appDelegate.persistentContainer.viewContext
                                            let insertChildren = NSEntityDescription.insertNewObject(forEntityName: "Children", into: managedContext)
                                            insertChildren.setValue("Mother:\(timeinserted)", forKey: "userId")
                                            insertChildren.setValue("Children:\(name)\(timeinserted)", forKey: "childrenId")
                                            insertChildren.setValue(CustomUserDefaults.shared.getUsername(), forKey: "agentCode")
                                            insertChildren.setValue(name, forKey: "childrenName")
                                            insertChildren.setValue("Please Choose", forKey: "childrenStatus")
                                            print("Children:\(name)")
                                            do {
                                                try managedContext.save()
                                                prospectChildrenList.append(insertChildren)
                                            } catch let error as NSError {
                                                print("Could not save. \(error), \(error.userInfo)")
                                            }
                                        }
                                    } catch let error as NSError {
                                        print("Could not fetch. \(error), \(error.userInfo)")
                                    }
                                }
                            }
                            //spouse
                            insertContact.setValue(spouseName, forKey: "spouseName")
                            insertContact.setValue("Spouse:\(timeinserted)", forKey: "spouseId")
                            //father
                            insertContact.setValue(fatherName, forKey: "fatherName")
                            insertContact.setValue("Father:\(timeinserted)", forKey: "fatherId")
                            //mother
                            insertContact.setValue(motherName, forKey: "motherName")
                            insertContact.setValue("Mother:\(timeinserted)", forKey: "motherId")
                            //deleted CoreData
                            insertContact.setValue("-", forKey: "spouseRemarks")
                            insertContact.setValue("-", forKey: "motherRemarks")
                            insertContact.setValue("-", forKey: "childrenAgeBand")
                            insertContact.setValue("-", forKey: "childrenRemarks")
                            insertContact.setValue("-", forKey: "fatherAgeBand")
                            insertContact.setValue("-", forKey: "fatherRemarks")
                            insertContact.setValue("-", forKey: "motherAgeBand")
                            insertContact.setValue("-", forKey: "spouseAgeBand")
                            insertContact.setValue("-", forKey: "spouseIncome")
                            insertContact.setValue("-", forKey: "spouseOccupation")
                        }
                    } else {
                        insertContact.setValue("", forKey: "spouseName")
                        insertContact.setValue("", forKey: "fatherName")
                        insertContact.setValue("", forKey: "motherName")
                        insertContact.setValue("", forKey: "spouseId")
                        insertContact.setValue("", forKey: "fatherId")
                        insertContact.setValue("", forKey: "motherId")
                        //deleted CoreData
                        insertContact.setValue("-", forKey: "spouseRemarks")
                        insertContact.setValue("-", forKey: "motherRemarks")
                        insertContact.setValue("-", forKey: "childrenAgeBand")
                        insertContact.setValue("-", forKey: "childrenRemarks")
                        insertContact.setValue("-", forKey: "fatherAgeBand")
                        insertContact.setValue("-", forKey: "fatherRemarks")
                        insertContact.setValue("-", forKey: "motherAgeBand")
                        insertContact.setValue("-", forKey: "spouseAgeBand")
                        insertContact.setValue("-", forKey: "spouseIncome")
                        insertContact.setValue("-", forKey: "spouseOccupation")
                    }
                    
                    let insertPhone = NSEntityDescription.insertNewObject(forEntityName: "Phone", into: managedContext)
                    insertPhone.setValue("Contact:\(id)", forKey: "userId")
                    insertPhone.setValue("Phone:\(id)", forKey: "phoneId")
                    insertPhone.setValue(CustomUserDefaults.shared.getUsername(), forKey: "agentCode")
                    insertPhone.setValue("Home", forKey: "phoneName")
                    insertPhone.setValue("\(primaryPhoneNumberStr!)", forKey: "phoneNumber")
                    
                    let insertEmail = NSEntityDescription.insertNewObject(forEntityName: "Email", into: managedContext)
                    insertEmail.setValue("Contact:\(id)", forKey: "userId")
                    insertEmail.setValue("Email:\(id)", forKey: "emailId")
                    insertEmail.setValue(CustomUserDefaults.shared.getUsername(), forKey: "agentCode")
                    insertEmail.setValue("Email 1", forKey: "emailName")
                    insertEmail.setValue("\(emailAddress!)", forKey: "emailAddress")
                    
                    do {
                        try managedContext.save()
                        prospectList.append(insertContact)
                        prospectPhoneList.append(insertPhone)
                        prospectEmailList.append(insertEmail)
                    } catch let error as NSError {
                        print("Could not save. \(error), \(error.userInfo)")
                    }
                } else {
                    DispatchQueue.main.async {
                        AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Contact already taken by other Agent", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
                    }
                }
            } catch let error as NSError {
                print("Could not fetch. \(error), \(error.userInfo)")
            }
        }
    }
    
//    @available(iOS 9.0, *)
//    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
//        var spouseName: String!
//        var sonName: String!
//        var fatherName: String!
//        var motherName: String!
//        var brotherName: String!
//        var sisterName: String!
//        var birthday: String!
//        var occupation: String!
//        var primaryPhoneNumberStr: String!
//        var primaryPhoneNumberStr3: String!
//        var emailAddress: String!
//        var brotherCount: Int!
//        var sisterCount: Int!
//        var brotherSisterCount: Int!
//        var sonCount: Int!
//
//        //core data
//        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
//            return
//        }
//        let managedContext = appDelegate.persistentContainer.viewContext
//        // You can fetch selected name and number in the following way
//        //identifier
//        let id: String = contact.identifier
//        // user name
//        let givenName:String = contact.givenName ?? ""
//        let familyName:String = contact.familyName ?? ""
//        let name = "\(givenName) \(familyName)"
//
//        // user phone number
//        let userPhoneNumbers:[CNLabeledValue<CNPhoneNumber>] = contact.phoneNumbers
//        if (userPhoneNumbers.count > 0) {
//            let firstPhoneNumber:CNPhoneNumber = userPhoneNumbers[0].value
//            // user phone number string
//            let primaryPhoneNumberStr1 = firstPhoneNumber.stringValue ?? ""
//            let primaryPhoneNumberStr2 = primaryPhoneNumberStr1.components(separatedBy: .whitespaces)
//            if primaryPhoneNumberStr2[0].range(of:"+") != nil {
//                primaryPhoneNumberStr3 = primaryPhoneNumberStr2.dropFirst().joined()
//            } else {
//                primaryPhoneNumberStr3 = primaryPhoneNumberStr2.joined()
//            }
//            let primaryPhoneNumberStr4 = primaryPhoneNumberStr3.replacingOccurrences(of: "-", with: "", options: .literal, range: nil)
//            let primaryPhoneNumberStr5 = primaryPhoneNumberStr4.replacingOccurrences(of: "(", with: "", options: .literal, range: nil)
//            let primaryPhoneNumberStr6 = primaryPhoneNumberStr5.replacingOccurrences(of: ")", with: "", options: .literal, range: nil)
//            if primaryPhoneNumberStr6.prefix(1) == "0" {
//                primaryPhoneNumberStr = primaryPhoneNumberStr6.replacingOccurrences(of: "0", with: "+62", options: .literal, range: nil)
//            } else if primaryPhoneNumberStr6.range(of:"+62") == nil {
//                primaryPhoneNumberStr = "+62\(primaryPhoneNumberStr6)"
//            } else {
//                primaryPhoneNumberStr = primaryPhoneNumberStr6
//            }
//            print("primaryPhoneNumberStr1:\(primaryPhoneNumberStr1)")
//            print("primaryPhoneNumberStr:\(primaryPhoneNumberStr!)")
//        } else {
//            primaryPhoneNumberStr = ""
//        }
//
//        // user email address
//        if (contact.emailAddresses.count > 0) {
//            emailAddress = contact.emailAddresses.first?.value as! String ?? ""
//        } else {
//            emailAddress = ""
//        }
//
//        // user birthday
//        let day = contact.birthday?.day ?? 00
//        let month = contact.birthday?.month ?? 00
//        let year = contact.birthday?.year ?? 0000
//        let temporaryBirthday = "\(day)-\(month)-\(year)"
//        if (temporaryBirthday == "0-0-0") {
//            birthday = Constant.convertDateStringtoAnotherFormat(dateString: "01-01-1900", fromFormat: "dd-MM-yyyy", toFormat: "dd MMMM yyyy")
//        } else {
//            birthday = Constant.convertDateStringtoAnotherFormat(dateString: "\(day)-\(month)-\(year)", fromFormat: "dd-MM-yyyy", toFormat: "dd MMMM yyyy")
//        }
//
//        // user occupation
//        let temporaryOccupation = contact.jobTitle.description
//        if (temporaryOccupation == "") {
//            occupation = "Please Choose"
//        } else {
//            occupation = contact.jobTitle.description
//        }
//
//        print("id:\(id), username:\(name), primaryPhoneNumber:\(primaryPhoneNumberStr!), email:\(emailAddress!), birthday:\(birthday!), occupation:\(occupation!)")
//
//        let fetchRequestContact = NSFetchRequest<NSManagedObject>(entityName: "Contact")
//        let namePredicate = NSPredicate(format: "name == %@", name)
//        fetchRequestContact.predicate = namePredicate
//        do {
//            totalProspect = try managedContext.count(for: fetchRequestContact)
//            print("totalContact:\(totalProspect!)")
//            if (totalProspect! == 0) {
//                ///to CoreData
//                let insertContact = NSEntityDescription.insertNewObject(forEntityName: "Contact", into: managedContext)
//                let insertSpouse = NSEntityDescription.insertNewObject(forEntityName: "Contact", into: managedContext)
//                let insertFather = NSEntityDescription.insertNewObject(forEntityName: "Contact", into: managedContext)
//                let insertMother = NSEntityDescription.insertNewObject(forEntityName: "Contact", into: managedContext)
//                let insertBro = NSEntityDescription.insertNewObject(forEntityName: "Contact", into: managedContext)
//                let insertSis = NSEntityDescription.insertNewObject(forEntityName: "Contact", into: managedContext)
//                let convertedBirthday = Constant.convertDateStringtoAnotherFormat(dateString: birthday!, fromFormat: "dd MMMM yyyy", toFormat: "dd-MM-yyyy")
//                let age = Constant.countAge(birthday: convertedBirthday)
//                if (age < 26) {
//                    insertContact.setValue("≤ 25 years", forKey: "ageBand")
//                } else if (25 < age && age < 36) {
//                    insertContact.setValue("26 - 35 years", forKey: "ageBand")
//                } else if (35 < age && age < 46) {
//                    insertContact.setValue("36 - 45 years", forKey: "ageBand")
//                } else if (45 < age && age < 55) {
//                    insertContact.setValue("46 - 55 years", forKey: "ageBand")
//                } else if (age > 55) {
//                    insertContact.setValue("≥ 56 years", forKey: "ageBand")
//                }
//                insertContact.setValue("Contact:\(id)", forKey: "id")
//                insertContact.setValue(CustomUserDefaults.shared.getUsername(), forKey: "agentCode")
//                insertContact.setValue(name, forKey: "name")
//                insertContact.setValue("Please Choose", forKey: "gender")
//                insertContact.setValue(birthday!, forKey: "dateOfBirth")
//                insertContact.setValue("Please Choose", forKey: "sourceOfProspect")
//                insertContact.setValue("Please Choose", forKey: "maritalStatus")
//                insertContact.setValue("0", forKey: "totalChildren")
//                insertContact.setValue(occupation!, forKey: "occupation")
//                insertContact.setValue("Please Choose", forKey: "exactSourceOfProspect")
//                insertContact.setValue("Please Choose", forKey: "exactOccupation")
//                insertContact.setValue("Please Choose", forKey: "income")
//                insertContact.setValue("1", forKey: "exactIncome")
//                // user address
//                let addresses = contact.postalAddresses
//                if (addresses.count > 0) {
//                    var subLocality: String!
//                    var subAdministrativeArea: String!
//                    for address in addresses {
//                        let rawAddressLabel = address.label?.description
//                        let rawAddressLabel1 = rawAddressLabel!.replacingOccurrences(of: "_$!<", with: "", options: .literal, range: nil)
//                        let addressLabel = rawAddressLabel1.replacingOccurrences(of: ">!$_", with: "", options: .literal, range: nil)
//                        let street = address.value.street
//                        if #available(iOS 10.3, *) {
//                            subLocality = address.value.subLocality
//                        } else {
//                            subLocality = ""
//                        }
//                        let city = address.value.city
//                        if #available(iOS 10.3, *) {
//                            subAdministrativeArea = address.value.subAdministrativeArea
//                        } else {
//                            subAdministrativeArea = ""
//                        }
//                        let state = address.value.state
//                        let postalCode = address.value.postalCode
//                        let country = address.value.country
//                        if (addressLabel == "Home") {
//                            print("inserted Home Address")
//                            insertContact.setValue("\(street), \(city), \(state), \(postalCode), \(country)", forKey: "homeAddress")
//                        } else if (addressLabel == "Work") {
//                            print("inserted Work Address")
//                            insertContact.setValue("\(street), \(city), \(state), \(postalCode), \(country)", forKey: "workAddress")
//                        }
//                    }
//                } else {
//                    insertContact.setValue("", forKey: "homeAddress")
//                    insertContact.setValue("", forKey: "workAddress")
//                }
//                insertContact.setValue("Please Choose", forKey: "locationAccessibility")
//                insertContact.setValue("Please Choose", forKey: "knowThisPerson")
//                insertContact.setValue("Please Choose", forKey: "howCloseAreYou")
//                insertContact.setValue("Please Choose", forKey: "howInfluenceThisPerson")
//                insertContact.setValue("Please Choose", forKey: "prospectingStatus")
//                //relation
//                let contactRelation = contact.contactRelations
//                if contactRelation.count > 0 {
//                    for contact in contactRelation {
//                        let rawContactLabel = contact.label?.description
//                        let rawContactLabel1 = rawContactLabel!.replacingOccurrences(of: "_$!<", with: "", options: .literal, range: nil)
//                        let contactLabel = rawContactLabel1.replacingOccurrences(of: ">!$_", with: "", options: .literal, range: nil)
//                        let contactName = contact.value.name
//                        print("contact label:\(contactLabel) with name:\(contactName)")
//                        if (contactLabel == "Son") {
//                            sonName = contactName ?? ""
//                            sonCount = sonName.count
//                            if (sonName.isEmpty == false) {
//                                let insertChildren = NSEntityDescription.insertNewObject(forEntityName: "Children", into: managedContext)
//                                insertChildren.setValue("Contact:\(id)", forKey: "userId")
//                                insertChildren.setValue("Children:\(sonName)\(id)", forKey: "childrenId")
//                                insertChildren.setValue(CustomUserDefaults.shared.getUsername(), forKey: "agentCode")
//                                insertChildren.setValue(sonName, forKey: "childrenName")
//                                insertChildren.setValue("Please Choose", forKey: "childrenStatus")
//                                do {
//                                    try managedContext.save()
//                                    prospectChildrenList.append(insertChildren)
//                                } catch let error as NSError {
//                                    print("Could not save. \(error), \(error.userInfo)")
//                                }
//                            }
//                        } else if (contactLabel == "Brother") {
//                            brotherName = contactName ?? ""
//                            brotherCount = brotherName.count
//                            //brother
//                            if (brotherName.isEmpty == false) {
//                                let insertBrother = NSEntityDescription.insertNewObject(forEntityName: "Brother", into: managedContext)
//                                insertBrother.setValue("Contact:\(id)", forKey: "userId")
//                                insertBrother.setValue("Brother:\(brotherName)\(id)", forKey: "brotherId")
//                                insertBrother.setValue(CustomUserDefaults.shared.getUsername(), forKey: "agentCode")
//                                insertBrother.setValue(brotherName, forKey: "brotherName")
//                                insertBrother.setValue("Please Choose", forKey: "brotherAgeBand")
//                                //brother in contact
//                                let timeinserted = Date().ticks
//                                insertBro.setValue("Please Choose", forKey: "ageBand")
//                                insertBro.setValue("Brothers:\(timeinserted)", forKey: "id")
//                                insertBro.setValue(CustomUserDefaults.shared.getUsername(), forKey: "agentCode")
//                                insertBro.setValue(brotherName, forKey: "name")
//                                insertBro.setValue("Male", forKey: "gender")
//                                insertBro.setValue(Constant.convertDateStringtoAnotherFormat(dateString: "01-01-1900", fromFormat: "dd-MM-yyyy", toFormat: "dd MMMM yyyy"), forKey: "dateOfBirth")
//                                insertBro.setValue("Family & Relatives", forKey: "sourceOfProspect")
//                                insertBro.setValue("Please Choose", forKey: "maritalStatus")
//                                insertBro.setValue("0", forKey: "totalChildren")
//                                insertBro.setValue("Please Choose", forKey: "occupation")
//                                insertBro.setValue("Please Choose", forKey: "exactSourceOfProspect")
//                                insertBro.setValue("Please Choose", forKey: "exactOccupation")
//                                insertBro.setValue("Please Choose", forKey: "income")
//                                insertBro.setValue("1", forKey: "exactIncome")
//                                insertBro.setValue("", forKey: "homeAddress")
//                                insertBro.setValue("", forKey: "workAddress")
//                                insertBro.setValue("Please Choose", forKey: "locationAccessibility")
//                                insertBro.setValue("Please Choose", forKey: "knowThisPerson")
//                                insertBro.setValue("Please Choose", forKey: "howCloseAreYou")
//                                insertBro.setValue("Please Choose", forKey: "howInfluenceThisPerson")
//                                insertBro.setValue("Please Choose", forKey: "prospectingStatus")
//                                insertBro.setValue("", forKey: "spouseName")
//                                insertBro.setValue("", forKey: "fatherName")
//                                insertBro.setValue("", forKey: "motherName")
//                                //deleted CoreData
//                                insertBro.setValue("-", forKey: "spouseRemarks")
//                                insertBro.setValue("-", forKey: "motherRemarks")
//                                insertBro.setValue("-", forKey: "childrenAgeBand")
//                                insertBro.setValue("-", forKey: "childrenRemarks")
//                                insertBro.setValue("-", forKey: "fatherAgeBand")
//                                insertBro.setValue("-", forKey: "fatherRemarks")
//                                insertBro.setValue("-", forKey: "motherAgeBand")
//                                insertBro.setValue("-", forKey: "spouseAgeBand")
//                                insertBro.setValue("-", forKey: "spouseIncome")
//                                insertBro.setValue("-", forKey: "spouseOccupation")
//                                do {
//                                    try managedContext.save()
//                                    prospectBrotherList.append(insertBrother)
//                                    prospectBroList.append(insertBro)
//                                } catch let error as NSError {
//                                    print("Could not save. \(error), \(error.userInfo)")
//                                }
//                            }
//                        } else if (contactLabel == "Sister") {
//                            sisterName = contactName ?? ""
//                            sisterCount = sisterName.count
//                            //sister
//                            if (sisterName.isEmpty == false) {
//                                let insertSister = NSEntityDescription.insertNewObject(forEntityName: "Sister", into: managedContext)
//                                insertSister.setValue("Contact:\(id)", forKey: "userId")
//                                insertSister.setValue("Sister:\(sisterName)\(id)", forKey: "sisterId")
//                                insertSister.setValue(CustomUserDefaults.shared.getUsername(), forKey: "agentCode")
//                                insertSister.setValue(sisterName, forKey: "sisterName")
//                                insertSister.setValue("Please Choose", forKey: "sisterAgeBand")
//                                //sister in contact
//                                let timeinserted = Date().ticks
//                                insertSis.setValue("Please Choose", forKey: "ageBand")
//                                insertSis.setValue("Sisters:\(timeinserted)", forKey: "id")
//                                insertSis.setValue(CustomUserDefaults.shared.getUsername(), forKey: "agentCode")
//                                insertSis.setValue(sisterName, forKey: "name")
//                                insertSis.setValue("Female", forKey: "gender")
//                                insertSis.setValue(Constant.convertDateStringtoAnotherFormat(dateString: "01-01-1900", fromFormat: "dd-MM-yyyy", toFormat: "dd MMMM yyyy"), forKey: "dateOfBirth")
//                                insertSis.setValue("Family & Relatives", forKey: "sourceOfProspect")
//                                insertSis.setValue("Please Choose", forKey: "maritalStatus")
//                                insertSis.setValue("0", forKey: "totalChildren")
//                                insertSis.setValue("Please Choose", forKey: "occupation")
//                                insertSis.setValue("Please Choose", forKey: "exactSourceOfProspect")
//                                insertSis.setValue("Please Choose", forKey: "exactOccupation")
//                                insertSis.setValue("Please Choose", forKey: "income")
//                                insertSis.setValue("1", forKey: "exactIncome")
//                                insertSis.setValue("", forKey: "homeAddress")
//                                insertSis.setValue("", forKey: "workAddress")
//                                insertSis.setValue("Please Choose", forKey: "locationAccessibility")
//                                insertSis.setValue("Please Choose", forKey: "knowThisPerson")
//                                insertSis.setValue("Please Choose", forKey: "howCloseAreYou")
//                                insertSis.setValue("Please Choose", forKey: "howInfluenceThisPerson")
//                                insertSis.setValue("Please Choose", forKey: "prospectingStatus")
//                                insertSis.setValue("", forKey: "spouseName")
//                                insertSis.setValue("", forKey: "fatherName")
//                                insertSis.setValue("", forKey: "motherName")
//                                //deleted CoreData
//                                insertSis.setValue("-", forKey: "spouseRemarks")
//                                insertSis.setValue("-", forKey: "motherRemarks")
//                                insertSis.setValue("-", forKey: "childrenAgeBand")
//                                insertSis.setValue("-", forKey: "childrenRemarks")
//                                insertSis.setValue("-", forKey: "fatherAgeBand")
//                                insertSis.setValue("-", forKey: "fatherRemarks")
//                                insertSis.setValue("-", forKey: "motherAgeBand")
//                                insertSis.setValue("-", forKey: "spouseAgeBand")
//                                insertSis.setValue("-", forKey: "spouseIncome")
//                                insertSis.setValue("-", forKey: "spouseOccupation")
//                                do {
//                                    try managedContext.save()
//                                    prospectSisterList.append(insertSister)
//                                    prospectSisList.append(insertSis)
//                                } catch let error as NSError {
//                                    print("Could not save. \(error), \(error.userInfo)")
//                                }
//                            }
//                        } else if (contactLabel == "Spouse") {
//                            spouseName = contactName ?? ""
//                            if (spouseName.isEmpty == false) {
//                                guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
//                                    return
//                                }
//                                let managedContext = appDelegate.persistentContainer.viewContext
//                                let fetchRequestAnotherContact = NSFetchRequest<NSManagedObject>(entityName: "Contact")
//                                let namePredicate = NSPredicate(format: "name == %@", spouseName!)
//                                let agentCodePredicate = NSPredicate(format: "agentCode == %@", CustomUserDefaults.shared.getUsername())
//                                let anotherContactPredicateCompound = NSCompoundPredicate.init(type: .and, subpredicates: [agentCodePredicate,namePredicate])
//                                fetchRequestAnotherContact.predicate = anotherContactPredicateCompound
//                                do {
//                                    let contacts = try managedContext.fetch(fetchRequestAnotherContact)
//                                    if (contacts.count == 0) {
//                                        let timeinserted = Date().ticks
//                                        insertSpouse.setValue("Please Choose", forKey: "ageBand")
//                                        insertSpouse.setValue("Spouse:\(timeinserted)", forKey: "id")
//                                        insertSpouse.setValue(CustomUserDefaults.shared.getUsername(), forKey: "agentCode")
//                                        insertSpouse.setValue(spouseName, forKey: "name")
//                                        insertSpouse.setValue("Female", forKey: "gender")
//                                        insertSpouse.setValue(Constant.convertDateStringtoAnotherFormat(dateString: "01-01-1900", fromFormat: "dd-MM-yyyy", toFormat: "dd MMMM yyyy"), forKey: "dateOfBirth")
//                                        insertSpouse.setValue("Family & Relatives", forKey: "sourceOfProspect")
//                                        insertSpouse.setValue("Married", forKey: "maritalStatus")
//                                        insertSpouse.setValue("0", forKey: "totalChildren")
//                                        insertSpouse.setValue("Please Choose", forKey: "occupation")
//                                        insertSpouse.setValue("Please Choose", forKey: "exactSourceOfProspect")
//                                        insertSpouse.setValue("Please Choose", forKey: "exactOccupation")
//                                        insertSpouse.setValue("Please Choose", forKey: "income")
//                                        insertSpouse.setValue("1", forKey: "exactIncome")
//                                        insertSpouse.setValue("", forKey: "homeAddress")
//                                        insertSpouse.setValue("", forKey: "workAddress")
//                                        insertSpouse.setValue("Please Choose", forKey: "locationAccessibility")
//                                        insertSpouse.setValue("Please Choose", forKey: "knowThisPerson")
//                                        insertSpouse.setValue("Please Choose", forKey: "howCloseAreYou")
//                                        insertSpouse.setValue("Please Choose", forKey: "howInfluenceThisPerson")
//                                        insertSpouse.setValue("Please Choose", forKey: "prospectingStatus")
//                                        insertSpouse.setValue(name, forKey: "spouseName")
//                                        insertSpouse.setValue("", forKey: "fatherName")
//                                        insertSpouse.setValue("", forKey: "motherName")
//                                        //deleted CoreData
//                                        insertSpouse.setValue("-", forKey: "spouseRemarks")
//                                        insertSpouse.setValue("-", forKey: "motherRemarks")
//                                        insertSpouse.setValue("-", forKey: "childrenAgeBand")
//                                        insertSpouse.setValue("-", forKey: "childrenRemarks")
//                                        insertSpouse.setValue("-", forKey: "fatherAgeBand")
//                                        insertSpouse.setValue("-", forKey: "fatherRemarks")
//                                        insertSpouse.setValue("-", forKey: "motherAgeBand")
//                                        insertSpouse.setValue("-", forKey: "spouseAgeBand")
//                                        insertSpouse.setValue("-", forKey: "spouseIncome")
//                                        insertSpouse.setValue("-", forKey: "spouseOccupation")
//                                        do {
//                                            try managedContext.save()
//                                            prospectSpouseList.append(insertSpouse)
//                                        } catch let error as NSError {
//                                            print("Could not save. \(error), \(error.userInfo)")
//                                        }
//                                    }
//                                } catch let error as NSError {
//                                    print("Could not fetch. \(error), \(error.userInfo)")
//                                }
//                            }
//                        } else if (contactLabel == "Father") {
//                            fatherName = contactName ?? ""
//                            if (fatherName.isEmpty == false) {
//                                guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
//                                    return
//                                }
//                                let managedContext = appDelegate.persistentContainer.viewContext
//                                let fetchRequestAnotherContact = NSFetchRequest<NSManagedObject>(entityName: "Contact")
//                                let namePredicate = NSPredicate(format: "name == %@", fatherName!)
//                                let agentCodePredicate = NSPredicate(format: "agentCode == %@", CustomUserDefaults.shared.getUsername())
//                                let anotherContactPredicateCompound = NSCompoundPredicate.init(type: .and, subpredicates: [agentCodePredicate,namePredicate])
//                                fetchRequestAnotherContact.predicate = anotherContactPredicateCompound
//                                do {
//                                    let contacts = try managedContext.fetch(fetchRequestAnotherContact)
//                                    if (contacts.count == 0) {
//                                        let timeinserted = Date().ticks
//                                        insertFather.setValue("Please Choose", forKey: "ageBand")
//                                        insertFather.setValue("Father:\(timeinserted)", forKey: "id")
//                                        insertFather.setValue(CustomUserDefaults.shared.getUsername(), forKey: "agentCode")
//                                        insertFather.setValue(fatherName, forKey: "name")
//                                        insertFather.setValue("Male", forKey: "gender")
//                                        insertFather.setValue(Constant.convertDateStringtoAnotherFormat(dateString: "01-01-1900", fromFormat: "dd-MM-yyyy", toFormat: "dd MMMM yyyy"), forKey: "dateOfBirth")
//                                        insertFather.setValue("Family & Relatives", forKey: "sourceOfProspect")
//                                        insertFather.setValue("Married", forKey: "maritalStatus")
//                                        insertFather.setValue("0", forKey: "totalChildren")
//                                        insertFather.setValue("Please Choose", forKey: "occupation")
//                                        insertFather.setValue("Please Choose", forKey: "exactSourceOfProspect")
//                                        insertFather.setValue("Please Choose", forKey: "exactOccupation")
//                                        insertFather.setValue("Please Choose", forKey: "income")
//                                        insertFather.setValue("1", forKey: "exactIncome")
//                                        insertFather.setValue("", forKey: "homeAddress")
//                                        insertFather.setValue("", forKey: "workAddress")
//                                        insertFather.setValue("Please Choose", forKey: "locationAccessibility")
//                                        insertFather.setValue("Please Choose", forKey: "knowThisPerson")
//                                        insertFather.setValue("Please Choose", forKey: "howCloseAreYou")
//                                        insertFather.setValue("Please Choose", forKey: "howInfluenceThisPerson")
//                                        insertFather.setValue("Please Choose", forKey: "prospectingStatus")
//                                        insertFather.setValue(motherName, forKey: "spouseName")
//                                        insertFather.setValue("", forKey: "fatherName")
//                                        insertFather.setValue("", forKey: "motherName")
//                                        //deleted CoreData
//                                        insertFather.setValue("-", forKey: "spouseRemarks")
//                                        insertFather.setValue("-", forKey: "motherRemarks")
//                                        insertFather.setValue("-", forKey: "childrenAgeBand")
//                                        insertFather.setValue("-", forKey: "childrenRemarks")
//                                        insertFather.setValue("-", forKey: "fatherAgeBand")
//                                        insertFather.setValue("-", forKey: "fatherRemarks")
//                                        insertFather.setValue("-", forKey: "motherAgeBand")
//                                        insertFather.setValue("-", forKey: "spouseAgeBand")
//                                        insertFather.setValue("-", forKey: "spouseIncome")
//                                        insertFather.setValue("-", forKey: "spouseOccupation")
//                                        do {
//                                            try managedContext.save()
//                                            prospectFatherList.append(insertFather)
//                                        } catch let error as NSError {
//                                            print("Could not save. \(error), \(error.userInfo)")
//                                        }
//                                    }
//                                } catch let error as NSError {
//                                    print("Could not fetch. \(error), \(error.userInfo)")
//                                }
//                            }
//                        } else if (contactLabel == "Mother") {
//                            motherName = contactName ?? ""
//                            if (motherName.isEmpty == false) {
//                                guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
//                                    return
//                                }
//                                let managedContext = appDelegate.persistentContainer.viewContext
//                                let fetchRequestAnotherContact = NSFetchRequest<NSManagedObject>(entityName: "Contact")
//                                let namePredicate = NSPredicate(format: "name == %@", motherName!)
//                                let agentCodePredicate = NSPredicate(format: "agentCode == %@", CustomUserDefaults.shared.getUsername())
//                                let anotherContactPredicateCompound = NSCompoundPredicate.init(type: .and, subpredicates: [agentCodePredicate,namePredicate])
//                                fetchRequestAnotherContact.predicate = anotherContactPredicateCompound
//                                do {
//                                    let contacts = try managedContext.fetch(fetchRequestAnotherContact)
//                                    if (contacts.count == 0) {
//                                        let timeinserted = Date().ticks
//                                        insertMother.setValue("Please Choose", forKey: "ageBand")
//                                        insertMother.setValue("Mother:\(timeinserted)", forKey: "id")
//                                        insertMother.setValue(CustomUserDefaults.shared.getUsername(), forKey: "agentCode")
//                                        insertMother.setValue(motherName, forKey: "name")
//                                        insertMother.setValue("Female", forKey: "gender")
//                                        insertMother.setValue(Constant.convertDateStringtoAnotherFormat(dateString: "01-01-1900", fromFormat: "dd-MM-yyyy", toFormat: "dd MMMM yyyy"), forKey: "dateOfBirth")
//                                        insertMother.setValue("Family & Relatives", forKey: "sourceOfProspect")
//                                        insertMother.setValue("Married", forKey: "maritalStatus")
//                                        insertMother.setValue("0", forKey: "totalChildren")
//                                        insertMother.setValue("Please Choose", forKey: "occupation")
//                                        insertMother.setValue("Please Choose", forKey: "exactSourceOfProspect")
//                                        insertMother.setValue("Please Choose", forKey: "exactOccupation")
//                                        insertMother.setValue("Please Choose", forKey: "income")
//                                        insertMother.setValue("1", forKey: "exactIncome")
//                                        insertMother.setValue("", forKey: "homeAddress")
//                                        insertMother.setValue("", forKey: "workAddress")
//                                        insertMother.setValue("Please Choose", forKey: "locationAccessibility")
//                                        insertMother.setValue("Please Choose", forKey: "knowThisPerson")
//                                        insertMother.setValue("Please Choose", forKey: "howCloseAreYou")
//                                        insertMother.setValue("Please Choose", forKey: "howInfluenceThisPerson")
//                                        insertMother.setValue("Please Choose", forKey: "prospectingStatus")
//                                        insertMother.setValue(fatherName, forKey: "spouseName")
//                                        insertMother.setValue("", forKey: "fatherName")
//                                        insertMother.setValue("", forKey: "motherName")
//                                        //deleted CoreData
//                                        insertMother.setValue("-", forKey: "spouseRemarks")
//                                        insertMother.setValue("-", forKey: "motherRemarks")
//                                        insertMother.setValue("-", forKey: "childrenAgeBand")
//                                        insertMother.setValue("-", forKey: "childrenRemarks")
//                                        insertMother.setValue("-", forKey: "fatherAgeBand")
//                                        insertMother.setValue("-", forKey: "fatherRemarks")
//                                        insertMother.setValue("-", forKey: "motherAgeBand")
//                                        insertMother.setValue("-", forKey: "spouseAgeBand")
//                                        insertMother.setValue("-", forKey: "spouseIncome")
//                                        insertMother.setValue("-", forKey: "spouseOccupation")
//                                        do {
//                                            try managedContext.save()
//                                            prospectMotherList.append(insertMother)
//                                        } catch let error as NSError {
//                                            print("Could not save. \(error), \(error.userInfo)")
//                                        }
//                                    }
//                                } catch let error as NSError {
//                                    print("Could not fetch. \(error), \(error.userInfo)")
//                                }
//                            }
//                        }
//                        //spouse
//                        insertContact.setValue(spouseName, forKey: "spouseName")
//                        //father
//                        insertContact.setValue(fatherName, forKey: "fatherName")
//                        //mother
//                        insertContact.setValue(motherName, forKey: "motherName")
//                        //deleted CoreData
//                        insertContact.setValue("-", forKey: "spouseRemarks")
//                        insertContact.setValue("-", forKey: "motherRemarks")
//                        insertContact.setValue("-", forKey: "childrenAgeBand")
//                        insertContact.setValue("-", forKey: "childrenRemarks")
//                        insertContact.setValue("-", forKey: "fatherAgeBand")
//                        insertContact.setValue("-", forKey: "fatherRemarks")
//                        insertContact.setValue("-", forKey: "motherAgeBand")
//                        insertContact.setValue("-", forKey: "spouseAgeBand")
//                        insertContact.setValue("-", forKey: "spouseIncome")
//                        insertContact.setValue("-", forKey: "spouseOccupation")
//                    }
//                } else {
//                    insertContact.setValue("", forKey: "spouseName")
//                    insertContact.setValue("", forKey: "fatherName")
//                    insertContact.setValue("", forKey: "motherName")
//                    //deleted CoreData
//                    insertContact.setValue("-", forKey: "spouseRemarks")
//                    insertContact.setValue("-", forKey: "motherRemarks")
//                    insertContact.setValue("-", forKey: "childrenAgeBand")
//                    insertContact.setValue("-", forKey: "childrenRemarks")
//                    insertContact.setValue("-", forKey: "fatherAgeBand")
//                    insertContact.setValue("-", forKey: "fatherRemarks")
//                    insertContact.setValue("-", forKey: "motherAgeBand")
//                    insertContact.setValue("-", forKey: "spouseAgeBand")
//                    insertContact.setValue("-", forKey: "spouseIncome")
//                    insertContact.setValue("-", forKey: "spouseOccupation")
//                }
//
//                let insertPhone = NSEntityDescription.insertNewObject(forEntityName: "Phone", into: managedContext)
//                insertPhone.setValue("Contact:\(id)", forKey: "userId")
//                insertPhone.setValue("Phone:\(id)", forKey: "phoneId")
//                insertPhone.setValue(CustomUserDefaults.shared.getUsername(), forKey: "agentCode")
//                insertPhone.setValue("Home", forKey: "phoneName")
//                insertPhone.setValue("\(primaryPhoneNumberStr!)", forKey: "phoneNumber")
//
//                let insertEmail = NSEntityDescription.insertNewObject(forEntityName: "Email", into: managedContext)
//                insertEmail.setValue("Contact:\(id)", forKey: "userId")
//                insertEmail.setValue("Email:\(id)", forKey: "emailId")
//                insertEmail.setValue(CustomUserDefaults.shared.getUsername(), forKey: "agentCode")
//                insertEmail.setValue("Email 1", forKey: "emailName")
//                insertEmail.setValue("\(emailAddress!)", forKey: "emailAddress")
//
//                do {
//                    try managedContext.save()
//                    prospectList.append(insertContact)
//                    prospectPhoneList.append(insertPhone)
//                    prospectEmailList.append(insertEmail)
//                } catch let error as NSError {
//                    print("Could not save. \(error), \(error.userInfo)")
//                }
//            } else {
//                DispatchQueue.main.async {
//                    AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Contact already taken by other Agent", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
//                }
//            }
//        } catch let error as NSError {
//            print("Could not fetch. \(error), \(error.userInfo)")
//        }
//    }
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}

//extension AlamofireBaseParent {
//    func hideKeyboardWhenTappedOutside() {
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.closeKeyboard))
//        view.addGestureRecognizer(tap)
//    }
//
//    func closeKeyboard() {
//        view.endEditing(true)
//    }
//}
