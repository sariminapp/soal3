//
//  AddConsultationViewController.swift
//  soal3
//
//  Created by Oscar Perdanakusuma Adipati on 28/12/20.
//

import Foundation
import UIKit
import CoreData

class AddConsultationViewController: BaseParentController {
    @IBOutlet weak var txtKlinik: UITextField!
    @IBOutlet weak var txtDoctorName: UITextField!
    @IBOutlet weak var txtDiagnosaPrimer: UITextView!
    @IBOutlet weak var txtDiagnosaSekunder: UITextView!
    @IBOutlet weak var txtTindakan: UITextView!
    @IBOutlet weak var txtObat: UITextView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var imgCalendar: UIImageView!
    var purpose: String?
    var idsa: String?
    var insertActivityList: [NSManagedObject] = []
    var birthdayPicker = UIDatePicker()
    var toolBar = UIToolbar()

    override func viewDidLoad() {
        super.viewDidLoad()
        if self.navigationController == nil {
            return
        }
        setEnableTapGestureOnMainView(isEnable: false)
        setRoundedViewWithBorder(view: dateView, borderColor: Constant.sharedIns.color_grey_300, bgColor: Constant.sharedIns.color_white)
        setRoundedViewWithBorder(view: txtKlinik, borderColor: Constant.sharedIns.color_grey_300, bgColor: Constant.sharedIns.color_white)
        setRoundedViewWithBorder(view: txtDoctorName, borderColor: Constant.sharedIns.color_grey_300, bgColor: Constant.sharedIns.color_white)
        setRoundedViewWithBorder(view: txtDiagnosaPrimer, borderColor: Constant.sharedIns.color_grey_300, bgColor: Constant.sharedIns.color_white)
        setRoundedViewWithBorder(view: txtDiagnosaSekunder, borderColor: Constant.sharedIns.color_grey_300, bgColor: Constant.sharedIns.color_white)
        setRoundedViewWithBorder(view: txtTindakan, borderColor: Constant.sharedIns.color_grey_300, bgColor: Constant.sharedIns.color_white)
        setRoundedViewWithBorder(view: txtObat, borderColor: Constant.sharedIns.color_grey_300, bgColor: Constant.sharedIns.color_white)
        if purpose == "add" {
            let btnRightTabBar: UIBarButtonItem = UIBarButtonItem(title: "Tambah", style: .done, target: self, action: #selector(self.registerAgent))
            self.navigationItem.rightBarButtonItem = btnRightTabBar
            addTapEventForView(view: imgCalendar, action: #selector(calendarClicked))
        } else if purpose == "cek" {
            view.isUserInteractionEnabled = false
            callAPIConsultationCek()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.tabBarController?.tabBar.isHidden = true
        }
    }
    
    @objc func calendarClicked () {
        birthdayPicker.isHidden = false
        toolBar.isHidden = false
        birthdayPicker.datePickerMode = UIDatePicker.Mode.date
        birthdayPicker.locale = NSLocale(localeIdentifier: "id_ID") as Locale
        birthdayPicker.addTarget(self, action: #selector(birthdayDateChanged(_:)), for: UIControl.Event.valueChanged)
        let pickerSize : CGSize = birthdayPicker.sizeThatFits(CGSize.zero)
        birthdayPicker.frame = CGRect(x: self.view.frame.width / 14, y: self.view.frame.height / 4, width: self.view.frame.width * 0.85, height: pickerSize.height)
        birthdayPicker.setValue(UIColor.black, forKeyPath: "textColor")
        birthdayPicker.backgroundColor = UIColor.gray
        toolBar.sizeToFit()
        toolBar.backgroundColor = UIColor(string: Constant.sharedIns.color_blue)
        let doneButton = UIBarButtonItem(title: "Tutup", style: UIBarButtonItem.Style.plain, target: self, action: #selector(donePicker))
        toolBar.setItems([doneButton], animated: false)
        toolBar.frame = CGRect(x: self.view.frame.width / 14, y: self.view.frame.height / 4 + pickerSize.height, width: self.view.frame.width * 0.85, height: toolBar.frame.height)
        self.view.addSubview(birthdayPicker)
        self.view.addSubview(toolBar)
    }
    
    @objc func birthdayDateChanged(_ sender:UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMMM yyyy"
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.timeStyle = DateFormatter.Style.none
        dateFormatter.locale = Locale(identifier: "id_ID")
        self.lblDate.text = dateFormatter.string(from: sender.date)
    }
    
    @objc func donePicker(){
        birthdayPicker.isHidden = true
        toolBar.isHidden = true
    }
    
    @objc func registerAgent() {
        let alert = UIAlertController(title: "Tambah Konsultasi",
                                      message: "Anda yakin menambahkan?",
                                      preferredStyle: .alert)
        
        let createNewAction = UIAlertAction(title: "Batal",
                                            style: .default) {
                                                [unowned self] action in
                                                self.cancelRegister()
        }
        
        let openContactAction = UIAlertAction(title: "Tambah",
                                              style: .default) {
                                                [unowned self] action in
                                                self.yesRegister()
        }
        
        alert.addAction(createNewAction)
        alert.addAction(openContactAction)
        
        self.present(alert, animated: true) {
            alert.view.superview?.isUserInteractionEnabled = true
            alert.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
        }
    }
    
    func cancelRegister() {
        popViewController()
    }
    
    func yesRegister() {
        if (lblDate.text?.isReallyEmpty == true) {
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Tanggal harus dipilih", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
        } else if (txtKlinik.text?.isReallyEmpty == true) {
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Klinik harus diisi", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
        } else if (txtDoctorName.text?.isReallyEmpty == true) {
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Nama Dokter harus diisi", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
        } else if (txtDiagnosaPrimer.text?.isReallyEmpty == true) {
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Diagnosa Primer harus diisi", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
        } else if (txtDiagnosaSekunder.text?.isReallyEmpty == true) {
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Diagnosa Sekunder harus diisi", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
        } else if (txtTindakan.text?.isReallyEmpty == true) {
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Tindakan harus diisi", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
        } else if (txtObat.text?.isReallyEmpty == true) {
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Obat harus diisi", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
        } else {
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            let managedContext = appDelegate.persistentContainer.viewContext
            let insertAgent = NSEntityDescription.insertNewObject(forEntityName: "Consultation", into: managedContext)
            insertAgent.setValue("\(txtDoctorName.text!)", forKey: "doctorName")
            insertAgent.setValue("\(txtKlinik.text!)", forKey: "hospital")
            insertAgent.setValue("\(Constant.getDateNow(format: "ddMMMYYYYHHmmss"))", forKey: "id")
            insertAgent.setValue(Constant.convertStringToDate(dateString: lblDate.text!), forKey: "date")
            insertAgent.setValue("\(txtDiagnosaPrimer.text!)", forKey: "primer")
            insertAgent.setValue("\(txtDiagnosaSekunder.text!)", forKey: "sekunder")
            insertAgent.setValue("\(txtTindakan.text!)", forKey: "tindakan")
            insertAgent.setValue("\(txtObat.text!)", forKey: "obat")
            do {
                try managedContext.save()
                insertActivityList.append(insertAgent)
                popViewController()
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
    }

    func callAPIConsultationCek() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequestAgent = NSFetchRequest<NSManagedObject>(entityName: "Consultation")
        let agentCodeKeyPredicate = NSPredicate(format: "id == %@", idsa!)
        fetchRequestAgent.predicate = agentCodeKeyPredicate
        do {
            let agents = try managedContext.fetch(fetchRequestAgent)
            if (agents.count == 0) {
                AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: "Tidak ada data", actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
            } else {
                //CoreData
                let agent = agents[0]
                lblDate.text = Constant.convertDateStringFormat(date: (agent.value(forKey: "date") as? Date)!, format: "d MMMM YYYY")
                txtKlinik.text = agent.value(forKey: "hospital") as? String
                txtDoctorName.text = agent.value(forKey: "doctorName") as? String
                txtDiagnosaPrimer.text = agent.value(forKey: "primer") as? String
                txtDiagnosaSekunder.text = agent.value(forKey: "sekunder") as? String
                txtTindakan.text = agent.value(forKey: "tindakan") as? String
                txtObat.text = agent.value(forKey: "obat") as? String
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
}
