//
//  Consultation+CoreDataProperties.swift
//  
//
//  Created by Oscar Perdanakusuma Adipati on 28/12/20.
//
//

import Foundation
import CoreData


extension Consultation {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Consultation> {
        return NSFetchRequest<Consultation>(entityName: "Consultation")
    }

    @NSManaged public var id: String?
    @NSManaged public var hospital: String?
    @NSManaged public var doctorName: String?
    @NSManaged public var date: Date?
    @NSManaged public var primer: String?
    @NSManaged public var sekunder: String?
    @NSManaged public var tindakan: String?
    @NSManaged public var obat: String?

}
