//
//  HTTPBody.swift
//  homecredit
//
//  Created by Oscar Perdanakusuma - Private on 8/16/17.
//  Copyright © 2017 Home Credit Indonesia. All rights reserved.
//

import Alamofire
import Firebase

class HTTPBody{
    
    static let sharedIns = HTTPBody()
    
    let user_id = "userId"
    let email = "email"
    let phone = "phone"
    let pin = "pin"
    let loginType = "loginType"
    let deviceId = "deviceId"
    let dateOfBirth = "dateOfBirth"
    let deviceInfo = "device"
    let OS_TYPE = "IOS"
    
//    class func loginBody(phone: String, email : String, pin : String, loginType : String) -> Parameters{
//        var deviceToken = ""
//        if let firebaseToken = InstanceID.instanceID().token(){
//            deviceToken = firebaseToken
//        }
//        print("Firebase registration token :\(deviceToken)")
//
//        let login : Parameters = [
//            sharedIns.user_id: phone,
//            sharedIns.email: email,
//            sharedIns.phone : phone,
//            sharedIns.pin: pin,
//            sharedIns.loginType: loginType,
//            sharedIns.deviceId: deviceToken,
//            sharedIns.deviceInfo: CommonUtils.getDeviceInfo(),
//            "osType": sharedIns.OS_TYPE
//        ]
//
//
//        return login
//    }
    
//    class func registerFormBody(phone: String, DOB: String, email : String, pin : String, loginType : String) -> Parameters{
//        var deviceToken = ""
//        if let firebaseToken = InstanceID.instanceID().token(){
//            deviceToken = firebaseToken
//        }
//        print("Firebase registration token :\(deviceToken)")
//
//        let registerForm : Parameters = [
//            sharedIns.user_id: phone,
//            sharedIns.email: email,
//            sharedIns.dateOfBirth : DOB,
//            sharedIns.phone : phone,
//            sharedIns.pin: pin,
//            sharedIns.loginType: loginType,
//            sharedIns.deviceId: deviceToken,
//        ]
//
//        return registerForm
//    }
    
    class func loginGuestBody(guestId : String)->Parameters{
        let guest : Parameters = [
            sharedIns.user_id: guestId,
            sharedIns.deviceInfo: CommonUtils.getDeviceInfo()
        ]
        
        return guest
    }
    
//    class func registerOTPVerification(otpId: String, otpCode : String, email : String, phone : String, pin : String, dateOfBirth : String) -> Parameters{
//        var deviceToken = ""
//        if let firebaseToken = InstanceID.instanceID().token(){
//            deviceToken = firebaseToken
//        }
//        print("Firebase registration token :\(deviceToken)")
//
//        let verifyotpregister : Parameters = [
//            "otpId": otpId,
//            "otpCode": otpCode,
//            "email" : email,
//            "userId": phone,
//            "phone": phone,
//            "pin": pin,
//            "loginType": "MANUAL",
//            "dateOfBirth": dateOfBirth,
//            sharedIns.deviceId: deviceToken,
//            sharedIns.deviceInfo: CommonUtils.getDeviceInfo(),
//            "osType": sharedIns.OS_TYPE
//        ]
//
//        return verifyotpregister
//    }
    
    class func verifyotpregisterBody(otpId: String, otpCode : String, email : String, phone : String, pin : String, dateOfBirth : String, deviceId : String, imei : String, osVersion : String, model : String, manufacturer : String, serialNumber : String, career : String, ram : String, totalDiskMemory : String, availableDiskMemory : String) -> Parameters{
        let verifyotpregister : Parameters = [
            "otpId": otpId,
            "otpCode": otpCode,
            "email" : email,
            "userId": phone,
            "phone": phone,
            "pin": pin,
            "loginType": "MANUAL",
            "dateOfBirth": dateOfBirth,
            "deviceId": deviceId,
            sharedIns.deviceInfo: CommonUtils.getDeviceInfo()
        ]
        
        return verifyotpregister
    }
    
    class func forgotPINformBody(phone: String, dateOfBirth : String) -> Parameters{
        let forgotpin : Parameters = [
            sharedIns.user_id: phone,
            sharedIns.phone: phone,
            sharedIns.dateOfBirth: dateOfBirth
        ]
        
        return forgotpin
    }
    
    class func verifyResetPINBody(userId: String, otpId : String, otpCode : String, pin : String) -> Parameters{
        let sendverifyforgotpin : Parameters = [
            sharedIns.user_id: userId,
            sharedIns.phone : userId,
            "otpId": otpId,
            "otpCode": otpCode,
            "pin": pin
        ]
        
        return sendverifyforgotpin
    }
    
    class func resendOTP(phone: String) -> Parameters{
        let otpregister : Parameters = [
            "phone": phone,
            "key": "secret"
        ]
        
        return otpregister
    }
    
    class func otpregisterBody(phone: String) -> Parameters{
        let otpregister : Parameters = [
            "phone": phone,
            "key": "secret"
        ]
        
        return otpregister
    }
    
    class func sentContactBody(agentCode: String, source_id: String, dateOfBirth: String, ageBand: String, exactIncome: String, exactOccupation: String, fatherName: String, gender: String, homeAddress: String, howCloseAreYou: String, howInfluenceThisPerson: String, income: String, knowThisPerson: String, locationAccessibility: String, maritalStatus: String, motherName: String, name: String, occupation: String, prospectingStatus: String, sourceOfProspect: String, spouseName: String, totalChildren: String, workAddress: String, exactSourceOfProspect: String, idCardNumber: String) -> Parameters{
        let sentContact : Parameters = [
            "agentCode": agentCode,
            "source_id": source_id,
            "dateOfBirth": dateOfBirth,
            "ageBand": ageBand,
            "exactIncome": exactIncome,
            "exactOccupation": exactOccupation,
            "fatherName": fatherName,
            "gender": gender,
            "homeAddress": homeAddress,
            "howCloseAreYou": howCloseAreYou,
            "howInfluenceThisPerson": howInfluenceThisPerson,
            "income": income,
            "knowThisPerson": knowThisPerson,
            "locationAccessibility": locationAccessibility,
            "maritalStatus": maritalStatus,
            "motherName": motherName,
            "name": name,
            "occupation": occupation,
            "prospectingStatus": prospectingStatus,
            "sourceOfProspect": sourceOfProspect,
            "spouseName": spouseName,
            "totalChildren": totalChildren,
            "workAddress": workAddress,
            "exactSourceOfProspect": exactSourceOfProspect,
            "idCardNumber": idCardNumber
        ]
        
        return sentContact
    }
    
    class func sentActivityBody(activityiId: String, activity: String, activityResults: String, caseId: String, caseAgentId: String, date: String, fromTime: String, nextStep: String, place: String, reminder: String, apeSPAJ: String, response: String, status: String, toTime: String, responseNotes: String, nextStepNotes: String) -> Parameters{
        let sentActivity : Parameters = [
            "activityiId": activityiId,
            "activity": activity,
            "activityResults": activityResults,
            "caseId": caseId,
            "caseAgentId": caseAgentId,
            "date": date,
            "fromTime": fromTime,
            "nextStep": nextStep,
            "place": place,
            "reminder": reminder,
            "apeSPAJ": apeSPAJ,
            "response": response,
            "status": status,
            "toTime": toTime,
            "responseNotes": responseNotes,
            "nextStepNotes": nextStepNotes
        ]
        
        return sentActivity
    }
    
    class func sentCaseBody(caseAgentId: String, source_id: String, userId: String, caseStatus: Int, purpose: String, predicateValue: String, notes: String) -> Parameters{
        let sentCase : Parameters = [
            "caseAgentId": caseAgentId,
            "source_id": source_id,
            "userId": userId,
            "caseStatus": caseStatus,
            "purpose": purpose,
            "predicateValue": predicateValue,
            "notes": notes
        ]
        
        return sentCase
    }
    
    class func sentChildrenBody(agentCode: String, childrenId: String, userId: String, childrenStatus: String, childrenName: String) -> Parameters{
        let sentChildren : Parameters = [
            "agentCode": agentCode,
            "childrenId": childrenId,
            "userId": userId,
            "childrenStatus": childrenStatus,
            "childrenName": childrenName
        ]
        
        return sentChildren
    }
    
    class func sentBrotherBody(agentCode: String, brotherId: String, userId: String, brotherAgeBand: String, brotherName: String) -> Parameters{
        let sentBrother : Parameters = [
            "agentCode": agentCode,
            "brotherId": brotherId,
            "userId": userId,
            "brotherAgeBand": brotherAgeBand,
            "brotherName": brotherName
        ]
        
        return sentBrother
    }
    
    class func sentSisterBody(agentCode: String, userId: String, sisterId: String, sisterAgeBand: String, sisterName: String) -> Parameters{
        let sentSister : Parameters = [
            "agentCode": agentCode,
            "userId": userId,
            "sisterId": sisterId,
            "sisterAgeBand": sisterAgeBand,
            "sisterName": sisterName
        ]
        
        return sentSister
    }
    
    class func sentPhoneBody(agentCode: String, userId: String, phoneNumber: String, phoneId: String, phoneName: String) -> Parameters{
        let sentPhone : Parameters = [
            "agentCode": agentCode,
            "userId": userId,
            "phoneNumber": phoneNumber,
            "phoneId": phoneId,
            "phoneName": phoneName
        ]
        
        return sentPhone
    }
    
    class func sentEmailBody(agentCode: String, userId: String, emailAddress: String, emailId: String, emailName: String) -> Parameters{
        let sentEmail : Parameters = [
            "agentCode": agentCode,
            "userId": userId,
            "emailAddress": emailAddress,
            "emailId": emailId,
            "emailName": emailName
        ]
        
        return sentEmail
    }
    
    class func sentMedicalExpensesBody(agentCode: String, medicalExpenseId: String, userId: String, priorityName: String, questionStatus: Int, notes: String) -> Parameters{
        let sentMedicalExpenses : Parameters = [
            "agentCode": agentCode,
            "medicalExpenseId": medicalExpenseId,
            "userId": userId,
            "priorityName": priorityName,
            "questionStatus": questionStatus,
            "notes": notes
        ]
        
        return sentMedicalExpenses
    }
    
    class func sentOldAgeFundsBody(agentCode: String, oldAgeFundsId: String, userId: String, priorityName: String, questionStatus: Int, notes: String) -> Parameters{
        let sentOldAgeFunds : Parameters = [
            "agentCode": agentCode,
            "oldAgeFundsId": oldAgeFundsId,
            "userId": userId,
            "priorityName": priorityName,
            "questionStatus": questionStatus,
            "notes": notes
        ]
        
        return sentOldAgeFunds
    }
    
    class func sentNecessityIncomeBody(agentCode: String, necessityIncomeId: String, userId: String, priorityName: String, questionStatus: Int, notes: String) -> Parameters{
        let sentNecessityIncome : Parameters = [
            "agentCode": agentCode,
            "necessityIncomeId": necessityIncomeId,
            "userId": userId,
            "priorityName": priorityName,
            "questionStatus": questionStatus,
            "notes": notes
        ]
        
        return sentNecessityIncome
    }
    
    class func sentEducationFundBody(agentCode: String, educationFundId: String, userId: String, priorityName: String, questionStatus: Int, notes: String) -> Parameters{
        let sentEducationFund : Parameters = [
            "agentCode": agentCode,
            "educationFundId": educationFundId,
            "userId": userId,
            "priorityName": priorityName,
            "questionStatus": questionStatus,
            "notes": notes
        ]
        
        return sentEducationFund
    }
    
    class func sentDebtsBody(agentCode: String, debtsciId: String, userId: String, priorityName: String, questionStatus: Int, notes: String) -> Parameters{
        let sentDebts : Parameters = [
            "agentCode": agentCode,
            "debtsciId": debtsciId,
            "userId": userId,
            "priorityName": priorityName,
            "questionStatus": questionStatus,
            "notes": notes
        ]
        
        return sentDebts
    }
    
    class func sentCIBody(agentCode: String, ciId: String, userId: String, priorityName: String, questionStatus: Int, notes: String) -> Parameters{
        let sentCI : Parameters = [
            "agentCode": agentCode,
            "ciId": ciId,
            "userId": userId,
            "priorityName": priorityName,
            "questionStatus": questionStatus,
            "notes": notes
        ]
        
        return sentCI
    }
    
    class func placeBody(placeIt: String, placeType: String, placeTitle: String, placeLatitude: Double, placeLongitude: Double) -> Parameters{
        let placeit : Parameters = [
            "placeIt": placeIt,
            "placeType": placeType,
            "placeTitle": placeTitle,
            "placeLatitude": placeLatitude,
            "placeLongitude": placeLongitude
        ]
        
        return placeit
    }
    
    class func filterPlaceBody(filterName: String) -> Parameters{
        let filtername : Parameters = [
            "filterName": filterName
        ]
        
        return filtername
    }
    
    class func loginBody(username: String, password: String) -> Parameters{
        let login : Parameters = [
            "username": username,
            "password": password
        ]
        
        return login
    }
    
    class func registerBody(username: String, password: String, phone: String) -> Parameters{
        let register : Parameters = [
            "username": username,
            "password": password,
            "phone": phone
        ]
        
        return register
    }
    
    class func productListBody(categoryid: String) -> Parameters{
        let productlist : Parameters = [
            "categoryid": categoryid
        ]
        
        return productlist
    }
    
    class func productBody(categoryid: String, productid: String) -> Parameters{
        let product : Parameters = [
            "categoryid": categoryid,
            "productid": productid
        ]
        
        return product
    }
    
    class func promoBody(date: String) -> Parameters{
        let promo : Parameters = [
            "date": date
        ]
        
        return promo
    }
    
    class func productSalesBody(date: String, clientid: String) -> Parameters{
        let productSales : Parameters = [
            "date": date,
            "clientid": clientid
        ]
        
        return productSales
    }
    
    class func cartBody(date: String, clientid: String, categoryid: String, productid: String, salesquantity: String, paymentchoise: String, takeorsend : String, address: String) -> Parameters{
        let cart : Parameters = [
            "date": date,
            "clientid": clientid,
            "categoryid": categoryid,
            "productid": productid,
            "salesquantity": salesquantity,
            "paymentchoise": paymentchoise,
            "takeorsend": takeorsend,
            "address": address
        ]
        
        return cart
    }
    
    class func creditcardBody(sales_id: Int, card_bank: String, card_name: String, card_number: String, card_cvc: String, card_month: Int, card_year : Int) -> Parameters{
        let creditcard : Parameters = [
            "sales_id": sales_id,
            "card_bank": card_bank,
            "card_name": card_name,
            "card_number": card_number,
            "card_cvc": card_cvc,
            "card_month": card_month,
            "card_year": card_year
        ]
        
        return creditcard
    }
    
    class func updateAccountBody(username: String, password: String, phone: String) -> Parameters{
        let updateAccount : Parameters = [
            "username": username,
            "password": password,
            "phone": phone
        ]
        
        return updateAccount
    }
    
    class func refreshTokenBody(token: String) -> Parameters{
        let refreshtoken : Parameters = [
            "token": token
        ]

        return refreshtoken
    }
    
    class func refreshTokenBody2(username: String, password: String) -> Parameters{
        let refreshtoken : Parameters = [
            "username": username,
            "password": password
        ]
        
        return refreshtoken
    }
    
    class func forgotPasswordBody(username: String) -> Parameters{
        let forgotpassword : Parameters = [
            "username": username
        ]
        
        return forgotpassword
    }
    
    class func agentProfileBody(username: String) -> Parameters{
        let agentprofile : Parameters = [
            "username": username
        ]
        
        return agentprofile
    }
    
    class func moneyReportBody(contactId: String) -> Parameters{
        let moneyreport : Parameters = [
            "contact_id": contactId
        ]
        
        return moneyreport
    }
    
    class func agentSPAJListBody(agentStatus: String, agentLevel: String, agentName: String, agentCode: String, spajNo: String, policyHolderName: String, lifeAssured: String, spajStatus: String, page: Int, offset: Int, branch: String, spajStatusStartDate: String, spajStatusEndDate: String) -> Parameters{
        let agentspajlist : Parameters = [
            "agent_status": agentStatus,
            "agent_level": agentLevel,
            "agent_name": agentName,
            "agent_code": agentCode,
            "spaj_no": spajNo,
            "policy_holder_name": policyHolderName,
            "life_assured": lifeAssured,
            "spaj_status": spajStatus,
            "page": page,
            "offset": offset,
            "branch": branch,
            "spaj_status_start_date": spajStatusStartDate,
            "spaj_status_end_date": spajStatusEndDate
        ]
        
        return agentspajlist
    }
    
    class func agentSPAJInfoBody(agentStatus: String, agentLevel: String, agentName: String, agentCode: String, spajNo: String, policyHolderName: String, lifeAssured: String, spajStatus: String, page: Int, offset: Int, branch: String, spajStatusStartDate: String, spajStatusEndDate: String, spajId: String) -> Parameters{
        let agentspajlist : Parameters = [
            "agent_status": agentStatus,
            "agent_level": agentLevel,
            "agent_name": agentName,
            "agent_code": agentCode,
            "spaj_no": spajNo,
            "policy_holder_name": policyHolderName,
            "life_assured": lifeAssured,
            "spaj_status": spajStatus,
            "page": page,
            "offset": offset,
            "branch": branch,
            "spaj_status_start_date": spajStatusStartDate,
            "spaj_status_end_date": spajStatusEndDate,
            "spaj_id": spajId
        ]
        
        return agentspajlist
    }
    
    class func freePABody(agentCode: String, name : String, gender : String, phone : String, dateOfBirth : String, email : String) -> Parameters{
        let freePA : Parameters = [
            "agent_code": agentCode,
            "name": name,
            "gender" : gender,
            "phone_number": phone,
            "date_of_birth": dateOfBirth,
            "email_address": email
        ]
        
        return freePA
    }
    
    class func homepageBody(versionCode: String) -> Parameters{
        let homepage : Parameters = [
            "versionCode": versionCode,
            "osType" : sharedIns.OS_TYPE
        ]
        
        return homepage
    }
    
    class func pairingContractBody(contractNumber: String) -> Parameters{
        let contract : Parameters = [
            "contractNumber": contractNumber
        ]
        
        return contract
    }
    
    class func contactmeBody(date: String, email: String, phone : String, notes : String, topic : String) -> Parameters{
        let contactme : Parameters = [
            "date": date,
            "email": email,
            "phone": phone,
            "notes" : notes,
            "topic" : topic
        ]
        
        return contactme
    }
    
    class func inboxBody(from:Int, to:Int, pointer:CLongLong) -> Parameters{
        if from > 0 {
            let inbox : Parameters = [
                "from": from,
                "to": to,
                "pointer" : pointer
            ]
            
            return inbox
        }else{
            let inbox : Parameters = [
                "from": from,
                "to": to
            ]
            
            return inbox
        }
    }
    
    class func switchMessageSettingBody(typeId: String, typeStatus: Bool) -> Parameters{
        let setting : Parameters = [
            "typeId": typeId,
            "typeStatus": typeStatus
        ]
        
        return setting
    }
    
    class func readDeleteInboxBody(eidList: [String], pointer : CLongLong) -> Parameters{
        if pointer > 0{
            let data : Parameters = [
                "inboxs": eidList,
                "pointer": pointer
            ]
            
            return data
        }else{
            let data : Parameters = [
                "inboxs": eidList
            ]
            
            return data
        }
        
    }
    
    class func calculatorflexifastBody(creditAmount: String) -> Parameters{
        let calculatorflexifast : Parameters = [
            "creditAmount": creditAmount
        ]
        
        return calculatorflexifast
    }
    
    class func posLocationBody(city: String, latitude: Double, longitude: Double, commodityType: String) -> Parameters{
        let param : Parameters = [
            "city": city,
            "latitude": latitude,
            "longitude": longitude,
            "commodity": commodityType
        ]
        
        return param
    }

    class func changepinBody(oldPin: String, newPin: String) -> Parameters{
        let changepin : Parameters = [
            "oldPin": oldPin,
            "newPin": newPin
        ]
        
        return changepin
    }
    
    class func paymentscheduleBody(contractNumber: String) -> Parameters{
        let paymentschedule : Parameters = [
            "contractNumber": contractNumber
        ]
        
        return paymentschedule
    }
    
    class func myprofileBody(serialNumber: String) -> Parameters{
        let myprofile : Parameters = [
            "device": [
                "serialNumber": serialNumber
            ]
        ]
        return myprofile
    }
    
    class func callMeFlexiFastBody(campaignId: String, email: String, phone: String, date: String, hourFrom: String, hourTo: String, installmentAmount: Double, creditAmount: Double, term: String, isFromOriginalOffer: Bool, isImmediate: Bool) -> Parameters{
        
        var requestedFrom = ""
        if isFromOriginalOffer{
            requestedFrom = "original offer"
        }else{
            requestedFrom = "flexifast simulation"
        }
        
        let submitCallMeFF : Parameters = [
            "creditAmount": creditAmount,
            "term": term,
            "installmentAmount": installmentAmount,
            "campaignId": campaignId,
            "date": date,
            "email": email,
            "phone": phone,
            "requestedFrom": requestedFrom,
            "hourFrom": hourFrom,
            "hourTo": hourTo,
            "isImmediate" : isImmediate
        ]
        
        return submitCallMeFF
    }
    
    class func generalcalculatorBody(creditAmount: Int, commodityType: String, commodityCategory: String, downPayment: Int, term: Int, withAman: Bool, withAdId: Bool) -> Parameters{
        let generalcalculator : Parameters = [
            "creditAmount": creditAmount,
            "commodityType": commodityType,
            "commodityCategory": commodityCategory,
            "downPayment": downPayment,
            "term": term,
            "withAman": withAman,
            "withAdId": withAdId
        ]
        
        return generalcalculator
    }
    
    class func generalSimulationBody(creditAmount: Double, commodityType: String, commodityCategory: String, downPayment: Int, withAMAN: Bool, withADLD: Bool) -> Parameters{
        let generalcalculator : Parameters = [
            "creditAmount": creditAmount,
            "commodityType": commodityType,
            "commodityCategory": commodityCategory,
            "downPayment": downPayment,
            "withAman": withAMAN,
            "withAdld": withADLD
        ]
        
        return generalcalculator
    }
    
    class func flexiFastSimulationBody(creditAmount: Double) -> Parameters{
        let calc : Parameters = [
            "creditAmount": creditAmount
        ]
        
        return calc
    }
    
//    class func newToken() -> Parameters{
//        /// Comment for release 1
//        var deviceToken = ""
//        if let firebaseToken = InstanceID.instanceID().token(){
//            deviceToken = firebaseToken
//        }
//        print("Firebase registration token :\(deviceToken)")
//        
//        let newToken : Parameters = [
//            "key": "secret",
//            sharedIns.deviceId: deviceToken,
//            "osType": sharedIns.OS_TYPE
//        ]
//        
//        return newToken
//    }
//    
//    class func updateStatusPushNotif(messageId: String, statusType: String) -> Parameters{
//        var deviceToken = ""
//        if let firebaseToken = InstanceID.instanceID().token(){
//            deviceToken = firebaseToken
//        }
//        print("Firebase registration token :\(deviceToken)")
//        
//        let data : Parameters = [
//            "messageId" : messageId,
//            "statusType" : statusType,
//            sharedIns.deviceId: deviceToken
//        ]
//        
//        return data
//    }
    
    class func sendFCMTokenBody(fcmToken: String) -> Parameters{
        let data : Parameters = [
            "osType" : sharedIns.OS_TYPE,
            sharedIns.deviceId: fcmToken
        ]
        
        return data
    }
    
    class func uploadImageBase64(base64: String) -> Parameters {
        let data: Parameters = [
            "file" : base64
        ]
        
        return data
    }

}
