//
//  ConnectionManager.swift
//  homecredit
//
//  Created by Oscar Perdanakusuma on 8/15/17.
//  Copyright © 2017 Home Credit Indonesia. All rights reserved.
//

import AlamofireObjectMapper
import Alamofire
//import Firebase

protocol MainCallBack{
    func onSuccessResponse<T>(message: String, object: AnyObject, response: DataResponse<T>)
    func onFailedResponse<T>(message: String, object: AnyObject, response: DataResponse<T>)
    func onFailureResponse(message: String)
}

protocol CommonAPICallback{
    @available(*, deprecated, message : "use protocol RecallAPICallback instead")
    func onRecallAPI()
}

protocol RecallAPICallback{
   func onRecallAPI<T>(object: AnyObject, response: DataResponse<T>)
}

protocol HomeScreenAPICallback{
    func onCriticalUpdate<T>(message: String, object: AnyObject, response: DataResponse<T>)
    func onMaintenance<T>(message: String, object: AnyObject, response: DataResponse<T>)
}

protocol LoginRegisterAPICallback{
    func onAccountExist<T>(message: String, object: AnyObject, response: DataResponse<T>)
}

class ConnectionManager{

    static let sharedIns = ConnectionManager()
    static var alfireSession : SessionManager!
    
    public enum ResponseEnum : String{
        case success = "SUCCESS"
        case failed = "FAILED"
        case failure = "FAILURE"
        case maintenance = "MAINTENANCE"
        case critical_update = "CRITICAL_UPDATE"
        case account_exist = "ACCOUNT_EXIST"
        case recall_api = "RECALL_API"
    }
    
    var mainCallback : MainCallBack?
    var commonCallback : CommonAPICallback?
    var homeCallback : HomeScreenAPICallback?
    var loginRegisCallback : LoginRegisterAPICallback?
    var recallAPICallback : RecallAPICallback?
    var refreshTokenModel : RefreshTokenModel?
    
    /**
     ** SSL Pinning
     **/
    static func configureAlamofireManager()->Void{
        // Your hostname and endpoint
        let hostname = uriBase_api
        //let endpoint = "YOUR_ENDPOINT"
        let cert = "*.homecredit.co.id" // e.g. for cert.der, this should just be "cert"
        
        // Set up certificates
        let pathToCert = Bundle.main.path(forResource: cert, ofType: "cer")
        
        //print("pathtocert:\(pathToCert!)")
        
        let localCertificate = NSData(contentsOfFile: pathToCert!)
        let certificates = [SecCertificateCreateWithData(nil, localCertificate!)!]
        
        // Configure the trust policy manager
        let serverTrustPolicy = ServerTrustPolicy.pinCertificates(
            certificates: certificates,
            validateCertificateChain: true,
            validateHost: true
        )
        
        let serverTrustPolicies = [hostname: serverTrustPolicy]
        let serverTrustPolicyManager = ServerTrustPolicyManager(policies: serverTrustPolicies)
        
        // Configure session manager with trust policy
        alfireSession = SessionManager(
            configuration: URLSessionConfiguration.default,
            serverTrustPolicyManager: serverTrustPolicyManager
        )
    }
    
    static func cancelAllRequest()->Void{
        if(alfireSession != nil){
            alfireSession.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
                sessionDataTask.forEach { $0.cancel() }
    //            sessionDataTask.forEach({task in
    //                if task.currentRequest?.url?.absoluteString == Constant.getAPIhome(){
    //                    task.cancel()
    //                }
    //            })
                uploadData.forEach { $0.cancel() }
                downloadData.forEach { $0.cancel() }
            }
        }
    }

    
    func getHTTPHeaders() -> HTTPHeaders{
        var accessToken : String = ""
        let bearer = "JWT "
        
        accessToken = "\(bearer) \(CustomUserDefaults.shared.getAccessToken())"
        
        print("ACCESS_TOKEN_HEADER:\(accessToken)")
        
        let headers: HTTPHeaders = [
            "Authorization": accessToken,
            "Content-Type": "application/json"
        ]
        
        return headers
    }
    
    func getHTTPHeadersRefreshToken() -> HTTPHeaders{
        var refreshToken : String = ""
        let bearer = "Bearer "
        
        let currentDate = "\(Date())"
        let tokenExpireDate = CustomUserDefaults.shared.getTokenExpireTime()
        if (currentDate == tokenExpireDate) {
            callAPIRefreshToken()
        }
        refreshToken = bearer + CustomUserDefaults.shared.getRefreshToken()
        
        print("ACCESS_TOKEN_HEADER:\(refreshToken)")
        
        let headers: HTTPHeaders = [
            "Authorization": refreshToken,
            "Content-Type": "application/json"
        ]
        
        return headers
    }
    
    func getHTTPHeadersSAMDB() -> HTTPHeaders{
    
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        
        return headers
    }
    
    func connectionCallback<T>(object: AnyObject, responseData: DataResponse<T>)->Void{
        
        let statusResponse = responseData.result
        var message : String?
        let status : String?
        
        if(statusResponse.isSuccess){
            
            //Get Server Date
            let date = responseData.response!.allHeaderFields["Date"] as! String
            print("SERVER_DATE:\(String(describing: date))")
            
            DispatchQueue.main.async {
                let date1 = Constant.sharedIns.setStringToServerDate(dateString: date)
                let date2 = Constant.convertDateStringFormat(date: date1, format: "dd/MM/yyyy")
                //print("SERVER_DATE:\(String(describing: date2))")
                
                CustomUserDefaults.shared.setServerDate(date2)
                print("SERVER_DATE:\(CustomUserDefaults.shared.getServerDate())")
            }
            
            let mainResponse = responseData.result.value as! MainResponse
            status = mainResponse.status
            
            print("API_STATUS:\(String(describing: status))")
            
            if(status == ResponseEnum.success.rawValue){
                print("SUCCESS_CALLBACK")
                
                if(object === LoginRegisModel.self){
                    let loginRegisModel = responseData.result.value as! LoginRegisModel
                    let userId = loginRegisModel.user_id
                    let token = loginRegisModel.token
                    let role = loginRegisModel.role
                    let firstTime = loginRegisModel.first_time
                    
                    print("userId:\(userId!)")
                    print("token:\(token!)")
                    print("role:\(role!)")
                    print("firstTime:\(firstTime?.description ?? "none")")
                    
                    CustomUserDefaults.shared.setAccessToken(token!)
                    CustomUserDefaults.shared.setDummyAccount(firstTime!)
                }
                
                
                mainCallback?.onSuccessResponse(message: message!, object: object, response: responseData)
                
            }else if(status == ResponseEnum.failed.rawValue){
                mainCallback?.onFailedResponse(message: message!, object: object, response: responseData)
                
            }else if(status == ResponseEnum.maintenance.rawValue){
                homeCallback?.onMaintenance(message: message!, object: object, response: responseData)
                
            }else if(status == ResponseEnum.critical_update.rawValue){
                homeCallback?.onCriticalUpdate(message: message!, object: object, response: responseData)
                
            }else if(status == ResponseEnum.account_exist.rawValue){
                loginRegisCallback?.onAccountExist(message: message!, object: object, response: responseData)
            }
            
        }else if(statusResponse.isFailure){
            if let httpStatusCode = responseData.response?.statusCode {
                if(httpStatusCode == 401){
                    let msg = "Unauthorized"
                    //Call API get new token
//                    getNewToken(completion: { (status, message) in
//                        if(status == ResponseEnum.success){
//                            self.commonCallback?.onRecallAPI()
//                            self.recallAPICallback?.onRecallAPI(object: object, response: responseData)
//
//                        }else{
//                            self.mainCallback?.onFailureResponse(message: message)
//                        }
//                    })
                    
                    print("API_MESSAGE:\(String(describing: msg))")
                    
                    return
                    
                }else if(httpStatusCode == 500){
                    message = Wordings.msg_server_error
                    
                }else if(httpStatusCode == 504){
                    message = Wordings.msg_timeout_error
                    
                }else{
                    print("API_MESSAGE: \(httpStatusCode)")
                    message = Wordings.msg_maintenance
                }
                
                mainCallback?.onFailureResponse(message: message!)
                
            } else {
                print("NO_RESPONSE: \(String(describing: statusResponse.error))")
                let errorResponse = String(describing: statusResponse.error)
                
                if(errorResponse.contains("cancelled")){
                    print("RESPONSE_CANCELLED")
                    message = ""
                }else if let err = statusResponse.error as? URLError, err.code == .notConnectedToInternet{
                    message = Wordings.msg_internet_error
                }else{
                    message = Wordings.msg_connection_error
                }
                
                mainCallback?.onFailureResponse(message: message!)
                print("API_MESSAGE: \(String(describing: message))")
            }
            
        }else{
            message = Wordings.msg_connection_error
            mainCallback?.onFailureResponse(message: message!)
            print("API_MESSAGE: \(String(describing: message))")
        }
        
    }
    
    /// Use this callback if you need completion
    //@available(*, deprecated, message : "use connectionCallback without completion instead")
    func connectionCallback<T>(responseData: DataResponse<T>, completion : @escaping (_ status : ResponseEnum, _ message : String)->Void){
        
        let statusResponse = responseData.result
        
        let status : String?
        var message : String?
        
        if(statusResponse.isSuccess){

            //Get Server Date
            let date = responseData.response!.allHeaderFields["Date"] as! String
            //print("SERVER_DATE:\(String(describing: date))")
            
            DispatchQueue.main.async {
                let date1 = Constant.sharedIns.setStringToServerDate(dateString: date)
                let date2 = Constant.convertDateStringFormat(date: date1, format: "dd/MM/yyyy")
                //print("SERVER_DATE:\(String(describing: date2))")
                
                CustomUserDefaults.shared.setServerDate(date2)
                print("SERVER_DATE:\(CustomUserDefaults.shared.getServerDate())")
            }
            
            let mainResponse = responseData.result.value as! MainResponse
            status = mainResponse.status
            
            print("API_STATUS:\(String(describing: status))")
            
            if(status == ResponseEnum.success.rawValue){
                completion(ResponseEnum.success, message!)
                
            }else if(status == ResponseEnum.failed.rawValue){
                completion(ResponseEnum.failed, message!)
                
            }else if(status == ResponseEnum.maintenance.rawValue){
                completion(ResponseEnum.maintenance, message!)
                
            }else if(status == ResponseEnum.critical_update.rawValue){
                completion(ResponseEnum.critical_update, message!)
                
            }else if(status == ResponseEnum.account_exist.rawValue){
                completion(ResponseEnum.account_exist, message!)
            }
            
        }else if(statusResponse.isFailure){
            if let httpStatusCode = responseData.response?.statusCode {
                
                if(httpStatusCode == 401){
                    let msg = "Unauthorized"
                    //Call API get new token
//                    getNewToken(completion: { (status, message) in
//                        if(status == ResponseEnum.success){
//                            completion(ResponseEnum.recall_api, message)
//
//                        }else{
//                            completion(ResponseEnum.failure, message)
//
//                        }
//                    })
                    
                    print("API_MESSAGE:\(String(describing: msg))")
                    
                    return
                    
                }else if(httpStatusCode == 500){
                    message = Wordings.msg_server_error
                    
                }else if(httpStatusCode == 504){
                    message = Wordings.msg_timeout_error
                    
                }else{
                    print("API_MESSAGE: \(httpStatusCode)")
                    message = Wordings.msg_maintenance
                }
                
                completion(ResponseEnum.failure, message!)
                
            } else {
                print("NO_RESPONSE: \(String(describing: statusResponse.error))")
                let errorResponse = String(describing: statusResponse.error)
                
                if(errorResponse.contains("cancelled")){
                    print("RESPONSE_CANCELLED")
                    message = ""
                }else if let err = statusResponse.error as? URLError, err.code == .notConnectedToInternet{
                    message = Wordings.msg_internet_error
                }else{
                    message = Wordings.msg_connection_error
                }
                
                completion(ResponseEnum.failure, message!)
                print("API_MESSAGE: \(String(describing: message))")
            }
            
        }else{
            message = Wordings.msg_connection_error
            completion(ResponseEnum.failure, message!)
        }
        
    }
    
    func callAPIRefreshToken() {
//        let param = HTTPBody.refreshTokenBody(token: CustomUserDefaults.shared.getRefreshToken())
//        print("param: \(String(describing: param))")
//        
//        let url = NSURL(string:Constant.getAPIRefreshToken())
//        print("LOGIN URL:\(String(describing: url!))")
//        
//        Alamofire.request(Constant.getAPIRefreshToken(), method: .post, parameters: param, encoding: JSONEncoding.default, headers: ConnectionManager.sharedIns.getHTTPHeadersRefreshToken())
//            .responseObject{ (response: DataResponse<RefreshTokenModel>) in
//                switch(response.result) {
//                case .success(_):
//                    self.refreshTokenModel = response.result.value as? RefreshTokenModel
//                    let refreshToken = self.refreshTokenModel?.access
//                    CustomUserDefaults.shared.setRefreshToken(refreshToken!)
//                    CustomUserDefaults.shared.setTokenExpireTime("\(Date().sixthDay)")
//                    break
//                    
//                case .failure(_):
//                    self.refreshTokenModel = response.result.value as? RefreshTokenModel
//                    let errorMessage = self.refreshTokenModel?.non_field_errors![0]
//                    print("error:\(String(describing: errorMessage))")
//                    AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: errorMessage!, actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
//                    break
//                    
//                }
//        }
    }
    
//    func getNewToken(completion : @escaping (_ status : ResponseEnum, _ message : String)->Void){
//
//        let param  = HTTPBody.newToken()
//        
//        Alamofire.request(Constant.getAPIrefreshToken(), method: .post, parameters: param, encoding: JSONEncoding.default, headers: getHTTPHeadersRefreshToken()).responseObject { (responseData: DataResponse<LoginRegisModel>) in
//
//            let statusResponse = responseData.result
//            
//            var message : String?
//            let status : String?
//            
//            if(statusResponse.isSuccess){
//                
//                //Get Server Date
//                let date = responseData.response?.allHeaderFields["Date"] as! String
//                //print("SERVER_DATE:\(String(describing: date))")
//                
//                DispatchQueue.main.async {
//                    let date1 = Constant.sharedIns.setStringToServerDate(dateString: date)
//                    let date2 = Constant.convertDateStringFormat(date: date1, format: "dd/MM/yyyy")
//                    //print("SERVER_DATE:\(String(describing: date2))")
//                    
//                    CustomUserDefaults.shared.setServerDate(date2)
//                    print("SERVER_DATE:\(CustomUserDefaults.shared.getServerDate())")
//                }
//                
//                let mainResponse = responseData.result.value
//                status = mainResponse?.status
//                
//                print("API_STATUS_TOKEN:\(String(describing: status))")
//                print("API_MESSAGE:\(String(describing: message))")
//                
//                if(status == ResponseEnum.success.rawValue){
//                    let loginRegisModel = responseData.result.value
//                    let userId = loginRegisModel?.user_id
//                    let token = loginRegisModel?.token
//                    let role = loginRegisModel?.role
//                    let firstTime = loginRegisModel?.first_time
//
//                    print("userId:\(userId!)")
//                    print("token:\(token!)")
//                    print("role:\(role!)")
//                    print("firstTime:\(firstTime?.description ?? "none")")
//                    
//                    CustomUserDefaults.shared.setAccessToken(token!)
//                    
//                    completion(ResponseEnum.success, message!)
//                    
//                }else if(status == ResponseEnum.failed.rawValue){
//                    completion(ResponseEnum.failed, message!)
//                    
//                }else{
//                    message = Wordings.msg_no_response_api
//                    completion(ResponseEnum.failed, message!)
//                }
//                
//                
//            }else if(statusResponse.isFailure){
//                if let httpStatusCode = responseData.response?.statusCode {
//                    
//                    if(httpStatusCode == 401){
//                        message = Wordings.msg_re_login
//                        print("API_MESSAGE_TOKEN:\(String(describing: message))")
//                        
//                        AppController.sharedInstance.logout()
//                        
//                    }else if(httpStatusCode == 500){
//                        message = Wordings.msg_server_error
//                        completion(ResponseEnum.failure, message!)
//                    }else if(httpStatusCode == 504){
//                        message = Wordings.msg_timeout_error
//                        completion(ResponseEnum.failure, message!)
//                    }else{
//                        print("API_MESSAGE_NEW_TOKEN: \(httpStatusCode)")
//                        message = Wordings.msg_maintenance
//                        completion(ResponseEnum.failure, message!)
//                    }
//                    
//                } else {
//                    print("NO_RESPONSE: \(String(describing: statusResponse.error))")
//                    let errorResponse = String(describing: statusResponse.error)
//                    
//                    if(errorResponse.contains("cancelled")){
//                        print("RESPONSE_CANCELLED")
//                        message = ""
//                    }else if let err = statusResponse.error as? URLError, err.code == .notConnectedToInternet{
//                        message = Wordings.msg_internet_error
//                    }else{
//                        message = Wordings.msg_connection_error
//                    }
//                    
//                    completion(ResponseEnum.failure, message!)
//                    print("API_MESSAGE: \(String(describing: message))")
//                }
//                
//            }else{
//                message = Wordings.msg_connection_error
//                completion(ResponseEnum.failure, message!)
//            }
//            
//        }
//    }

    
}



